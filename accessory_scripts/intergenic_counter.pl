#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $input_file;
my $output_file;

GetOptions ( 
            "infile=s"   => \$input_file,
            "outfile=s"   => \$output_file,
            );  # flag

usage() unless $input_file;

open(IN, "<$input_file") or die "can't open $input_file: $!\n";
open(OUT, "+>$output_file") or die "can't open $output_file: $!\n";

my %hits;
my $total_count = 0;

while (my $line = <IN>) {
	chomp $line;
	if ($line =~ /Key = (.+?)\tCount = (.+)/) {
		$hits{$1}{'count'} += $2;
		$total_count += $2;
		}
	else {
		print "Unable to parse and counts line!!";
		}
	}

foreach my $key (keys %hits) {
	$hits{$key}{'percent'} = $hits{$key}{count}/$total_count;
	}

foreach my $key (sort { $hits{$b}{count} <=> $hits{$a}{count} } keys %hits) {
	print OUT "$key\t$hits{$key}{count}\t$hits{$key}{percent}\n";
	}

close IN;
close OUT;
