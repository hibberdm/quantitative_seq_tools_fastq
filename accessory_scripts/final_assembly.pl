#!/usr/bin/env perl

use strict;
use warnings;

my $assembly_name = shift;
my $genome_name = shift;

my $log_file = 'assembly_' . $assembly_name . '_logfile.txt';
my $stats_file = 'assembly_' . $assembly_name . '_data_31/stats.txt';
my $contigs = 'assembly_' . $assembly_name . '_data_31/contigs.fa';

my $clean_name = $genome_name;
$clean_name =~ s/_/ /g;


my $coverage_stats = read_coverage_stats(1000-31, $stats_file);

# join the genomes

system("cp $contigs $genome_name.fna");

#my $accession = "NC_GK_" . $assembly_name;
my $accession = "NC_" . $assembly_name;


my ($n50, $coverage) = write_assembly_stats($log_file, $assembly_name,$genome_name, $coverage_stats, $accession);

my $header = "ANNOTATE";

$header = "ANNOTATEpoor" if ($n50 < 1000 || $coverage < 10);

if ($genome_name =~ /Bacteroides|Enterobacteriaceae/) {
	print $header . "perl ../process_new_genome.pl -i $genome_name.fna -o $accession -g $assembly_name -n '$clean_name' -k bacn\n";
}
else {
	print $header . "perl ../process_new_genome.pl -i $genome_name.fna -o $accession -g $assembly_name -n '$clean_name' -k bacp\n";
}

sub read_coverage_stats {
	my ($minLen, $stats_file) = @_;

	open (IN, $stats_file) or die "can't open $stats_file: $!\n";
	my $header = <IN>;
	my $count = 0;
	my $sumPerfect = 0;
	my $sum = 0;
	while (<IN>) {
		chomp;
		my @pieces = split /\t/;
		my $contigLen = $pieces[1];
		my $coverage = $pieces[5];
		my $coveragePerfect = $pieces[6];

		if ($contigLen > $minLen) {
			$sum += $coverage;
			$sumPerfect += $coveragePerfect;
			$count++;	
		}
	}
	my %res;
	$res{'coverage'} = $sum/$count;
	$res{'coveragePerfect'} = $sumPerfect/$count;

#	die "mean coverage $res{coverage} perfect coverage $res{coveragePerfect}\n";

	close IN;

	return \%res;
}

sub write_assembly_stats {
	my ($in,$assembly_name, $genome_name, $cov_stats, $accession)=@_;

	open (IN, $in) or die "can't open $in: $!\n";

	my $read_now=0;
	my $n50;
	my $numContigs;
	my $longestContig;
	my $totalBases;
	my $hashSize;
	my $num1Kcontigs;
	my $basesIn1Kcontigs;
	while(<IN>) {
#		print;
		chomp;
		if ($read_now) {
#			print "*$_\n";
			if (/^Velvet hash value:\s+(\d+)/) { $hashSize = $1; }
			elsif (/^n50:\s+(\d+)/) { $n50 = $1; }
			elsif (/^Total number of contigs:\s+(\d+)/) { $numContigs = $1; }
			elsif (/^length of longest contig:\s+(\d+)/) { $longestContig = $1; }
			elsif (/^Total bases in contigs:\s+(\d+)/) { $totalBases = $1; }
			elsif (/^Number of contigs > 1k:\s+(\d+)/) { $num1Kcontigs = $1; }
			elsif (/^Total bases in contigs > 1k:\s+(\d+)/) { $basesIn1Kcontigs = $1; }
		}
		$read_now = 1 if (/Final optimised assembly details/);
	}

	print "#accession\tassembly_name\tgenome_name\ttotalBases\tbasesIn1Kcontigs\tn50\tnumContigs\tlongestContig\tnum1Kcontigs\thashSize\tcoverage\tcoveragePerfect\n";
	print "$accession\t$assembly_name\t$genome_name\t$totalBases\t$basesIn1Kcontigs\t$n50\t$numContigs\t$longestContig\t$num1Kcontigs\t$hashSize\t$cov_stats->{coverage}\t$cov_stats->{coveragePerfect}\n";

	close IN;

	return $n50, $cov_stats->{coverage};
}
