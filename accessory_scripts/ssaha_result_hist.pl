#!/usr/bin/env perl

# prints out a histogram (on the terminal) of the number of reads mapping with each score to help
# choose a cut-off threshold for accepting read matches as "true" hits

use IO::Uncompress::AnyUncompress;

use strict;
use warnings;
my $in=shift;

die "Usage: ssaha_result_hist.pl <ssaha2 output file>\n" unless $in;

my $fh = new IO::Uncompress::AnyUncompress $in or die "can't open $in: $!\n";
#open (IN, $in) or die "can't open $in: $!\n";

my $prev = "";
my $searched=0;
my $matched=0;

my %counts;

while (<$fh>) {
	chomp;

	if (/^ALIGNMENT/) {
#		my @pieces = split /\s+/, $_;
		my ($header, $score, $query, $db, $qstart, $qstop, $dstart, $dstop, $orientation, $aln_length, $percent_match, $query_len) = split /\s+/, $_;
		next if $query eq $prev;

#		my $j = join "|", @pieces;
#		print "$score $query\n";

		$matched++;
		$counts{$score}++;	

		$prev = $query;
#		$
	}
	elsif (/^Matches For/) {
		$searched++;
	}
}

close $fh;

$searched = 1 unless $searched;

my @keys = sort {$b <=> $a} keys %counts;
my $max = -9999999;
my $sum = 0;
for my $k (@keys) {
	if ($counts{$k} > $max) {
		$max = $counts{$k};
	}
	$sum += $counts{$k};
}

my $hist_len = 100;  # make histogram 100 characters

my $len_per_bar = $max/$hist_len;

my $cum = 0;
for my $k (@keys) {
	printf "#len %5d\t", $k;
	my $bar_len = round($counts{$k}/$len_per_bar);
	for my $i (0 .. $hist_len) {
		if ($i <= $bar_len) {
			print "-";
		}
		else {
			print " ";
		}
	}
	$cum+= $counts{$k};
	printf " (%.3f) $counts{$k}\n", $cum/$sum;
	
#	print "$k $counts{$k}\n";
}

printf "matched %d of %d (%.2f%%) searches\n", $matched, $searched, $matched/$searched;


sub round {
	my $num = shift;
	return int($num + .5);
}

