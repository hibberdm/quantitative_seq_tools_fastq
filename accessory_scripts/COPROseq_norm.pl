#!/usr/bin/env perl

# combines COPRO-Seq files into an excel file or matlab files
#

use warnings;
use strict;
use Getopt::Long;

my @counts_files = `ls *.counts`;


my $length_norm_file; #= shift;
my $output_format = "excel";
my $outfile;
my $show_help;
my $IGS_file;
my $id_to_name;
my $inverse_length_norm;


GetOptions(
#	"gene_positions=s"   => \$gene_pos,
	"length_norm_file=s"     => \$length_norm_file,
	"IGS_correction=s"     => \$IGS_file,
	"format=s"     => \$output_format,
         "outfile=s" => \$outfile,
         "name_translation=s" => \$id_to_name,
         "v" => \$inverse_length_norm,
         "help" => \$show_help,
	);

usage() if $show_help;


my ($length_norm) = get_length_norm($length_norm_file) if $length_norm_file;
my ($name_translation) = get_name_translation($id_to_name) if $id_to_name;
my ($IGS) = get_IGS($IGS_file) if $IGS_file;

my %all_hits;
my @counts;
my @fractions;
my @files;
for my $c (@counts_files) {
	chomp $c;
	next if $c =~ /^\s*$/;

	if ($length_norm_file || $IGS) {
		my ($fractions, $counts) = get_counts($c, \%all_hits, $length_norm, $IGS);
		push @fractions, $fractions;
		push @counts, $counts;
	#	push @counts, get_counts($c, \%all_hits, $length_norm, $IGS);
	}
	else {
		my ($fractions, $counts) = get_counts($c, \%all_hits);
		push @fractions, $fractions;
		push @counts, $counts;
	}
	push @files, $c;
}

my @all_names = sort {$all_hits{$a} <=> $all_hits{$b}} keys %all_hits;
#my $length_norm;


print_count_matrix(\@all_names, \@counts, \@fractions, \@files, $length_norm, $outfile, $name_translation);

sub get_IGS {
	my $in=shift;
	open(IN, $in) or die "can't open $in: $!\n";
	my $head = <IN>;

	my %IGS;
	while (<IN>) {
		my ($name, $IGS) = split /\t/;

		$IGS{$name} = $IGS;

#		die "have $name\t$IGS\n";
	}
	close IN;


	return \%IGS;
}

sub get_name_translation {
	my $in=shift;

	my %name_to_fullname;
	open(IN, $in) or die "can't open $in: $!\n";
	while (<IN>) {
		chomp;
		#my ($name, $length, $species) = split /\t/;
		my ($id, $name) = split /\t/;
		$name_to_fullname{$id}=$name;
	}

	return \%name_to_fullname;
}

sub get_length_norm {
	my $in = shift;

	open(IN, $in) or die "can't open $in: $!\n";
	my $sum=0;
	my $count = 0;
	my %length_scale;
#	my %name_to_fullname;
	while (<IN>) {
		chomp;
		#my ($name, $length, $species) = split /\t/;
		my ($name, $length) = split /\t/;
		$length_scale{$name} = $length; # want short ones to become long
#		$name_to_fullname{$name} = $species if $species;

		$count++;
		$sum+=$length;
	}
	my $mean = $sum/$count;
#	print "have count $count\tlen$sum\t$mean\n";
	
	$sum=0;
	for my $n (keys(%length_scale)) {
		my $genome_length = $length_scale{$n};
		$length_scale{$n} = $mean/$length_scale{$n};
		$sum+=$length_scale{$n};
#		printf "scale $n\t$genome_length\t$length_scale{$n}\t%d\n", $genome_length*$length_scale{$n};
	}

	return \%length_scale;#, \%name_to_fullname;
}

sub print_count_matrix {
	my ($names, $counts, $ratios, $conditions,  $len_norm, $outfile, $name_translation, $IGS) = @_;
	my $fh_out = *STDOUT; # default to stdout
	if ($outfile) {
		open ($fh_out, ">$outfile") or die "can't open $outfile: $!\n";
	}


	my $species_file;
	$species_file = "species";
	$species_file = $outfile . ".species" if $outfile;

	my $counts_file = "count";
	$counts_file = $outfile . ".count" if $outfile;

	open (COUNTS, ">$counts_file") or die "can't open $counts_file: $!\n";
	

	open(NAMES, ">$species_file") or die "can't open $species_file: $!\n";
	for my $n (@$names) {
#		print NAMES "$n\n";
		if ($name_translation->{$n}) {
			my $nt = $name_translation->{$n};
			$nt =~ s/\s+/_/g;  # make things more matlab friendly
			print NAMES "$n\t$nt\n";
		}
		else {
			print NAMES "$n\n";
		}
		
	}
	my $conditions_file = "conditions";
	$conditions_file = $outfile . ".conditions" if $outfile;
	open(FILES, ">$conditions_file") or die "can't open $conditions_file: $!\n";
	for my $n (@$conditions) {
		$n =~ s/\.counts$//;
		print FILES "$n\n";
	}
	

	for my $i (0 .. $#$counts) {
		my $c = $counts->[$i];
		my $r = $ratios->[$i];
		my $counts_text;
		my $ratio_text;
#		$text .= "$conditions->[$i]\t";

		#length_normalized($c, $len_norm) if $len_norm;
		for my $n (@$names) {
			if ($c->{$n}) {
				$counts_text .= sprintf("%f\t", $c->{$n});
			}
			else {
				$counts_text .= "0\t";
			}
			if ($r->{$n}) {
				$ratio_text .= sprintf("%f\t", $r->{$n});
			}
			else {
				$ratio_text .= "0\t";
			}
		}
		chop $ratio_text;
		print $fh_out "$ratio_text\n";
		print COUNTS "$counts_text\n";
	}
}

sub length_normalized {
	my $counts = shift;
	my $len_norm = shift;

	my $sum=0;
	my $num=0;
	for my $n (keys %$counts) {
		$counts->{$n} = $counts->{$n} * $len_norm->{$n};
	}
}

sub get_counts {
	my $in = shift;
	my $all_hits = shift;
	my $length_norm=shift;
	my $IGS = shift;

	my %hash;
	my %hash_counts;
	open (IN, $in) or die "can't open $in: $!\n";
	my $sum=0;

	my @lines;
	my @counts;
	while (<IN>) {
		next if /^#/;
		next if /^feature_name/;
#		next if /have.*ties/;
		chomp;

		my @pieces = split /\t/;
#		print "@pieces\n";
		my ($name, $count, $discard1, $discard2, $score, $percent);
		if (scalar @pieces == 4) {
			($name, $count, $percent, $score) = @pieces;
		}
		elsif (scalar @pieces == 6) {
			($name, $count, $discard1, $discard2, $score, $percent) = @pieces;
		}

		$hash_counts{$name} = $count;

		if ($IGS) {
			if ($IGS->{$name}) {
				my $temp = $count;
				$count /= $IGS->{$name};
				print STDERR "count $temp scaled to $count from IGS $IGS->{$name}\n";
			}
			else {
#				print STDERR "warning couldn't find $name in IGS table\n";
			}
		}
		if ($length_norm) {
	#		print "in length norm with $name and $length_norm->{$name} $count\n";
			$count *= $length_norm->{$name};
		}

		$all_hits{$name} += $count;
		$hash{$name} = $count;
		$sum += $count;
	}
	close IN;

	for my $k (keys %hash) {
		$hash{$k} /= $sum;
	}

	return \%hash, \%hash_counts;
}

sub usage {
	print "\n\tUsage: COPROseq_norm.pl\n".
		"\t\t-o [outfile (default STDOUT)]\n".
		"\t\t-l genome length file (for scaling)\n".
		"\t\t-I IGS file (for scaling)\n".
		"\t\t-f format [excel (default) or matlab]\n".
		"\t\t-n name translation (to replace an accession number with a species name\n".
		"\t\t-v inverse length norm (use to adjust IGS back to where long genomes get \n".
		"\t\t   more relative weight (to enable correct DNA/fecal-pellet estiamtes)\n".
#                "\t\t-f [format (excel OR matlab); default excel]\n".
		"\t\t-h help (this message)\n";

	exit(1);
}
