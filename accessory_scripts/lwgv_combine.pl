#!/usr/bin/env perl

use strict;
use warnings;

my $genome = shift;
my $make_separate_tracks=shift;

die "Usage: perl lwgv_combine.pl <genome_name/accession>\n" unless $genome;

my @files = `ls *.$genome.counts.genome_counts`;

my @gfiles;

for my $f (@files) {
	chomp $f;
	next if $f =~ /^\s*$/;

	push @gfiles, $f;
}

#die "have @gfiles\n";
my $out = "$genome.RNAseq";
my $outBIN = "$genome.RNAseqBIN";
my $fh;
open $fh, ">$out" or die "can't open $out: $!\n";

my $fhBIN;
open $fhBIN, ">$outBIN" or die "can't open $out: $!\n";
binmode($fhBIN);

my @genome_counts;
my @genome_counts_f;
my @genome_counts_r;
for my $g (@gfiles) {

	my @stuff = split /\./, $g;
	my $track_name = $stuff[0];
	print STDERR "adding $g $stuff[0]\n";
	
	add_genome_counts(\@genome_counts, \@genome_counts_f, \@genome_counts_r, $g);
	
	if ($make_separate_tracks) {
		my $track_file = $genome . '_' . $track_name . '.RNAseq';
		open $fh, ">$track_file" or die "can't open $out: $!\n";
		print STDERR "printing track $track_name to $track_file\n";
		print_track(\@genome_counts, $track_name);
		@genome_counts = ();
		close $fh;
	}
}

if (!$make_separate_tracks) {
	print_track(\@genome_counts, 'RNAseq');
#	print_track(\@genome_counts_f, 'RNAseq_for');
#	print_track(\@genome_counts_r, 'RNAseq_rev');
}

sub print_track { 
	my ($gc, $track_name)=@_;

	my $mapped_reads = 0;

	for my $c (@$gc) { $mapped_reads += $c if $c && log($c) > 0}

	my $power = int(log($mapped_reads));

	
	my ($min, $max) = calculate_min_max($gc);


	# for the binary track we start with a header of size (unsigned long), null, min (float), max (float) 
	my $temp = pack('L', scalar @$gc);
	print $fhBIN $temp;

	my $null = pack('f', 0.0);
	print $fhBIN $null;

	$temp = pack('f', $min);
	print $fhBIN $temp;
	$temp = pack('f', $max);
	print $fhBIN $temp;

	my $track = "graph $track_name" . "_e$power addPoints(";
	for (my $i=0; $i<$#$gc; $i++) {
		if ($gc->[$i] && log($gc->[$i]) > 0 ) { # skip the non-existant and ones only there a single time
			$track .= sprintf("%d:%.2f,", $i, log($gc->[$i]));
			print $fhBIN pack('f', log($gc->[$i]));
		}
		else {
			print $fhBIN $null;
		}
	}
	chop $track;
	$track .= ")";
	print $fh "$track\n";
}

sub calculate_min_max {
	my $gc = shift;

	my $max=-9999999999999999;
	my $min=99999999999999999;

	for (my $i=0; $i<$#$gc; $i++) {
		my $val = 0;
		$val = log($gc->[$i]) if $gc->[$i] && $gc->[$i] != 0;

		if ($val > $max) { # skip the non-existant and ones only there a single time
			$max=$val;
		}
		if ($val < $min) {
			$min=$val;
		}
	}

	return $min, $max;
}



sub add_genome_counts {
	my ($counts, $counts_f, $counts_r, $infile) = @_;

	open (IN, $infile) or die "can't open $infile: $!\n";
	while (<IN>) {
		chomp;
		my ($pos, $count, $count_f, $count_r) = split /\t/;
		$counts->[$pos] += $count;

		$counts_f->[$pos] += $count_f if $count_f; 
		$counts_r->[$pos] += $count_r if $count_r; 
	}

	close IN;
}


