#package genomecodes;

# Eventually need to automate updates to this module such that a 3-column table can be used as
# input, where column 1 is accession #, 2 is abbreviation, 3 is species name

sub declare_inhouseacc_for_genomeabbrev {
	my %inhouseacc_for_genomeabbrev = (
		"BACCAC"	=> "NZ_AAVM00000000",
		"BACOVA" 	=> "NZ_AAXF00000000",
		"BACTHE7330"	=> "NC_Bthetaiotaomicron7330",
		"BACTHE" 	=> "NC_004663",
		"BACUNI" 	=> "NZ_AAYH00000000",
		"BACVUL" 	=> "NC_009614",
		"BACWH2" 	=> "NC_BWH2",
		"BACWH2-CLOSED"	=> "NC_BacWH2_v1",
		"BIFANI" 	=> "NZ_BanimalisDN1730010",
		"BLAHYD"	=> "NZ_ACBZ00000000",
		"BRYFOR"	=> "NZ_ACCL00000000",
		"CLOSCI" 	=> "NZ_ABFY00000000",
		"CLOSPI" 	=> "NZ_ABIK00000000",
		"CLOSYM"	=> "NC_Csymbiosum",
		"COLAER" 	=> "NZ_AAVN00000000",
		"DESPIG"	=> "NZ_ABXU00000000",
		"DESPIGGOR1"	=> "NC_DPigerGOR1",
		"DORLON" 	=> "NZ_AAXB00000000",
		"DORFOR"	=> "NZ_AAXA00000000",
		"ESCCOL"	=> "NC_000913",
		"EUBHAL"	=> "NZ_ACEP00000000",
		"EUBREC" 	=> "NC_012781",
		"FAEPRA" 	=> "NZ_ABED00000000",
		"LACDEL130"	=> "NC_LB130",
		"LACDEL182" 	=> "NC_LB182",
		"LACLAC066"	=> "NC_LACLAC",
		"METSMI"	=> "NC_009515",
		"PARDIS" 	=> "NC_009615",
		"ROSINT"	=> "NZ_ABYJ00000000",
		"RUMOBE" 	=> "NZ_AAVO00000000",
		"RUMTOR" 	=> "NZ_AAVP00000000",
		"STRTHE147"	=> "NC_SthermophilusDN001147",			# NOT the Activia strain
		"STRTHE171" => "NZ_SthermophilusDN001171"			# The Activia strain
	);
	return %inhouseacc_for_genomeabbrev;
}

sub declare_speciesname_for_inhouseacc {
	my %speciesname_for_inhouseacc = (
		"NC_010655"	=> "Akkermansia_muciniphila_ATCC_BAA-835",
		"NC_Aindistictus"	=> "Alistipes_indistinctus_YIT_12060",
		"NZ_ABXA00000000"	=> "Anaerococcus_hydrogenalis_DSM_7454",
		"NZ_ABGD00000000"	=> "Anaerotruncus_colihominis_DSM_17241",
		"NZ_AAVM00000000"	=> "Bacteroides_caccae_ATCC_43185",
		"NZ_ACCH00000000"	=> "Bacteroides_cellulosilyticus_DSM_14838",
		"NZ_ACBW00000000"	=> "Bacteroides_coprophilus_DSM_18228",
		"NZ_ABWZ00000000"	=> "Bacteroides_dorei_DSM_17855",
		"NZ_ABVO00000000"	=> "Bacteroides_eggerthii_DSM_20697",
		"NZ_ABXI00000000"	=> "Bacteroides_finegoldii_DSM_17565",
		"NZ_ABJL00000000"	=> "Bacteroides_intestinalis_DSM_17393",
		"NZ_AAXF00000000"	=> "Bacteroides_ovatus_ATCC_8483",
		"NZ_ABQC00000000"	=> "Bacteroides_plebeius_DSM_17135",
		"NZ_ABFZ00000000"	=> "Bacteroides_stercoris_ATCC_43183",
		"NC_Bthetaiotaomicron3731"	=> "Bacteroides_thetaiotaomicron_3731",
		"NC_Bthetaiotaomicron7330"	=> "Bacteroides_thetaiotaomicron_7330",
		"NC_004663"	=> "Bacteroides_thetaiotaomicron_VPI-5482",
		"NZ_AAYH00000000"	=> "Bacteroides_uniformis_ATCC_8492",
		"NC_009614"	=> "Bacteroides_vulgatus_ATCC_8482",
		"NC_BWH2"	=> "Bacteroides_WH2",
		"NC_BxylanisolvensXB1A"	=> "Bacteroides_xylanisolvens_XB1A",
		"NZ_AAXD00000000"	=> "Bifidobacterium_adolescentis_L2-32",
		"NZ_ABYS00000000"	=> "Bifidobacterium_angulatum_DSM_20098",
		"NC_Bbifidum_20456"	=> "Bifidobacterium_bifidum_DSM_20456",
		"NC_Bdentium"	=> "Bifidobacterium_dentium_ATCC_27678",
		"NZ_ABXX00000000"	=> "Bifidobacterium_pseudocatenulatum_DSM_20438",
		"NZ_ABYU00000000"	=> "Blautia_hansenii_DSM_20583",
		"NZ_ACBZ00000000"	=> "Blautia_hydrogenotrophica_DSM_10507",
		"NC_Bluti"	=> "Blautia_luti_DSM_14534",
		"NZ_ACCL00000000"	=> "Bryantella_formatexigens_DSM_14469",
		"NZ_ACCK00000000"	=> "Catenibacterium_mitsuokai_DSM_15897",
		"NZ_ABWL00000000"	=> "Citrobacter_youngae_ATCC_29220",
		"NZ_ACCJ00000000"	=> "Clostridium_asparagiforme_DSM_15981",
		"NZ_ABEZ00000000"	=> "Clostridium_bartlettii_DSM_16795",
		"NZ_ABCC00000000"	=> "Clostridium_bolteae_ATCC_BAA-613",
		"NZ_ACIO00000000"	=> "Clostridium_hathewayi_DSM_13479",
		"NZ_ABWP00000000"	=> "Clostridium_hiranonis_DSM_13275",
		"NZ_ABYI00000000"	=> "Clostridium_hylemonae_DSM_15053",
		"NZ_ABCB00000000"	=> "Clostridium_leptum_DSM_753",
		"NZ_ABWO00000000"	=> "Clostridium_nexile_DSM_1787",
		"C_nexile_A2_232"	=> "Clostridium_nexile-related_A2-232",
		"NC_orbiscindens"	=> "Clostridium_orbiscindens_DSM_6740",
		"NZ_ABFX00000000"	=> "Clostridium_ramosum_DSM_1402",
		"NZ_ABFY00000000"	=> "Clostridium_scindens_ATCC_35704",
		"NZ_ACFX00000000"	=> "Clostridium_sp_M62_1",
		"NZ_ABIK00000000"	=> "Clostridium_spiroforme_DSM_1552",
		"NZ_ABKW00000000"	=> "Clostridium_sporogenes_ATCC_15579",
		"NC_Csymbiosum"	=> "Clostridium_symbiosum_DSM_934",
		"NZ_AAVN00000000"	=> "Collinsella_aerofaciens_ATCC_25986",
		"NZ_ABXH00000000"	=> "Collinsella_intestinalis_DSM_13280",
		"NZ_ABXJ00000000"	=> "Collinsella_stercoris_DSM_13279",
		"NZ_ABVR00000000"	=> "Coprococcus_comes_ATCC_27758",
		"NZ_ABEY00000000"	=> "Coprococcus_eutactus_ATCC_27759",
		"NC_DpigerGOR1"	=> "Desulfovibrio_piger_GOR1",
		"NZ_AAXA00000000"	=> "Dorea_formicigenerans_ATCC_27755",
		"NZ_AAXB00000000"	=> "Dorea_longicatena_DSM_13814",
		"NZ_ADGK00000000"	=> "Edwardsiella_tarda_ATCC_23685",
		"NC_Ecancerogenus"	=> "Enterobacter_cancerogenus_ATCC_35316",
		"NC_000913"	=> "Escherichia_coli_K-12_substr_MG1655",
		"NC_011740"	=> "Escherichia_fergusonii_ATCC_35469",
		"NZ_ABYT00000000"	=> "Eubacterium_biforme_DSM_3989",
		"NC_Ecylindroides"	=> "Eubacterium_cylindroides_DSM_3983",
		"NZ_ABAW00000000"	=> "Eubacterium_dolichum_DSM_3991",
		"NC_012778"	=> "Eubacterium_eligens_ATCC_27750",
		"NC_EhaliiL27"	=> "Eubacterium_halii-related_L2-7",
		"NZ_ACEP00000000"	=> "Eubacterium_hallii_DSM_3353",
		"NC_Eplautii"	=> "Eubacterium_plautii_ATCC_29863",
		"NC_012781"	=> "Eubacterium_rectale_ATCC_33656",
		"NZ_AAVL00000000"	=> "Eubacterium_ventriosum_ATCC_27560",
		"NZ_ABED00000000"	=> "Faecalibacterium_prausnitzii_M21_2",
		"NZ_ACIE00000000"	=> "Fusobacterium_varium_ATCC_27725",
		"NZ_ACCF00000000"	=> "Holdemania_filiformis_DSM_12042",
		"NC_009513"	=> "Lactobacillus_reuteri_DSM_20016",
		"NZ_ACGS00000000"	=> "Lactobacillus_ruminis_ATCC_25644",
		"NC_Mfuniformis"	=> "Megamonas_funiformis_DSM_19343",
		"NZ_ABWK00000000"	=> "Mitsuokella_multacida_DSM_20544",
		"NC_009615"	=> "Parabacteroides_distasonis_ATCC_8503",
		"NZ_ABYH00000000"	=> "Parabacteroides_johnsonii_DSM_18315",
		"NZ_AAXE00000000"	=> "Parabacteroides_merdae_ATCC_43184",
		"NZ_ABVP00000000"	=> "Proteus_penneri_ATCC_35198",
		"NZ_ABXW00000000"	=> "Providencia_alcalifaciens_DSM_30120",
		"NZ_ACCI00000000"	=> "Providencia_rettgeri_DSM_1131",
		"NZ_ABXV00000000"	=> "Providencia_rustigianii_DSM_4541",
		"NZ_ABJD00000000"	=> "Providencia_stuartii_ATCC_25827",
		"NZ_ABYJ00000000"	=> "Roseburia_intestinalis_L1-82",
		"NZ_AAYG00000000"	=> "Ruminococcus_gnavus_ATCC_29149",
		"NC_RgnavusGM2_1"	=> "Ruminococcus_gnavus_GM2_1",
		"NZ_ABOU00000000"	=> "Ruminococcus_lactaris_ATCC_29176",
		"NZ_AAVO00000000"	=> "Ruminococcus_obeum_ATCC_29174",
		"NZ_AAVP00000000"	=> "Ruminococcus_torques_ATCC_27756",
		"NZ_ABJK00000000"	=> "Streptococcus_infantarius_subsp_infantarius_ATCC_BAA-102",
		"NZ_ACBY00000000"	=> "Subdoligranulum_variabile_DSM_15176",
		"NZ_ABDE00000000"	=> "Victivallis_vadensis_ATCC_BAA-548"
	);

	return %speciesname_for_inhouseacc;
}

1;

# NCBI codes below are not being maintained until I can figure out how to get complete genomes pulled down from GenBank

sub declare_genbankacc_for_genomeabbrev {
	my %genbankacc_for_genomeabbrev = (
		"BACCAC"		=> "NZ_AAVM00000000",
		"BACOVA"		=> "NZ_AAXF00000000",
		"BACTHE"		=> "NC_004663",
		"BACUNI"		=> "NZ_AAYH00000000",
		"BACVUL"		=> "NC_009614",
		# No GenBank entry for BACWH2 yet...
		# No GenBank entry for B. animalis DN 173-010 yet...
		"CLOSCI"		=> "NZ_ABFY00000000",
		"CLOSPI"		=> "NZ_ABIK00000000",
		"COLAER"		=> "NZ_AAVN00000000",
		"DORLON"		=> "NZ_AAXB00000000",
		"EUBREC"		=> "NC_012781",
		"FAEPRA"		=> "NZ_ABED00000000",
		# No GenBank entry for L. delbrueckii bulgaricus DN 100-130 yet...
		# No GenBank entry for L. delbrueckii bulgaricus DN 100-182 yet...
		# No GenBank entry for L. lactis DN 030-066 yet...
		"PARDIS"		=> "NC_009615",
		"RUMOBE"		=> "NZ_AAVO00000000",
		"RUMTOR"		=> "NZ_AAVP00000000"
		# No GenBank entry for S. thermophilus DN 001-147 or 001-171 yet...
	);
	return %genbankacc_for_genomeabbrev;
}

sub declare_speciesname_for_genbankacc {
	my %speciesname_for_genbankacc = (
		"NZ_AAVM00000000"		=> "B_caccae",
		"NZ_AAXF00000000"		=> "B_ovatus",
		"NC_004663"				=> "B_thetaiotaomicron",
		"NZ_AAYH00000000"		=> "B_uniformis",
		"NC_009614"				=> "B_vulgatus",
		# No GenBank entry for BACWH2 yet...
		# No GenBank entry for B. animalis DN 173-010 yet...
		"NZ_ABFY00000000"		=> "C_scindens",
		"NZ_ABIK00000000"		=> "C_spiroforme",
		"NZ_AAVN00000000"		=> "C_aerofaciens",
		"NZ_AAXB00000000"		=> "D_longicatena",
		"NC_012781"				=> "E_rectale",
		"NZ_ABED00000000"		=> "F_prausnitzii M21-2",
		# No GenBank entry for L. delbrueckii bulgaricus DN 100-130 yet...
		# No GenBank entry for L. delbrueckii bulgaricus DN 100-182 yet...
		# No GenBank entry for L. lactis DN 030-066 yet...
		"NC_009615"				=> "P_distasonis",
		"NZ_AAVO00000000"		=> "R_obeum",
		"NZ_AAVP00000000"		=> "R_torques"
		# No GenBank entry for S. thermophilus DN 001-147 or 001-171 yet...
	);
	return %speciesname_for_genbankacc;
}

1;
