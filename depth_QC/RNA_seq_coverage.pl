#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use Cwd;
use POSIX qw(strftime);

my $datestring = strftime "%g%m%d", localtime;

my @feature_types = ("CDS","TU","5S_rRNA","16S_rRNA","23S_rRNA","miRNA","transcription start site","operon","tRNA", "tmRNA", "sRNA","miscRNA");

use lib "../lib/";

#Separate metadata conversion package
use SAMPLE_METADATA;
use GENOMECODES;
my %metadata_hash = get_metadata;
my %accession_hash = declare_speciesname_for_inhouseacc;

my $infile = "";
my $outfile = "";
my $threshold = 1;
my $pseudocount = 1;
my $help;
my $metadata;
my $convert_accessions;
my $header_out = 0;
my $dir = getcwd;
my @accession_array = ();

GetOptions ("infile=s"   => \$infile, #list of accessions/codes for organisms for which coverage will be calculated, no suffixes.
            "outfile=s" => \$outfile,
            "directory=s" => \$dir, #defauls to cwd
	          "threshold=i" => \$threshold, #threshold for filtering raw counts by a user-defined threshold number
            "metadata" => \$metadata, #"flag" which indicates whether or not to print metadata to output files
           	"convert_accessions" => \$convert_accessions,
           	"help" => \$help,
           );

#check required options, calling &usage if required options not set
usage() unless $infile && $outfile;
usage() if $help;

#open file(s)
open (IN, "<$infile") || die "Couldn't open input file $infile, $!\n";
open (OUT, "+>$outfile") || die "Couldn't open output file $outfile, $!\n";
open (LOG, "+>$datestring\_cds_coverage.log") || die "Couldn't open log file, $!\n";

while (<IN>) {
    chomp $_;
    push @accession_array, $_ ;
	}

print LOG "\nMY ACCESSIONS ARE: \n\t".join("\t", @accession_array)."\n\n";

my %data_hash = ();
my %CDS_hash = ();
my %header_hash = ();

foreach my $accession (@accession_array) {

  open (AN, "<$dir\/$accession\.genome") || die "Couldn't open input file $accession, $!\n";
	open (IN, "<$dir\/$accession\_normalized\/$accession\.normalized") || die "Couldn't open input file $accession\.normalized, $!\n";

	my $CDS_count = 0;
	my $intergenic_count = 0;
	my $RNA_count = 0;
	my $other_count = 0;
	my $an_line_count = `grep -c '>' '$dir$accession\.genome'`;

	while (my $line = <AN>) {
		chomp $line;
		if ($line =~ /^>(.+)\|CDS\|/) {
			$CDS_hash{$accession}{$1} = 0;
#			print LOG "$1\n";
			$CDS_count++;
			}
		elsif ($line =~ /^>.+intergenic.+/) {
			$intergenic_count++;
			}
		elsif ($line =~ /^>.+\|.+RNA\|.+/) {
			$RNA_count++;
			}
		elsif ($line =~ /^>/) {
			$other_count++;
			}
		}

	if ($an_line_count == $CDS_count + $intergenic_count + $RNA_count + $other_count) {
		print LOG "Annotation file $accession read correctly.\n  Summary:\n\tCDS: $CDS_count\n\tintergenic: $intergenic_count";
		print LOG "\n\tRNAs: $RNA_count\n\tother: $other_count\n\t\tTotal: $an_line_count\n";
		}
	else {
		die ("Counting error. Not all lines handled correctly. Exiting...\n");
		}

	my $firstline = 0;
  my $data_cds_count = 0;
  my @headers = ();

	while (my $line = <IN>) {
    chomp $line;
		if ($firstline == 0) {
			my @array = split("\t", $line);
			for (my $i = 1; $i < $#array; $i+=4) {
				$array[$i] =~ /counts:(.+)/;
				push(@headers, $1);
        $header_hash{$1} = 1;
        $data_hash{$accession}{$1}{"count"} = 0;
        $data_hash{$accession}{"CDS_count"} = 0;
        }
			$firstline++;
			}
		else {
#      print "@headers\n";
      my @array = split("\t", $line);
			if (exists $CDS_hash{$accession}{$array[0]}) {
				$data_hash{$accession}{CDS_count}++;
#				my @processed = ();
        my $headers_index = 0;
        for (my $i = 1; $i < $#array; $i+=4) {
					if ($array[$i] >= ($threshold + $pseudocount)) { #take care of pseudocount
#						push(@processed, $array[$i]);
            #print "$headers[$headers_index] $array[$i] Pass!\n";
            $data_hash{$accession}{$headers[$headers_index]}{count}++;
            $headers_index++;
            }
					else {
            #print "$headers[$headers_index] $array[$i] Fail!\n";
            $headers_index++;
#						push(@processed, "0");
						}
					}
#				$data_hash{$array[0]} = [@processed];
#				print "$array[0] => @{$data_hash{$array[0]}}\n$#{$data_hash{$array[0]}} elements\n";
				}
			else {
				die("Non-CDS feature in results file: $array[0]!  Exiting...");
#				print LOG "$array[0]\n";
#				print LOG "CDS not found!!!!\n";
				}
			}
		}

	close AN;
	close IN;
}

if ($header_out == 0) {
		print OUT "CDS Coverage\t";
		foreach my $sample (sort keys %header_hash) {
			if (exists $metadata_hash{$sample}) {
				print OUT "$metadata_hash{$sample}[0]\t";
				}
			else {
				print OUT "$sample\t";
				}
			}
		print OUT "\n";
		if ($metadata) {
			foreach my $index (1 .. $#{$metadata_hash{google_name}}) {
				print OUT "$metadata_hash{google_name}[$index]\t";
					foreach my $key (sort keys %header_hash) {
						if (exists $metadata_hash{$key}[$index]) {
							print OUT "$metadata_hash{$key}[$index]\t";
							}
						else {
							print OUT "not_found\t";
							}
						}
						print OUT "\n";
					}
			}
		}
	$header_out++;

my $column = 0;

foreach my $accession (@accession_array) {
  foreach my $sample (sort keys %header_hash) {
    if ($column == 0) {
      if (($convert_accessions) && (exists($accession_hash{$accession}))) {
        print OUT "$accession_hash{$accession}\t";
        }
      else {
        print OUT "$accession\t";
        }
      $column++;
      if (exists $data_hash{$accession}{$sample}) {
        my $frac_pass = $data_hash{$accession}{$sample}{count}/$data_hash{$accession}{CDS_count};
        printf OUT "%.3f\t", $frac_pass;
#        print OUT "$data_hash{$accession}{$sample}{count}\/$data_hash{$accession}{CDS_count}\t"
#        print "$frac_pass\n";
      }
      else {
        print OUT "0\t";
      }
      }
    else {
      if (exists $data_hash{$accession}{$sample}) {
        my $frac_pass = $data_hash{$accession}{$sample}{count}/$data_hash{$accession}{CDS_count};
        printf OUT "%.3f\t", $frac_pass;
#        print OUT "$data_hash{$accession}{$sample}{count}\/$data_hash{$accession}{CDS_count}\t"
#        print "$frac_pass\n";
      }
      else {
        print OUT "0\t";
      }
    }
  }
  print OUT "\n";
  $column = 0;
}

close OUT;
close LOG;

sub usage {
	print "\n\tCalculates CDS feature coverage for multiple RNA-Seq genomes\n".
	  "\n\tUsage: perl RNA_seq_coverage.pl -i <> -o <>\n".
		"\t\t-i <input file containing accessions to be considered (one per line)>\n".
    "\t\t-d <directory containing RNA-Seq analysis (default is CWD)>\n".
    "\t\t-o <output file>\n".
    "\t\t-t <raw count threshold to determine P/A (default = 1 as per RNA-Seq pipeline default)>\n".
		"\t\t-m <FLAG: print metadata from SAMPLE_METADATA.pm>\n".
		"\t\t-c <FLAG: convert accession numbers to strain names using GENOMECODES.pm>\n".
		"\t\t-h <FLAG: print help and exit>\n\n";
	exit(1);
	}
