#!/usr/bin/env perl

# note this is pretty slow; just to read such a large file in perl takes a while
# would be hard to gain much more performance in perl, as I've already used a lot of 
# efficiency tricks to make it faster (e.g. use a hash of file handles)
#
# might be worth rewriting in C sometime.....
#150310: MCH Update: added support for fastq output to align with requirements of GEO
#	and am considering removing scarf support altogether.
#170109: MCH removed the 8th base -> T conversion in the barcode checks. Why not be more stringent
##and in accordance with our COPRO-Seq strategy

use strict;
use warnings;
#use IO::Uncompress::AnyUncompress;


#my $FASTQ = 1;
#my $FASTQ_ASCII = 2;

my $barcode_file = shift;
my $input_file  = shift;
my $barcode_out = shift;
my $keep_length = shift;

$barcode_out = $input_file . ".barcode_stats" if $barcode_out;

die "Usage: split_barcodes.pl <barcode_file> <input_fastq_file> [print_barcode_stats (1 = yes)]\n" unless $barcode_file && $input_file;

my ($barcodes, $barcode_to_name) = read_barcodes($barcode_file);

my %filehandles = %$barcodes;

if ($input_file =~ /fastq$/) {
    split_fastq($barcodes, $input_file, $barcode_out, $barcode_to_name, $keep_length);
	}
else {
	die "File format not recognized.  Check compatibility and re-try\n";
	}

sub split_fastq {
        my ($barcodes, $in, $barcode_out, $barcode_to_name, $seq_length)=@_;

        #my $IN = new IO::Uncompress::AnyUncompress $in or die "can't open $in: $!\n";
        #my $IN = new IO::Uncompress::AnyUncompress $in or die "can't open $in: $!\n";
        #open(my $IN, "-|", "zcat " . $in);
        open(my $IN, $in);
        #open (IN, $in) or die "can't open $in: $!\n";

        my $num_read = 0;
        my $num_missed = 0;
        my %counts;
        my $format_type = 0;

        my $barcode_len = get_barcode_len($barcodes);

        if ($barcode_out) {
                open (OUT, ">$barcode_out") or die "can't open $barcode_out: $!\n";
	        }

        while (<$IN>) {
                my $header = $_;
                #chomp $header;
                my $seq = <$IN>;
                chomp $seq;
                my $plus = <$IN>;
                #chomp $plus;
                my $quality = <$IN>;
                chomp $quality;

                my $missed_barcode = 1;
#               my @pieces = split /:/, $_;
#               print "@pieces\n";
                my $bc = substr($seq, 0, $barcode_len);
#                $bc =~ s/N$/T/;
                $bc =~ s/[ACGTN]$/T/;
                #my @pieces2 = split /:/, $line2;

                if (my $fh = $barcodes->{$bc}) { # match barcode
#                        print "My scarf_fh is $fh\nMy fastq_fh is $fh2\n\n";
                        $missed_barcode = 0;
                        $counts{$bc}++;
                        if ($seq_length) {
				$seq = substr($seq, $barcode_len, $seq_length);
                       		$quality = substr($quality, $barcode_len, $seq_length);
				}
			else {
				$seq = substr($seq, $barcode_len);
				$quality = substr($quality, $barcode_len);
				}

#                       my @pieces = ($header1,"","","","",$seq1,$quality1);
#                       my @pieces2 = ($header2,"","","","",$seq1,$quality1);
#                       my @pieces = ($num_read,"","","","",$seq,$quality);
	#BCK					my @pieces_fastq = ($header,$seq,$plus,$quality);

 #                      my $read = join ":", @pieces;
	#BCK					my $fastq_read = join "\n", @pieces_fastq;
#                       print "$seq\n";
#                       my $fh1 = $fh->[0];
#MCH#			        print $fh "$read\n";
                        print $fh "$header$seq\n$plus$quality\n";
                        #BCK print $fh "$fastq_read\n";
                }

                $num_missed++ if $missed_barcode;
                $num_read++;
        }

        my $num_matched = $num_read-$num_missed;
        printf STDERR "\n\t\tSUMMARY STATS\n";
        printf STDERR "%d of %d (%.1f%%) of reads had a barcode match\n", $num_matched, $num_read, ($num_matched/$num_read)*100;
        #printf STDERR "%d of %d (%.1f%%) of reads had PE barcodes that differed\n", $pe_not_same, $num_read, ($pe_not_same/$num_read)*100;
        printf STDERR "#%d\t%d\t%f\n", $num_matched, $num_read, ($num_matched/$num_read);
        printf OUT "%d\t%d\t%f\n", $num_matched, $num_read, ($num_matched/$num_read) if $barcode_out;
#       printf OUT "%d of %d (%.1f%%) of reads had a barcode match\n", $num_matched, $num_read, ($num_matched/$num_read)*100;
        #printf OUT "%d of %d (%.1f%%) of reads had PE barcodes that differed\n", $pe_not_same, $num_read, ($pe_not_same/$num_read)*100;

        my @s = sort {$counts{$b} <=> $counts{$a}} keys %counts;
        for my $s (@s) {
                printf STDERR "$s\t%.1f%% ($counts{$s} of $num_matched)\n", ($counts{$s}/$num_matched) * 100;
                if ($barcode_to_name && $barcode_to_name->{$s}) {
                        printf OUT "$barcode_to_name->{$s}\t$s\t$counts{$s}\t$num_matched\n", ($counts{$s}/$num_matched) * 100 if $barcode_out;
                }
                else {
                        printf OUT "$s\t$counts{$s}\t$num_matched\n", ($counts{$s}/$num_matched) * 100 if $barcode_out;
                }
        }
        close OUT if $barcode_out;
}

sub get_barcode_len {
	my $barcodes = shift;

	my @keys = keys %$barcodes;
	my $first = shift @keys;

	my $len = length($first);

	for my $k (@keys) {
		if ($len != length($k)) {
			die "Error: barcodes must all be the same length (found $len and " . length($k)  . " bp\n";
		}
	}

	return $len;
}

sub read_barcodes {
	my $in=shift;
	open (IN, $in) or die "can't open $in: $!\n";

	my @fileHandles;
	my %barcodes;
	my %seq_to_barcode_name;
	while (<IN>) {
		chomp;
		my (@stuff) = split /\t/;

		my ($barcode_name, $barcode, $filename);
		if (scalar @stuff == 3) {
			($barcode_name, $barcode, $filename) = @stuff;
		}
		else {
			($barcode, $filename) = @stuff;
		}
#		my ($barcode, $filename) = split /\t/;
		print "have $barcode $filename\n";
	 	open my $out, ">$filename" or die "can't open $filename: $!\n";
#		push @fileHandles, $out;
		$barcodes{$barcode} = $out; # barcode points to the file handle
		if ($barcode_name) {
			$seq_to_barcode_name{$barcode} = $barcode_name;
		}
	}
	close IN;
	
#	return \@fileHandles, \@barcodes;
	return \%barcodes, \%seq_to_barcode_name;
}
