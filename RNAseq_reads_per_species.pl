#!/usr/bin/env perl

#Updated by Matt Hibberd
#July 1, 2013
#170112 MCH Rewrote tabulation structure to be more flexible for organisms not present in all samples

use strict;
use warnings;
use Getopt::Long qw(:config no_ignore_case);
use List::MoreUtils qw(uniq);
use FindBin;
use lib "./";
#use lib "/home/comp/jglab/mhibberd/scratch/Reads_per_species";

#Separate metadata conversion package
use SAMPLE_METADATA;
use genomecodes;
my %metadata_hash = get_metadata;
my %accession_hash = declare_speciesname_for_inhouseacc;

#default directory is cwd
my $infolder = "./";
my $outfile = "";
my $metadata;
my $formatExcel;
my $convert_accessions;
my %counts_hash;

GetOptions ("infolder:s"   => \$infolder, #location of the .normalized files
            "outfile=s"    => \$outfile,
            "metadata"   => \$metadata,
            "excel"      => \$formatExcel,
            "convert_accessions"	=> \$convert_accessions,
            ) or die("Error in command line arguments\n");

usage() unless $outfile;

open (OUT, "+>$outfile") or die "Can't open $outfile: $!\n";

my @files = `ls $infolder*.normalized`;

print "Files analyzed are:\n";
foreach my $file (@files) {
	chomp $file;
	print "\t$file\n";
	my @stuff = split /\//, $file;
	my $line = "";
	my $accession = $stuff[$#stuff];
	$accession =~ s/\.normalized//;
	my $hash_ref = get_counts($file, $formatExcel, $accession, \%counts_hash);
	%counts_hash = %$hash_ref;
	}

my @headers_all = ();

foreach my $accession (sort keys %counts_hash) {
	foreach my $sample (sort keys %{$counts_hash{$accession}}) {
		push(@headers_all, $sample);
#		print "$sample\n";
		}
	}

my @unique_headers = uniq(@headers_all);
my @sorted_headers = sort @unique_headers;

print OUT "Accession\t";

for my $h (@sorted_headers) {
	my $line = "";
	$h =~ /counts:(.+)/;
	if (($metadata) && (exists $metadata_hash{$1})) {
			$line .= "$metadata_hash{$1}[0]";
			}
		else {
			$line .= "$h";
			}
	print OUT "$line\t";
	}
print OUT "\n";
if ($metadata) {
	&print_metadata_fields(\@sorted_headers);
	}

foreach my $accession (sort keys %counts_hash) {
	my $line_header = "";
	my $line = "";
	if (($convert_accessions) && (exists($accession_hash{$accession}))) {
		$line_header = $accession_hash{$accession};
		}
	else {
		$line_header = $accession;
		}	
	foreach my $sample (@sorted_headers) {	
		if (exists $counts_hash{$accession}{$sample}) {
			$line .= "$counts_hash{$accession}{$sample}\t";
			}
		else {
			$line .= "0\t";
			}
		}
	print OUT "$line_header\t$line\n";
}

close OUT;

sub get_counts {
	my $in=shift;
	my $formatExcel=shift;
	my $accession=shift;
	my $stuff=shift;
	my %counts_hash = %$stuff;

	open (IN, $in) or die "can't open $in:$!\n";
#	print "on file $in\n";

	my @counts;
	my $header;
	my @headers;
	
	if ($formatExcel) {
		$header = <IN>;
		chomp $header;
		@headers = (split /\t/, $header);
#		print @headers;
		}

	my @finalHeader;
	
	while(<IN>) {
		chomp;
		my @cols = split /\t/;	

		if ($formatExcel) {
			for (my $i=1; $i<$#cols; $i+=4) {
				$counts_hash{$accession}{$headers[$i]} += $cols[$i]-1;
#				print "$headers[$i]\n"
			}
		}
		else {
			for (my $i=0; $i<$#cols; $i+=4) {
				my $index = $i/4;
#				$counts[$index] +=  $cols[$i]-1;
#				$finalHeader[$index] =  $headers[$i];
			}
		}
	}

	return \@counts, \@finalHeader, \%counts_hash;
}

sub print_metadata_fields {
		my @presorted_keys = @{$_[0]};
#		print @presorted_keys;
		foreach my $index (1 .. $#{$metadata_hash{google_name}}) {
			print OUT "$metadata_hash{google_name}[$index]\t";
			foreach my $key (@presorted_keys) {
				$key =~ /counts:(.+)/;
				if (exists $metadata_hash{$1}[$index]) {
					print OUT "$metadata_hash{$1}[$index]\t";
					}
				else {
					print OUT "not_found\t";
					}
				}
			print OUT "\n";
			}
		}

sub usage {
        print "\n\tCalculates per-organism representation in the metatranscriptome from raw transcript counts\n".
            "\n\tUsage: perl RNAseq_reads_per_species.pl -i <> -o <>\n".
                "\t\t-i <folder location of .normalized files>\n" .
                "\t\t-o <output file>\n".
                "\t\t-m <FLAG: add metadata from SAMPLE_METADATA.pm>\n".
                "\t\t-e <FLAG: excel format (recommended)>\n".
                "\t\t-c <FLAG: convert accessions to alternate names from genomecodes.pm>\n\n";
        exit(1);
	}

