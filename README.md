#Gordon Lab RNA-Seq Pipeline version 1.3

#Module load on HTCF
ml rnaseq/1.3

#Retrieve instructions and example input files
retrieve_rnaseq_pipeline_instructions.pl