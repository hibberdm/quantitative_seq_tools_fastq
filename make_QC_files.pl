#!/usr/bin/env perl

use warnings;
use strict;

print_QC_stats();
print_barcode_stats();

sub print_barcode_stats {
	my @f = `ls *.barcode_stats`;
	my @files;
	
	# clean up file list
	for my $f (@f) {
		chomp $f;
		if ($f =~ /\.barcode_stats/) {
			push @files, $f;
		}
	}
	my ($headers, $stats) = retrieve_barcode_stats(\@files);
	print_stats_for_barcode($headers, $stats);
}

sub print_stats_for_barcode {
	my ($headers, $stats) = @_;

	my @keys = sort {$a cmp $b} keys %$headers;
#	my @keys;

	my $text;
		
	for (@keys) {
		$text .= "$_\t";
	}
	chop $text;
	$text .= "\n";
	
	my $outfile = "barcode_stats.tab";
	open (OUT, ">$outfile") or die "can't open $outfile: $!\n";
	for my $s (@$stats) {
		for my $i (0 .. $#keys) {
			my $key = $keys[$i];
			if ($s->{$key}) {
				$text .= "$s->{$key}\t";
			}
			else {
				$text .= "\t";
			}
		}
		chop $text; 
		$text .= "\n";
	}
	print OUT $text;

	close OUT;
}

sub retrieve_barcode_stats {
	my $files = shift;

#	print "reading @$files\n";
	
	my @stats;
	my %all_headers;
	for my $f (@$files) {
		my $res = parse_barcode_stats($f);
	
		for my $k (keys %$res) {
			$all_headers{$k} = 1;
		}
		push @stats, $res;
	}

	return \%all_headers, \@stats;
}

sub parse_barcode_stats {
	my $in = shift;


	open (IN, $in) or die "can't open $in: $@!\n";

	my $basics = <IN>;
	chomp $basics;

	my ($mapped, $total, $fraction_mapped) = split /\t/, $basics;

	my %stats;
	$stats{file} = $in;
	$stats{reads_with_barcodes} = $mapped;
	$stats{read_number} = $total;
	$stats{reads_with_barcodes_fraction} = $fraction_mapped;

#	print "have $in $mapped $total $fraction_mapped\n";

	close IN;

	return \%stats;
}

sub print_QC_stats {
	my @f = `ls *.counts`;
	my @files;
	
	# clean up file list
	for my $f (@f) {
		chomp $f;
		if ($f =~ /\.counts/) {
			push @files, $f;
		}
	}
	my ($headers, $stats) = retrieve_stats(\@files);
	print_stats($headers, $stats);
}



sub print_stats {
	my ($headers, $stats) = @_;

	my @headers = sort {$a cmp $b} keys %$headers;

#	die "headers are @headers\n";
	my $header_line = "file\t";
	for my $h (@headers) {
		$header_line .= "$h\t";
	}
	chop $header_line;
	print "$header_line\n";

	for my $s (@$stats) {
		my $line = "$s->{file}\t";
		for my $h (@headers) {
			if ($s->{$h}) {
				$line .= "$s->{$h}\t";
			}
			else {
				$line .= "\t";
			}
		}
		chop $line;
		print "$line\n";
	}


}

sub retrieve_stats {
	my $files = shift;

	my @all_stats;
	my %all_headers;
	for my $f (@$files) {
		my $stats = grab_stats_from_file($f, \%all_headers);
		push @all_stats, $stats;
	}

	return \%all_headers, \@all_stats;
}

sub grab_stats_from_file {
	my ($file, $all_headers) = @_;

	open (IN, $file) or die "can't open $file: $!\n";

	my %stats;
	$stats{'file'} = $file;
	$stats{'file'} =~ s/\.counts//;
	while (<IN>) {
		chomp;
		if (/#/) {
			next if /^##/;
			my $header_line = $_;
			my $data_line = <IN>;
			$header_line =~ s/^#//;
			$data_line =~ s/^#//;
			chomp $data_line;

			my @headers = split /\t/, $header_line;
			my @data = split /\t/, $data_line;

			if (scalar @headers != scalar @data) {
				printf STDERR "Warning: number of headers and number of data are different (%d vs %d)\n@headers\n@data\n\n", scalar @headers, scalar @data;
			}
			
			for my $i (0 .. $#headers) { 
				my $h = $headers[$i];
#				print "adding $h\t$data[$i]\n";
				$all_headers->{$h} = 1; 
				$stats{$h} = $data[$i];
			}
		}
		elsif (/^chr\d+/) { # count mouse reads
			my @pieces = split /\t/;
			$all_headers->{"mouse_reads"} = 1;
			$stats{"mouse_reads"} += $pieces[1];
		}
		elsif (/^phix/i) { # count mouse reads
			my @pieces = split /\t/;
			$all_headers->{"phiX_reads"} = 1;
			$stats{"phiX_reads"} += $pieces[1];
		}
	}

	return \%stats;
}
