#!/usr/bin/env perl

# note this is pretty slow; just to read such a large file in perl takes a while
# would be hard to gain much more performance in perl, as I've already used a lot of 
# efficiency tricks to make it faster (e.g. use a hash of file handles)
#
# might be worth rewriting in C sometime.....

use IO::Uncompress::AnyUncompress;
use strict;
use warnings;

my $SCARF = 1;
my $SCARF_ASCII = 2;

my $barcode_file = shift;
my $solexa_file  = shift;
my $barcode_out = shift;

$barcode_out = $solexa_file . ".barcode_stats" if $barcode_out;

die "Usage: split_barcodes.pl <barcode_file> <solexa_file> [print_barcode_stats (1 = yes)]\n" unless $barcode_file && $solexa_file;

my ($barcodes, $barcode_to_name) = read_barcodes($barcode_file);

if ($solexa_file =~ /fastq$/) {
	split_fastq($barcodes, $solexa_file, $barcode_out, $barcode_to_name);
}
else {
	split_solexa($barcodes, $solexa_file, $barcode_out, $barcode_to_name);
}

sub split_fastq {
	my ($barcodes, $in, $barcode_out, $barcode_to_name)=@_;
	my $in1 = $in;
	my $in2 = $in;
	$in1 =~ s/\.fastq$/_1.fastq/;
	$in2 =~ s/\.fastq$/_2.fastq/;

#	print "in fastq reading $in1\t$in2\n";



	my $IN1 = new IO::Uncompress::AnyUncompress $in1 or die "can't open $in: $!\n";
	my $IN2 = new IO::Uncompress::AnyUncompress $in2 or die "can't open $in: $!\n";

#	open (IN1, $in1) or die "can't open $in1: $!\n";
#	open (IN2, $in2) or die "can't open $in2: $!\n";
#	open (IN1, $in1) or die "can't open $in1: $!\n";
#	open (IN2, $in2) or die "can't open $in2: $!\n";

	my $num_read = 0;
	my $num_missed = 0;
	my %counts;
	my $format_type = 0;
	my $pe_not_same = 0;

	my $barcode_len = get_barcode_len($barcodes);

	if ($barcode_out) {
		open (OUT, ">$barcode_out") or die "can't open $barcode_out: $!\n";
	}

	while (<$IN1>) {
		my $header1 = $_;	
		chomp $header1;
		my $seq1 = <$IN1>;
		chomp $seq1;
		my $plus1 = <$IN1>;
		chomp $plus1;
		my $quality1 = <$IN1>;
		chomp $quality1;
		my $header2 = <$IN2>;
		chomp $header2;
		my $seq2 = <$IN2>;
		chomp $seq2;
		my $plus2 = <$IN2>;
		chomp $plus2;
		my $quality2 = <$IN2>;
		chomp $quality2;
	
#		$format_type = infer_format_type($_) unless $format_type;	

		my $missed_barcode = 1;
#		my @pieces = split /:/, $_;
#		print "@pieces\n";
		my $bc1 = substr($seq1, 0, $barcode_len);
#		$bc1 =~ s/N$/T/;
		$bc1 =~ s/[ACTGN]$/T/; #last base be flexible because software not accurate there
		#my @pieces2 = split /:/, $line2;
		my $bc2 = substr($seq2, 0, $barcode_len);
		$bc2 =~ s/[ACTGN]$/T/; #last base be flexible because software not accurate there
#		$bc2 =~ s/N$/T/;

#		print "bc\t$bc1\t$bc2\n";
#		die;


		if ($bc1 ne $bc2) { # barcodes have to be the same (if not probably chimera)
			$pe_not_same++;
		#	print "$bc1\t$bc2\n";
		}
		elsif (my $fh = $barcodes->{$bc1}) { # match barcode
			$missed_barcode = 0;
			$counts{$bc1}++;
			$seq1 = substr($seq1, $barcode_len);
			$seq2 = substr($seq2, $barcode_len);
			$quality1 = substr($quality1, $barcode_len);
			$quality2 = substr($quality2, $barcode_len);

#			my @pieces = ($header1,"","","","",$seq1,$quality1);
#			my @pieces2 = ($header2,"","","","",$seq1,$quality1);
			my @pieces = ($num_read,"","","","",$seq1,$quality1);
			my @pieces2 = ($num_read,"","","","",$seq2,$quality2);
			
			my $read1 = join ":", @pieces;
			my $read2 = join ":", @pieces2;
#			print "$seq\n";
			my $fh1 = $fh->[0];
			my $fh2 = $fh->[1];
			print $fh1 "$read1\n";
			print $fh2 "$read2\n";
		}

		$num_missed++ if $missed_barcode;
		$num_read++;
	}
	my $num_matched = $num_read-$num_missed;
	printf STDERR "\n\t\tSUMMARY STATS\n";
	printf STDERR "%d of %d (%.1f%%) of reads had a barcode match\n", $num_matched, $num_read, ($num_matched/$num_read)*100;
	printf STDERR "%d of %d (%.1f%%) of reads had PE barcodes that differed\n", $pe_not_same, $num_read, ($pe_not_same/$num_read)*100;
	printf STDERR "#%d\t%d\t%f\n", $num_matched, $num_read, ($num_matched/$num_read);
	printf OUT "%d\t%d\t%f\n", $num_matched, $num_read, ($num_matched/$num_read) if $barcode_out;
#	printf OUT "%d of %d (%.1f%%) of reads had a barcode match\n", $num_matched, $num_read, ($num_matched/$num_read)*100;
	printf OUT "%d of %d (%.1f%%) of reads had PE barcodes that differed\n", $pe_not_same, $num_read, ($pe_not_same/$num_read)*100;
	
	my @s = sort {$counts{$b} <=> $counts{$a}} keys %counts;
	for my $s (@s) {
		printf STDERR "$s\t%.1f%% ($counts{$s} of $num_matched)\n", ($counts{$s}/$num_matched) * 100;
		if ($barcode_to_name && $barcode_to_name->{$s}) {
			printf OUT "$barcode_to_name->{$s}\t$s\t$counts{$s}\t$num_matched\n", ($counts{$s}/$num_matched) * 100 if $barcode_out;
		}
		else {
			printf OUT "$s\t$counts{$s}\t$num_matched\n", ($counts{$s}/$num_matched) * 100 if $barcode_out;
		}
	}
	close OUT if $barcode_out;
}

sub split_solexa {
	my ($barcodes, $in, $barcode_out, $barcode_to_name)=@_;

	my $in1 = $in;
	my $in2 = $in;
	$in1 =~ s/\.scarf$/_1.scarf/;
	$in1 =~ s/\.txt$/_1.txt/;
	$in2 =~ s/\.scarf$/_2.scarf/;
	$in2 =~ s/\.txt$/_2.txt/;
	open (IN1, $in1) or die "can't open $in1: $!\n";
	open (IN2, $in2) or die "can't open $in2: $!\n";

#	print "input files are $in1\t$in2\n";

	my $num_read = 0;
	my $num_missed = 0;
	my %counts;
	my $format_type = 0;
	my $pe_not_same = 0;

	my $barcode_len = get_barcode_len($barcodes);

	if ($barcode_out) {
		open (OUT, ">$barcode_out") or die "can't open $barcode_out: $!\n";
	}

	while (<IN1>) {
		chomp;
		$format_type = infer_format_type($_) unless $format_type;	

		my $missed_barcode = 1;
		my @pieces = split /:/, $_;
#		print "@pieces\n";
		my $line2 = <IN2>;
		chomp $line2;
		my $bc1 = substr($pieces[5], 0, $barcode_len);
		my @pieces2 = split /:/, $line2;
		my $bc2 = substr($pieces2[5], 0, $barcode_len);
		$bc2 =~ s/N$/T/;
		$bc1 =~ s/N$/T/;

#		print "bc\t$bc1\t$bc2\n";
#		next unless $bc1 eq $bc2; # barcodes have to be the same
#		die;

		if ($bc1 ne $bc2) { # barcodes have to be the same (if not probably chimera)
			$pe_not_same++;
#			print "DIFF\t$bc1|$bc2\n";
		}
		elsif (my $fh = $barcodes->{$bc1}) { # match barcode
#		if (my $fh = $barcodes->{$bc1}) { # match barcode
			$missed_barcode = 0;
			$counts{$bc1}++;
			$pieces[5] = substr($pieces[5], $barcode_len);
			$pieces2[5] = substr($pieces2[5], $barcode_len);
#			print "new piece $pieces[5]\n";
			if ($format_type == $SCARF_ASCII) {
#				print "before $pieces[6]\n";
				$pieces[6]=~ s/^\s*\D{$barcode_len}//;
				$pieces2[6]=~ s/^\s*\D{$barcode_len}//;
#				print "after $pieces[6]\n";
			}
			elsif ($format_type == $SCARF) {
				$pieces[6] =~ s/^\s*([-\d]+\s){$barcode_len}//;
				$pieces2[6] =~ s/^\s*([-\d]+\s){$barcode_len}//;
			}
			
			my $seq = join ":", @pieces;
			my $seq2 = join ":", @pieces2;
#			print "$seq\n";
			my $fh1 = $fh->[0];
			my $fh2 = $fh->[1];
			print $fh1 "$seq\n";
			print $fh2 "$seq2\n";
		}

		$num_missed++ if $missed_barcode;
		$num_read++;
	}
	my $num_matched = $num_read-$num_missed;
	printf STDERR "\n\t\tSUMMARY STATS\n";
	printf STDERR "%d of %d (%.1f%%) of reads had a barcode match\n", $num_matched, $num_read, ($num_matched/$num_read)*100;
	printf STDERR "%d of %d (%.1f%%) of reads had PE barcodes that differed\n", $pe_not_same, $num_read, ($pe_not_same/$num_read)*100;
	printf STDERR "#%d\t%d\t%f\n", $num_matched, $num_read, ($num_matched/$num_read);
	printf OUT "%d\t%d\t%f\n", $num_matched, $num_read, ($num_matched/$num_read) if $barcode_out;
	
	my @s = sort {$counts{$b} <=> $counts{$a}} keys %counts;
	for my $s (@s) {
		printf STDERR "$s\t%.1f%% ($counts{$s} of $num_matched)\n", ($counts{$s}/$num_matched) * 100;
		if ($barcode_to_name && $barcode_to_name->{$s}) {
			printf OUT "$barcode_to_name->{$s}\t$s\t$counts{$s}\t$num_matched\n", ($counts{$s}/$num_matched) * 100 if $barcode_out;
		}
		else {
			printf OUT "$s\t$counts{$s}\t$num_matched\n", ($counts{$s}/$num_matched) * 100 if $barcode_out;
		}
	}
	close OUT if $barcode_out;
}

sub get_barcode_len {
	my $barcodes = shift;

	my @keys = keys %$barcodes;
	my $first = shift @keys;

	my $len = length($first);

	for my $k (@keys) {
		if ($len != length($k)) {
			die "Error: barcodes must all be the same length (found $len and " . length($k)  . " bp\n";
		}
	}

	return $len;
}

sub infer_format_type {
	shift;

	if(/^\S+:\d+:\d+:\d+:\d+.?.?.?.?:\D+$/) {
		return $SCARF_ASCII;
	}
        elsif(/^\S+:\d+:\d+:\d+:\d+/) {
		return $SCARF;
	}

}

sub read_barcodes {
	my $in=shift;
	open (IN, $in) or die "can't open $in: $!\n";

	my @fileHandles;
#	my @barcodes;
	my %barcodes;
	my %seq_to_barcode_name;
	while (<IN>) {
		chomp;
		my (@stuff) = split /\t/;

		my ($barcode_name, $barcode, $filename);
		if (scalar @stuff == 3) {
			($barcode_name, $barcode, $filename) = @stuff;
		}
		else {
			($barcode, $filename) = @stuff;
		}

		my $filename1 = $filename;
		my $filename2 = $filename;
		$filename1 =~ s/\.scarf$/_1.scarf/;
		$filename1 =~ s/\.txt$/_1.txt/;
		$filename2 =~ s/\.scarf$/_2.scarf/;
		$filename2 =~ s/\.txt$/_2.txt/;

#		print "export to $filename1 and $filename2\n";

		# PE will have two files per barcode
		

#		my ($barcode, $filename) = split /\t/;
	#	print "have $barcode $filename\n";
	 	open my $out1, ">$filename1" or die "can't open $filename1: $!\n";
	 	open my $out2, ">$filename2" or die "can't open $filename2: $!\n";
	#	push @fileHandles, $out;
		$barcodes{$barcode}[0] = $out1; # barcode points to the file handle
		$barcodes{$barcode}[1] = $out2; # barcode points to the file handle
		if ($barcode_name) {
			$seq_to_barcode_name{$barcode} = $barcode_name;
		}
	}

	close IN;
	
#	return \@fileHandles, \@barcodes;
	return \%barcodes, \%seq_to_barcode_name;
}
