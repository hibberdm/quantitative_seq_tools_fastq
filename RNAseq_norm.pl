#!/usr/bin/env perl

# normalizes RNA-Seq transcripts to reads per million per kilobase (RPKM)
# change log
# Fri Nov 19 11:58:35 CST 2010	JJF	added upper-quartile normalization as an option
# Mon Jan 09 13:45:00 CST 2016	MCH	added transcripts per kilobase million (TPM) normalization as an option
# Fri June 09 11:25:00 CST 2017 MCH changed default normalization to TPM

use strict;
use warnings;
use Getopt::Long;
use File::Basename;

my $gene_pos;     # file with gene positions
my $normalization_type = "TPM";   # method way to normalize
my $output_format = "excel";
my $gene_cluster_file;
my $outfile;
my $help;
#my $TEMP_GLOBAL_SUM=0;
#my $TEMP_GLOBAL_COUNT=0;

#my $scale = 1000000;

my $skip_length_normalization;
GetOptions (
            "gene_positions=s"   => \$gene_pos,
            "normalization_type=s"     => \$normalization_type,
            "clusters=s"     => \$gene_cluster_file,
            "skip_length_norm"     => \$skip_length_normalization,
            "format=s"     => \$output_format,
	    "outfile=s" => \$outfile,
	    "help" => \$help,
	    "usage" => \$help,
            );  # flag

$skip_length_normalization = 1 if $gene_cluster_file;



if (!$skip_length_normalization && !$gene_pos) {
	usage();
	exit(1);
}
if ($help) {
	usage();

	exit(0);
}

#my @files = `ls *CDS.counts *CDS.COUNTS *.counts`;
my @files = `ls *.counts`; #MCH#
my $basename = basename($gene_pos, ".gene_pos");
my $gene_lengths;
my $gene_clusters;

$gene_lengths = get_gene_lengths($gene_pos) if $gene_pos;
$gene_clusters = get_gene_clusters($gene_cluster_file) if $gene_cluster_file;

my @exps;

my %allGenes;
for my $f (@files) {
	chomp $f;
	next if $f =~ /^\s*$/;

	push @exps, read_RNA_count_file($f, \%allGenes, $gene_lengths, $gene_clusters);
}

print STDERR "On Accession: $basename\n";
my @sequencedGenes = sort {$a cmp $b } keys %allGenes;
print STDERR "\tHave " . scalar @sequencedGenes . " sequenced genes\n";

for my $i (0 .. $#exps) {
	scale_to_absolute_num(\@sequencedGenes, $exps[$i], $normalization_type, $gene_lengths);
}

print_expression_matrix(\@sequencedGenes, \@exps, $skip_length_normalization, $output_format, $outfile, $normalization_type);


#printf STDERR "mean upper quartile post Read-per-million %f", $TEMP_GLOBAL_SUM/$TEMP_GLOBAL_COUNT;
# 143.092087
# 149.761577
# 139.052789
# based on this I'd say let's scale to 150


sub print_expression_matrix {
	my ($genes, $exps, $skip_length_norm, $format, $outfile, $norm_type) = @_;

	my $fh_out = *STDOUT; # default to stdout
	if ($outfile) {
		open ($fh_out, ">$outfile") or die "can't open $outfile: $!\n";
	}

	# print the header
	my $out_genes;
	if ($format =~ /excel/i) {
		my $text = "gene\t";
		for my $e (@$exps) {
			my $f = $e->{filename};
			$f = clean_file_name($f);
			if ($skip_length_norm) {
				$text .= "counts:$f\tstatus:$f\tscale:$f\t";
			}
			elsif ($norm_type =~ /^TPM/) {
				$text .= "counts:$f\tstatus:$f\trpk:$f\ttpm_scale:$f\t";
				}
			else {
				$text .= "counts:$f\tstatus:$f\tscale:$f\tsize_scale:$f\t";
			}
		}
		chop $text;
		print $fh_out "$text\n";
	}
	elsif ($format =~ /matlab/i) {
		my $text = "";
		my $out = "conditions";
		$out = $outfile . ".conditions" if $outfile;

		open (CONDITIONS, ">$out") or die "can't open $out: $!\n";
		for my $e (@$exps) {
			my $f = $e->{filename};
			$f = clean_file_name($f);
			if ($skip_length_norm) {
				$text .= "counts:$f\nstatus:$f\nscale:$f\n";
			}
			elsif ($norm_type =~ /^TPM/) {
				$text .= "counts:$f\tstatus:$f\trpk:$f\ttpm_scale:$f\t";
				}
			else {
				$text .= "counts:$f\nstatus:$f\nscale:$f\nsize_scale:$f\n";
			}
		}
		chop $text;
		print CONDITIONS "$text\n";
		close CONDITIONS;

		$out_genes = "genes";
		$out_genes = $outfile . ".genes" if $outfile;
	}
	else {
		die "Unrecognized format: $format\n";
	}

	if ($format =~ /matlab/) {
		open (GENES, ">$out_genes") or die "can't open $out_genes: $!\n";
	}

	for my $g (@$genes) {
		my $clean_gene_name = clean_gene_name($g);
#		print STDERR "$clean_gene_name\n";
		my $text = "";
		if ($format =~ /excel/i) { $text .= "$clean_gene_name\t"; }
		elsif ($format =~ /matlab/i) { print GENES "$clean_gene_name\n"; }


		for my $e (@$exps) {
#			$text .= "|$e->{$g}{count}|$e->{$g}{status}|$e->{$g}{scale}|$e->{$g}{size_scale}|^";
			$e->{$g}{status} = status_to_binary($e->{$g}{status}) if ($format =~ /matlab/);

			if ($skip_length_norm) {
				$text .= "$e->{$g}{count}\t$e->{$g}{status}\t$e->{$g}{scale}\t";
			}
			elsif ($norm_type =~ /^TPM/) {
				$text .= "$e->{$g}{count}\t$e->{$g}{status}\t$e->{$g}{rpk}\t$e->{$g}{tpm_scale}\t";
				}
			else {
				$text .= "$e->{$g}{count}\t$e->{$g}{status}\t$e->{$g}{scale}\t$e->{$g}{size_scale}\t";
			}
		}
		chop $text;

		print $fh_out "$text\n";
	}

}

sub status_to_binary {
	my $status = shift;

	if ($status eq 'A') {
		return 0;
	}
	elsif ($status eq 'P') {
		return 1;
	}
	else {
		die "in correct status ($status); should be A or P\n";
	}

}

sub clean_file_name {
	my $name = shift;
	$name =~ s/\.counts$//;
	$name =~ s/\.mappedCDS$//;
	$name =~ s/\.mapped$//;

	return $name;
}

sub upper_quartile {
	my (@p) = @_;

	my $med = median(@p);
#	print STDERR "median is $med\n";

	my @sorted = sort { $a <=> $b } @p;

	my @upper_set;
	my $num=0;
	if( (@sorted % 2) == 1 ) { # odd number of elements
		$num = (scalar @sorted+1)/2;
		#if ($sorted[$num-1] != $sorted[$num]) {
		#print STDERR "med is $sorted[$num-1]\n";
		#print STDERR "past med is $sorted[$num]\n";
		#}
#		$ret = $pole[((@pole+1) / 2)-1];
	} else {
		$num = (scalar @sorted)/2;
#		print STDERR "even med is $sorted[$num-1]\n";
#		print STDERR "even past med is $sorted[$num]\n";
#		$ret = ($pole[(@pole / 2)-1] + $pole[@pole / 2]) / 2;
	}
	for my $i ($num .. $#sorted) {
		push @upper_set, $sorted[$i];
	}

	my $quartile = median(@upper_set);

#	print STDERR "upper quartile is $quartile split at $num min is $upper_set[0] max is $upper_set[$#upper_set]\n";


	return $quartile;
}

sub median {
    my (@p) = @_;

    my $ret;

    my @pole = sort { $a <=> $b } @p;

    if( (@pole % 2) == 1 ) {
        $ret = $pole[((@pole+1) / 2)-1];
    } else {
        $ret = ($pole[(@pole / 2)-1] + $pole[@pole / 2]) / 2;
    }

    return $ret;
}

sub trimmed_mean {

	print "\n\nEntered trimmed mean sub\n\n";

	my ($vec, $percent_trim) = @_;

	my $size = scalar @$vec;

	my $trim_amount = round($size*$percent_trim);

	my @vec_sort = sort {$a <=> $b} @$vec;

	# calculate a mean after trimming of the top and bottom genes
	print STDERR "Skipping $trim_amount of $size for mean trim\n";
	my $sum = 0;
	my $count = 0;

	for my $i ($trim_amount .. $#vec_sort - $trim_amount) {
		$sum+=$vec_sort[$i];
		$count++;
	print "Sum = $sum, Count = $count\n";
	}

	return $sum/$count;
}

sub round {
	my $num = shift;

	return int ($num+0.5);
}


sub get_gene_lengths {
	my $in = shift;

	open (IN, $in) or die "can't open $in: $!\n";

	my %gene_to_len;
	while (<IN>) {
		chomp;
		my ($name, $start, $stop) = split /\t/;

		$gene_to_len{$name} = $stop-$start;

#		printf "$name is %d\n", $stop-$start;
	}
	close IN;

	return \%gene_to_len;
}

sub scale_to_absolute_num {
        my ($genes, $exp, $norm_type, $gene_lengths) = @_;

        my $pseudocount = 1;
        my $gene_len_scale = 1000; # 1KB

        # get total counts
        my $sum = 0;
        my $rpk_sum = 0;
        my @values;
        for my $g (@$genes) {
                my $clean_gene_name = clean_gene_name($g);
                if ($exp->{$g}{count}) {
                        $exp->{$g}{count} += $pseudocount;
                		if ($gene_lengths && $gene_lengths->{$clean_gene_name}) {
                			$exp->{$g}{rpk} = sprintf("%.3f", ($exp->{$g}{count}/$gene_lengths->{$clean_gene_name}) * $gene_len_scale);
                			}
                }
                else {
                        $exp->{$g}{count} = $pseudocount;
                        $exp->{$g}{status} = 'A'; # p for present, a for absent
                		if ($gene_lengths && $gene_lengths->{$clean_gene_name}) {
                			$exp->{$g}{rpk} = sprintf("%.3f", ($exp->{$g}{count}/$gene_lengths->{$clean_gene_name}) * $gene_len_scale);
                			}
                }

                $sum += $exp->{$g}{count};
                $rpk_sum += $exp->{$g}{rpk};
                push @values, $exp->{$g}{count};
        }

        my $scaleFactor;

        if ($norm_type =~ /^m/) { # mean_trim
                my $med = median(@values);
                my $mean_trim = trimmed_mean(\@values, 0.02); # cut off top and bottom 2%
                my $mean_trim_scale = 200;   # have mean value = 200
                $scaleFactor = $mean_trim_scale/$mean_trim;
                printf STDERR "scaled with mean trim\n$exp->{filename} total counts (including pseudocounts) is $sum scale factor is %f   median is $med    mean trim is $mean_trim\n", $scaleFactor;
                }

        elsif ($norm_type =~ /^upper/) { # upper quartile
                my $quartile = upper_quartile(@values);
                my $scale = 150;  # want the upper-quartile to be set at 150; this keeps the data on roughly the same scale that we saw with RPKM
                if ($quartile != 0) {
                        $scaleFactor = $scale/$quartile;  # CPM = counts per million
                }
                else {
                        $scaleFactor = 0; # bug fix from Nate to fix division by 0 when no gene present
                }

#               printf STDERR "upper-quartile is $quartile scaled to $scale with scale factor $scaleFactor\n";

        }
        else {
                my $scale = 1000000;

                if ($norm_type =~ /^TPM/) {
                	if ($rpk_sum != 0) {
                		$scaleFactor = $scale/$rpk_sum;  # TPM = Transcripts per Kilobase Million
                		}
                	else {
                		$scaleFactor = 0;  # TPM = Transcripts per Kilobase Million
                		}
                	}
                else {
                	if ($sum != 0) {
                        $scaleFactor = $scale/$sum;  # CPM = counts per million
                		}
                	else {
                        $scaleFactor = 0; # bug fix from Nate to fix division by 0 when no gene present
                		}
        			}
        }

#        my @temp_values;
        # scale the gene counts
        for my $g (@$genes) {
			$exp->{$g}{scale} = sprintf("%.3f", $exp->{$g}{count} * $scaleFactor);
			if ($norm_type =~ /^TPM/) {
				$exp->{$g}{tpm_scale} = sprintf("%.3f", $exp->{$g}{rpk} * $scaleFactor);
				}

#			push @temp_values, $exp->{$g}{scale}; # just try to figure out roughly what number to scale by to make upper-quartile "look" like RPKM

			my $clean_gene_name = clean_gene_name($g);

			# now scale by the length of the gene, if the gene lengths hash exists
			if ($gene_lengths && $gene_lengths->{$clean_gene_name}) {
		        $exp->{$g}{size_scale} = sprintf("%.3f", ($exp->{$g}{scale}/$gene_lengths->{$clean_gene_name}) * $gene_len_scale);
				}
#               print "have clean $clean_gene_name len $gene_lengths->{$clean_gene_name}\n";
			}

#       $TEMP_GLOBAL_COUNT++;
#       $TEMP_GLOBAL_SUM+=upper_quartile(@temp_values);
	}


sub clean_gene_name {
	my $name = shift;
	$name =~ s/-CDS$//;
#	$name =~ s/_//;
#	print "name is $name\n";
	$name =~ s/\|.*//; # for 100 genomes names

	if ($name =~ /Bryfor(\d+)/) {  # bryantella format is weird
		my $num = $1;
		while (length($num) < 4) {
			$num = '0' . $num;
		}
		$name = "Bryfor$num";
	}
#	print "fixed name is $name\n";

	return $name;
}



sub read_RNA_count_file {
	my $file = shift;
	my $allGenes = shift;
	my $possibleGenes = shift;
	my $gene_to_cluster = shift;


	open (IN, $file) or die "can't open $file: $!\n";
	my %counts;
	while (<IN>) {
		chomp;
		next if /^#/ || /^feature_name/;

		my ($gene, $count, @stuff) = split /\t/;
		my $clean_g = clean_gene_name($gene);
		my $temp_g = $clean_g;
		print "can't find cluster for $clean_g\n" if $gene_to_cluster && !$gene_to_cluster->{$clean_g};
		$clean_g = $gene_to_cluster->{$clean_g} if $gene_to_cluster && $gene_to_cluster->{$clean_g};
#		print "gene is $clean_g\n";
		if (!$possibleGenes) { # keep everything, don't limit based on the gene.position file
			if ($gene_to_cluster) {
				if ($gene_to_cluster->{$temp_g}) { # only keep things with a cluster
					$counts{$clean_g}{count} = $count;
					$counts{$clean_g}{status} = 'P'; # p for present, a for absent
					$allGenes{$clean_g} = 1;
				}
			}
			else {
				$counts{$clean_g}{count} = $count;
				$counts{$clean_g}{status} = 'P'; # p for present, a for absent
				$allGenes{$clean_g} = 1;
			}
		}
		else {
			if ($possibleGenes->{$clean_g}) {
				$counts{$clean_g}{count} = $count;
				$counts{$clean_g}{status} = 'P'; # p for present, a for absent
				$allGenes{$clean_g} = 1;
			}
			else {
#				print STDERR "skipping $gene\t$clean_g (not in list)\n";
			}
		}

#		print "have $gene\t$count\n";
	}
	$counts{filename} = $file;
	close IN;

	return \%counts;
}

sub get_gene_clusters {
	my $infile = shift;

	open (IN, $infile) or die "can't open $infile: $!\n";

	my %gene_to_cluster;
	while (<IN>) {
		chomp;
		my ($gene, $cluster) = split /\t/;
#		$cluster .= "_C";
		$gene_to_cluster{$gene} = $cluster;
	}

	return \%gene_to_cluster;
}

sub usage {
	print "\n\tUsage: RNAseq_norm.pl\n".
		"\t\t-n <normalization_type (counts_per_million OR mean_trim OR upper_quartile OR TPM [default])>\n" .
		"\t\t-g <gene_positions_file (required unless using options -s or -c)>\n".
		"\t\t-o [outfile (default STDOUT)]\n".
		"\t\t-s (skip length normalization)\n".
		"\t\t-f [format (excel OR matlab); default excel]\n".
		"\t\t-c [cluster_file]\n\n";
}
