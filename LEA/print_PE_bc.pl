#!/usr/bin/env perl

# what does this do?
# 1) grabs the random barcodes and the 16S sequences associated with them
# 2) outputs a table of the format:
#     BC	N	M1:seqA1^seqB1|M:seqA:seqB,etc...
#     where BC is the random barcode, N is the number of observations of that barcode, M1 is the number of observations of seqA1^seqB1 within the reads for BC
#    the file is sorted from most abundant barcode to least abundant

# barcodes 13-22 (this is the reverse complement of what the were in the original
# TACGATC GATCTGC CTCGTCA GGGTATT TCTTTTG TTCCGAA CTATGAC GTTGGGT ACATGTT CCATAGG TAAACCT TGCAACC

# TO DO
# 1) adding fuzzy matching for the primer would increase number of usable hits
# 2) most mutations occur at the end of the second paired-end read;  maybe trim that?

use strict;
use warnings;
#use String::Approx 'asubstitute';

my $in1=shift;
my $in2=shift;
my $bc = shift;
my $region=shift;
my $easy_to_read = shift;
my $front_len = 63; # should be 63-65 for MP
my $back_len = 79; # should be 9=79-81 for MP

#my $len_check = "length_check.txt";
#open(LEN, ">$len_check");


die "Usage: print_random_bc.pl <scarf_file_PE1> <scarf_file_PE2> <bc> [easy_to_read]\n" unless $in1 && $in2 && $bc;

open (IN, $in1) or die "can't open $in1: $!\n";
open (IN2, $in2) or die "can't open $in2: $!\n";
my $log = "$in1.log";

open (LOG, ">$log") or die "can't open $log: $!\n";


my $primer = 'AGAGTTTGATCCTGGCTCAG';
my $primer338R = 'TGCTGCCTCCCGTAGGAGT';
my %counts;
my %seqs;

$region = "" unless $region;

if ($region =~ /v4v5_1/i) {
	$primer338R = 'GGACTAC[ACG][GC]GGGTATCTAAT';
	$primer = 'GTGCCAGC[AC]GCCGCGGTAA';
	$front_len = 64; # should be 63-65 for MP
	$back_len = 79; # should be 9=79-81 for MP
}
elsif ($region =~ /v4v5_2/i) {
	$primer338R = 'GGACTACA[AG]GGTATCTAAT';
	$primer = 'GTGCCAGC[AC]GCCGCGGTAA';
	$front_len = 64; # should be 63-65 for MP
	$back_len = 80; # should be 9=79-81 for MP
}
elsif ($region =~ /v4v5_a/i) {
	$primer338R = 'GGACTACCAGGGTATCTAATCC';
	$primer = 'GTGCCAGCAGCCGCGGTAA';
	$front_len = 64; # should be 63-65 for MP
#	$back_len = 79; # should be 9=79-81 for MP
	$back_len = 77; # should be 9=79-81 for MP
}
elsif ($region =~ /v4v5_c/i) {
	$primer338R = 'GGACTAC[ACT][ACG]GGGTATCTAATCC';
	$primer = 'GTGCCAGCAGCCGCGGTAA';
	$front_len = 64; # should be 63-65 for MP
#	$front_len = 62; # should be 63-65 for MP
	#$back_len = 79; # should be 9=79-81 for MP
	$back_len = 77; # should be 9=79-81 for MP
}
elsif ($region =~ /v5v6/i) {
	$primer338R = 'CCATGCA[ACGT]CACCT';
	$primer = 'GGATTAGATACCC';
	$front_len = 70; # should be 63-65 for MP
	$back_len = 86; # should be 9=79-81 for MP
}
elsif ($region =~ /v4v8/i) {
	$primer338R = 'GCCAGCAGCCGCGGTAA';
	$primer = 'GACGGGCGGTGTGT[AG]CA';
	$front_len = 66; # should be 63-65 for MP
	$back_len = 82; # should be 9=79-81 for MP
}

my $too_short_count = 0;
my $PE1_ok = 0;
my $PE2_ok = 0;
while (<IN>) {
	my @PE1 = split /:/, $_;
	my $PE2 = <IN2>;
	my @PE2 = split /:/, $PE2;
#	die "$PE[5]\n";
	
#	print "$1\n" 
#	my $temp = $PE[5];
#	if (/:$bc(.*)$primer(.*):/) {
	my $seqA = $PE1[5];
#	die "have $seqA\n";
	if ($seqA =~ /^([ACTG]{12,20})$primer([ACTG]+)$bc$/) {
#	if ($seqA =~ /$bc$/) {
#		print STDERR "have $bc\n";
		my $rand_bc = $1;
		$seqA = $2;

#		print STDERR "have $bc\t$rand_bc\t$seqA\n";
#		print "$hit $bc
		# make all output seqs be the same size
		if (length($seqA) < $front_len) {
			$too_short_count++;
			next; 
		}
		$PE1_ok++;

		# truncate to standard length for the region (i.e. remove phasing)
		$seqA = substr($seqA, 0, $front_len);
#		print STDERR "havE $bc\t$rand_bc\t$seqA\n";

		my $seqB = $PE2[5];
		if ($seqB =~ /^[ACTG]{0,3}$primer338R/) {
			$PE2_ok++;
			$seqB =~ s/^[ACTG]{0,3}$primer338R//;
                        # make all output seqs be the same size (i.e. remove phasing)
#			printf STDERR "%d\n", length($seqB);
			$seqB = substr($seqB, 0, $back_len);
	
#			printf LEN "%d:%d\n", length($seqA),length($seqB);
	#		printf STDERR "%d:%d\n", length($seqA),length($seqB);

			my $seq = $seqA . "^$seqB";
			$counts{$rand_bc}++ ;
			if ($seqs{$rand_bc}) {
				$seqs{$rand_bc}{$seq}++;
			}
			else {
				my %seq_count;
				$seq_count{$seq}++;
				$seqs{$rand_bc} = \%seq_count;
			}
		}
	}
}
	
close IN;
close IN2;
print STDERR "PE1 ok = $PE1_ok\tPE2 ok = $PE2_ok\tskipped $too_short_count that were too short\n";
print LOG "PE1 ok = $PE1_ok\tPE2 ok = $PE2_ok\tskipped $too_short_count that were too short\n";

my @keys = sort { $counts{$b} <=> $counts{$a} } keys %counts;

for my $k (@keys) {
	print "===\t" if $easy_to_read;
	print "$k\t$counts{$k}\t";

	my $seq_counts = $seqs{$k};

	my @seqs = sort { $seq_counts->{$b} <=> $seq_counts->{$a} } keys %$seq_counts;
	my $text = "";

	for my $s (@seqs) {
		if ($easy_to_read) {
			$text .= "\n$seq_counts->{$s}\t$s";
		}
		else {
			$text .= "$seq_counts->{$s}:$s|";
		}
	}
	chop $text;

	print "$text\n";
}

