#!/usr/bin/env perl
use strict;
use warnings;
my @files = `ls *.LEA`;

print "#file\tnum_randBC\tnumReads\treads_per_BC\tnum_reads_at_least4_counts\tnum_reads_at_least8_counts\tconsensus_majority\tconsensus\tmajority\tCM_reads\tCM_FP\tCM_TP\tCM_FN\tCM_10reads\tCM_10FP\tCM10_TP\tCM10_FN\tCM100_reads\tCM100_FP\tCM100_TP\tCM100_FN\tC_reads\tC_FP\tC_TP\tC_FN\tC10_reads\tC10_FP\tC10_TP\tC10_FN\tM_reads\tM_FP\tM_TP\tM_FN\tM10_reads\tM10_FP\tM10_TP\tM10_FN\n";
for my $f (@files) {
	chomp $f;

	next if $f =~ /^\s*$/;
	print_run_stats($f);
}

sub print_run_stats {
	my $in=shift;

	open (IN, $in) or die "can't open $in: $!\n";
	my $num_randBC = 0;
	my $num_reads = 0;
	my $reads_per_BC = 0;
	my $reads_at_least_4 = 0;
	my $reads_at_least_8 = 0;
	

	while (<IN>) {
		chomp;	
		$num_randBC++;

		my ($BC,$count,$seqs) = split /\t/;
		$num_reads += $count;
		if ($count >= 4) {
			$reads_at_least_4++;
			if ($count >= 8) {
				$reads_at_least_8++;
			}
		}
	#	$_ = $seqs;
	#	my $seqs_per_BC = tr/\|//;
	#	$reads_per_BC += $seqs_per_BC

	#	die;
	}
	close IN;

	my $consensus = $in;	
	$consensus =~ s/_LBC\d+.LEA/.consensus.fna/;
	print STDERR "consensus file $consensus\n";
	
	if (-e $consensus) {
		my ($CM,$C,$M) = count_consensus_reads($consensus);
		printf "$in\t$num_randBC\t$num_reads\t%f\t%d\t%d\t$CM\t$C\t$M", $num_randBC/$num_reads, $reads_at_least_4, $reads_at_least_8;
	}
	else {
		printf "$in\t$num_randBC\t$num_reads\t%f\t%d\t%d", $num_randBC/$num_reads, $reads_at_least_4, $reads_at_least_8;
	}

	if ($in =~ /(\d\dG)_spike/) {
		my $perfect_file = "perfect" . $1 . ".fna";	

		if (-e $perfect_file) {
			my $CM_file = $consensus;
			my $C_file = $consensus;
			my $M_file = $consensus;

			$CM_file =~ s/\.fna/\.CM.fna/;
			$C_file =~ s/\.fna/\.C.fna/;
			$M_file =~ s/\.fna/\.M.fna/;
			
			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 25);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 50);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 250);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 500);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 2500);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 10000);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 25000);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 50000);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 100000);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 5000);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 2);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 5);
			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 10);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 25);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 50);
			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 100);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 500);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 1000);
#			print "\t" . join "\t",  get_performance_stats($perfect_file, $CM_file, 5000);
			print "\t" . join "\t",  get_performance_stats($perfect_file, $C_file);
			print "\t" . join "\t",  get_performance_stats($perfect_file, $C_file, 10);
			print "\t" . join "\t",  get_performance_stats($perfect_file, $M_file);
			print "\t" . join "\t",  get_performance_stats($perfect_file, $M_file, 10);
#			get_performance_stats($perfect_file, $CM_file);
#			my ($FP, $TP, $FN) = get_performance_stats($perfect_file, $C_file);
#			my ($FP, $TP, $FN) = get_performance_stats($perfect_file, $M_file);
		}
#		die "have perfect $perfect_file\n";
	}

	print "\n";
}

sub get_performance_stats {
	my ($perfect_file, $test_file, $cutoff) = @_;

	$cutoff = 0 unless $cutoff;

	my ($perfect_reads, $total_perfect_reads) = fasta_to_hash($perfect_file);
	my ($test_reads, $total_test_reads) = fasta_to_hash($test_file);
		
	my $TP = 0; 
	my $FP = 0;
	my $FN = 0;
	my $test_reads_count = 0;
	for my $s (keys %$test_reads) {
		next if ($test_reads->{$s} < $cutoff); 

		$test_reads_count+=$test_reads->{$s};

		if ($perfect_reads->{$s}) {
			$TP++;
		}
		else {
			$FP++;	
		}
	}

	my @total = keys %$perfect_reads;
	my $num_reads = scalar @total;
	$FN = $num_reads - $TP;

#	die "FP $FP\tTP $TP\tFN $FN\n";

	return $test_reads_count, $FP, $TP, $FN;
}

sub fasta_to_hash {
	my $in=shift;

	open (SEQ, $in) or die "can't open $in: $!\n";

	my %seqs;
	my $seq;
	my $header;
	my $total_reads = 0;
	while (<SEQ>) {
		chomp;
		next if /^\s*$/;
		if (/^>/) {
			if ($header) {
				$seqs{$seq}++;
				$total_reads++;
			}
			
			$header = $_;
			$seq = "";	
		}
		else {
			$seq .= $_;
		}
	}
	close SEQ;

	if ($header) {
		$seqs{$seq}++;
		$total_reads++;
	}

	return \%seqs, $total_reads;
}

sub count_consensus_reads {
	my $in=shift;

	open(IN, $in) or die "can't open $in: $!\n";

	my ($CM,$C,$M) = (0,0,0);

	while (<IN>) {
		if (/^>CM\|/) {
			$CM++;
		}
		elsif(/^>M\|/) {
			$M++;
		}
		elsif(/^>C\|/) {
			$C++;
		}
	}


	return $CM,$C,$M;
}

