#!/usr/bin/env perl

use strict;
use warnings;

my $fasta=shift;

my $use48spike = shift;

# number of times seen reads in imperfect or no-hit is the threshold for what is "real"
# ABOVE is NOT RIGHT;  need to count and see how many unique sequences there are in imperfect or no-hit;
# they all look unique now because I'm using the random barcode
# need to run with M+CM and C+CM


my ($fasta1, $fasta2) = split_ends($fasta);
run_bowtie($fasta1,$fasta2, $fasta);


sub run_bowtie {
	my ($PE1, $PE2, $basename) = @_;
	my $outPE = "$basename.bowtie";
	my $outPEnoHit = "$basename.NoBowtieHit";


#	my $cmd1 = "blastall -a 2 -i $PE1 -d 32G_spike1.fna -p blastn -m 8 > $outPE1";
	my $cmd = "bowtie 32G_spike1 -1 $PE1 -2 $PE2 -f -v3 -X 800 -a --un $outPEnoHit -y --chunkmbs 300 > $outPE";
	
	if ($use48spike) {
		$cmd = "bowtie 48Gspike1 -1 $PE1 -2 $PE2 -f -v3 -X 800 -a --un $outPEnoHit -y --chunkmbs 300 > $outPE";
	}

	system($cmd);


	parse_bowtie($outPE);
}

sub parse_bowtie {
	my $in=shift;

	my $fh;
	open ($fh, $in) or die "can't open $in: $!\n";

	my $previous = "";
	my @hit_series;
	my $hit_count = 0;
	my %perfect_hits = ();;
	my %imperfect_hits = ();
	while (my @hit = get_hit($fh)) {
#		print "|@hit|\n";
		my ($query, $target, $score, $maxScore, $seqA, $seqB) = @hit;

#		if ($query eq $previous || $hit_count==0) {
		if ($query eq $previous) {
			push @hit_series, [$query, $target, $score, $maxScore, $seqA, $seqB];
#			print "adding $hit_count=>$query, $target, $score, $maxScore\n";
		}
		else {
			process_hits(\@hit_series, \%perfect_hits, \%imperfect_hits) if scalar @hit_series >= 1;
			@hit_series = ();
			push @hit_series, [$query, $target, $score, $maxScore, $seqA, $seqB];
		}
		$previous = $query;
		$hit_count++;
	}

	close $fh;
	process_hits(\@hit_series, \%perfect_hits, \%imperfect_hits);

	print_hits(\%perfect_hits, "$in.perfect");
	print_hits(\%imperfect_hits, "$in.imperfect");
#	process_hits(\@hit_series);

#	process_new(\@hit_series);
}

sub print_hits {
	my ($hits, $out) = @_;

	open (OUT, ">$out") or die "can't open $out: $!\n";

	my @k = sort {$hits->{$a} <=> $hits->{$b}} keys %$hits;

	for my $k (@k) {
		print OUT "$hits->{$k}\t$k\n";
	}
	close OUT;
}

sub process_hits {
	my $hits = shift;
	my $perfect = shift;
	my $imperfect = shift;

	my @sorted_hits = sort {$b->[2] <=> $a->[2]} @$hits;

#	print "have " . scalar @sorted_hits . " hits\n";	

	my @tie;

	
        my $top_hit = shift @sorted_hits;
        my $top_score = $top_hit->[2];
#	print "P:$top_hit->[0]\t$top_hit->[1]\t$top_hit->[2]\t$top_hit->[3]\n";
	my $percent_match = $top_hit->[2]/$top_hit->[3];

        push @tie, $top_hit;
        for my $h (@sorted_hits) {
                if ($h->[2] == $top_score) {
                        push @tie, $h;
                }
        }

	my %unique;
	for my $t (@tie) {
#		$unique{$t->[1]} = sprintf("$t->[1]\t$t->[0]\t%d\t%.2f", $t->[2], $t->[2]/$t->[3]);
		if ($percent_match == 1.0) {
			# default
#			$unique{$t->[1]} = sprintf("$t->[1]\t%d\t%.2f", $t->[2], $t->[2]/$t->[3]);
			# to obtain the perfect sequences
			$unique{$t->[1]} = sprintf("$t->[1]\t%d\t%.2f\t$t->[4]\t$t->[5]", $t->[2], $t->[2]/$t->[3]);
		}
		else {
			# show where they came from
			#$unique{$t->[1]} = sprintf("$t->[1]\t$t->[0]\t%d\t%.2f\t$t->[4]\t$t->[5]", $t->[2], $t->[2]/$t->[3]);
			# masks where they came from but compresses same sequences
                	#return $query, $target, $score, $maxScore, $seq, $PEseq;
			$unique{$t->[1]} = sprintf("$t->[1]\t%d\t%.2f\t$t->[4]\t$t->[5]", $t->[2], $t->[2]/$t->[3]);
		}
#		$text .= join ":", @$t;
#		$text .= "\t";
	}

#	my @k = sort {$b cmp $a} keys %unique;
	my @k = sort {$b cmp $a} keys %unique;
	my $num_unique = scalar @k;
#	print "have $num_unique\t@k\n";


	my $k = shift @k;
	my $text = "$unique{$k}\t$num_unique";
	for my $k (@k) {
#		$unique{$k} =~ s/\t/:/g;	
		$text = "$k:$text";
	}

#	for my $k (

	if ($percent_match == 1.0) {
		$perfect->{$text}++;
	}
	else {
		$imperfect->{$text}++;
	}


#	print "$text\n";
	return $text;
}

sub get_hit {
	my $fh = shift;

	while (<$fh>) {
                chomp;
                my ($query, $orientation, $target, $dstart, $seq, $qual, $M_res, $mismatches) = split /\s+/, $_;

		$query =~ s/\/\d//; # get rid of the extra bit that bowtie adds
                my $score = length($seq);
                my $len = $score;
                if ($mismatches) {
#			print "here |$mismatches|\n";
                        $score -= 1; # at least one mismatch
                        my $comma_count = $mismatches =~ tr/,/,/;
                        $score -= $comma_count;  # mismatches are comma separated
#                       print "have $score with commas $comma_count\n";
                }
#               print "have $score\n";
                $dstart++;  # bowtie is 0-based I want 1-based
                my $dstop = $dstart + ($len-1);

		# PE from bowtie are on two separate lines
                $_ = <$fh>;
                my ($PEquery, $PEorientation, $PEtarget, $PEstart, $PEseq, $PEqual, $PEM_res, $PEmismatches) = split /\s+/, $_;
                my $PEscore = length($PEseq);
                my $PElen = $PEscore;
#		print "l,PE:$len\t$PElen\n";
                if ($PEmismatches) {
#			print "herePE |$PEmismatches|\n";
			$PEscore -= 1; # at least one mismatch
			my $PEcomma_count = $PEmismatches =~ tr/,/,/;
			$PEscore -= $PEcomma_count;  # mismatches are comma separated
#                       print "have $score with commas $comma_count\n";
		}
		$score += $PEscore;

		my $maxScore = $len + $PElen;
#		print "s:$score\tm:$maxScore\n";
                return $query, $target, $score, $maxScore, $seq, $PEseq;
	}

	return;
}

sub split_ends {
	my $fasta = shift;
	my $fasta1 = "$fasta.PE1";
	my $fasta2 = "$fasta.PE2";
	open (PE1, ">$fasta1") or die "can't open $fasta1: $!\n";
	open (PE2, ">$fasta2") or die "can't open $fasta2: $!\n";

	open (IN, $fasta) or die "can't open $fasta: $!\n";
	
	my $header;
	my $seq;
	while (<IN>) {
		chomp;
		next if /^\s*$/;

		if (/^>/) {
			if ($header) {
				my ($seqA, $seqB) = split /\^/, $seq;

				print PE1 "$header\n$seqA\n";
				print PE2 "$header\n$seqB\n";
			}
			$header = $_;
			$seq = "";
		}
		else {
			$seq .= $_;
		}

	}
	close IN;

	if ($header) {
		my ($seqA, $seqB) = split /\^/, $seq;

		print PE1 "$header\n$seqA\n";
		print PE2 "$header\n$seqB\n";
	}
	close PE1;
	close PE2;


	return $fasta1, $fasta2;
}
