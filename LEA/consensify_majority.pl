#!/usr/bin/env perl

# figure out a decent set of thresholds to start with
# optimize the parameters to give the best quantitative accuracy on the spike-ins then test on the remaining spike in sets
# check for chimeras by when the PE1 and PE2 have top hits to two different genomes
# chimeras seem to like to happen within a genus where the 16S reads are only separated by a couple bases anyways


# TO DO
# 3) calculate chimeras for each sequence with uchime 
# 4) switch to clustalw if consensus seems bad

# DONE
# 1) need to write consensus algorithm
# 2) print consensus, majority and label (if consensus==majority indicate;  >C >M >CM

# parameters
# cutoff for random barcodes (1 mismatch, 2 mismatch, 3 mismatch, 4 mismatch)
# cutoff for clusters, which running uclust on sequences for given random barcode
# ratio of largest cluster to second largest cluster for given random barcode
# consensus quality cutoff (e.g. 1/max; 2/max; 90%, etc..)
# weighting function for consensus (sqrt; log; linear?)

# do alignment only when things look funny with the consensus? 
#clustalw2 -quicktree -infile=temp_uc9.best.fa
#
# weight consensus with the square root (log?) of the number of observations

# need a LEA-Seq blast that will run two blasts (one for each end) combine results and scores (sum scores multiply evals?) AND look for chimeras


# write a check for internal chimeras (i.e. within a barcode how often clusters and between what)?

use strict;
use warnings;

my $in=shift;
my $min_reads = shift;
my $con_thresh = shift;
my $detail_thresh = shift;



#open (LOG, ">$in.log.txt");



my $GLOBAL_COUNT = 0;

my $randBC_uniq_threshold = 0.86;

my $MINIMUM_READS = 3;
$MINIMUM_READS = $min_reads if $min_reads;

# lengths front 59 back 82>TAAATGATCCTTCGG|178585|25817
#my $front_len = 40;
#my $front_len = 63; # should be 63-65 for MP
#my $back_len = 79; # should be 9=79-81 for MP

my $consensus_threshold = 2/3;
$consensus_threshold = $con_thresh if $con_thresh;

my $detailed_check_threshold = 2.5;
$detailed_check_threshold = $detail_thresh if $detail_thresh;
#my $back_len = 75;
#my $front_len = 59; # for nonMP
#my $back_len = 82; # for nonMP

die "Usage: perl consensify_q16S.pl <infile>\n" unless $in;

my $rand_BC_keep = check_randBC_for_unique($in, $randBC_uniq_threshold, $MINIMUM_READS);
#my $rand_BC_keep;

open (IN, $in) or die "can't open $in: $!\n";

my $line_count = 0;



while (<IN>) {
	chomp;
	my ($rand_BC, $rand_BC_count, $seqs) = split /\t/;

	print STDERR "on line $line_count\n" if $line_count % 10000 == 0;
	create_consensus($seqs, $rand_BC, $rand_BC_count, $MINIMUM_READS, $rand_BC_keep, $detailed_check_threshold);
#	print "#$rand_BC\t$rand_BC_count\tline$line_count\n";

	$line_count++;
}

sub create_consensus {
	my $seqs = shift;
	my $rand_BC = shift;
	my $rand_BC_count = shift;
	my $min_read_count = shift;
	my $BC_to_keep = shift;
	my $detailed_check_thresh = shift;
	# the software puts a '|' in two places
	my @seq_count = split /\|/, $seqs;
	my @orig = @seq_count;

	if ($BC_to_keep) {
		return if (!$BC_to_keep->{$rand_BC});  # only use unique BCs
	}


	my @hash_counts;
	my %all_bases;

	my $sum = 0;

	if (scalar @seq_count > 1) {
		my $scBest = shift @seq_count;
		my $scSecond = shift @seq_count;
		my ($countBest, $seqBest) = split /:/, $scBest;
		my ($countSecond, $seqSecond) = split /:/, $scSecond;

		#if ($countBest/$countSecond >= 2 && $countBest > 3) {
#		if (($countBest/$countSecond >= 2 && $countBest > 4) || ($countBest > 3 && detailed_check(\@orig) >= 6)) {

		# note need to make a consensus if want to keep this schema
#		if (($countBest/$countSecond >= 3 && $countBest > 4) || ($rand_BC_count > 3 && $countBest > 1 && detailed_check(\@orig) >= 6)) {
#		if ($rand_BC_count > 3 && detailed_check(\@orig) >= 2.5) {
		if ($rand_BC_count > 3 && detailed_check(\@orig) >= $detailed_check_thresh) {
			my ($front, $back) = split /\^/, $seqBest;


#			print "F\t" . length($front) . "\tB\t" . length($back) . "\n";
			# make all output seqs be the same size
#			if (length($front) >= $front_len && length($back) >= $back_len) {
#			$front = substr($front, 0, $front_len);
#			$back = substr($back, 0, $back_len);
			my $majority_seq = "$front^$back";
			my $consensus_seq = generate_consensus(\@orig, $consensus_threshold);

			if ($consensus_seq) {
				if ($consensus_seq eq $majority_seq) {
					print ">CM|$rand_BC|$rand_BC_count|$countBest\n$consensus_seq\n";
				}
				else {
					print ">C|$rand_BC|$rand_BC_count|$countBest\n$consensus_seq\n";
					if ($countBest > $countSecond) { # print most abundant if it is truly more abundant
						print ">M|$rand_BC|$rand_BC_count|$countBest\n$majority_seq\n";
					}

				}
			}
			# MIGHT WANT TO MAKE THIS A RATIO THAT MUST PASS
			elsif ($countBest > $countSecond) { # print most abundant if it is truly more abundant
				print ">M|$rand_BC|$rand_BC_count|$countBest\n$majority_seq\n";
			}

#			$seqBest =~ s/\^//;
#				print ">$rand_BC|$rand_BC_count|$countBest\n$front$back\n";
		}
#		else {
#			printf LOG "CON\t$rand_BC\t%f\t%d\t%d\n", $countBest/$countSecond, $countBest, $rand_BC_count;
#			if ($countBest/$countSecond > 1.5) {
#				detailed_check(\@orig);
#			}
#		}
	}
	else {  # all perfect
		if ($rand_BC_count > $min_read_count) {
			my $scBest = shift @seq_count;
			my ($countBest, $seqBest) = split /:/, $scBest;
			#my $seq = "$front^$back";
#			die ">CM|$rand_BC|$rand_BC_count|$countBest\n$seqBest\n$scBest\n";
			print ">CM|$rand_BC|$rand_BC_count|$countBest\n$seqBest\n";
		}
		else {
			print STDERR "Warning: skipping @seq_count\n";
		}
	}


#	for my $i (0 .. $#seen_bases) {
#		print "#$seen_bases[$i]\t$lines[$i]\n";
#	}

	#die;

}

sub generate_consensus {
	my $seqs = shift;
	my $threshold = shift;
	my %all_bases;
	my @hash_counts;

	my $sum = 0;
	for my $s (@$seqs) {
		my ($count, $seq) = split /:/, $s;

		
		$count = sqrt($count);
#		$count = 1;
		$sum+=$count;

		my @bases = split "", $seq;

		for my $i (0 .. $#bases) {
			$all_bases{$bases[$i]}=1;
			$hash_counts[$i]{$bases[$i]}+=$count;
		}
	}
	my @seen_bases = sort {$a cmp $b} keys %all_bases;

	my $consensus;
	my $consensus_score;
	my @lines;

	my $min_score = 1.0;
        for my $h (@hash_counts) {
                my @bases_sorted = sort {$h->{$b} <=> $h->{$a}} keys %$h;

		$consensus .= $bases_sorted[0];

		my $score = $h->{$bases_sorted[0]}/$sum;
		if ($score < $min_score) {
			$min_score = $score 
		}


		my $short_score = sprintf("%.0f", 10*$score);
		if ($short_score eq "10") {
			$short_score = "0";
		}
		$consensus_score .= $short_score;



		for my $i (0 .. $#seen_bases) {
			my $base_count = 0;
			$base_count = $h->{$seen_bases[$i]} if $h->{$seen_bases[$i]};
			$lines[$i] .= sprintf("%d:%.2f|", $base_count, $base_count/$sum);
		}
	}

	my $out = "$$.$in.consensus$GLOBAL_COUNT.txt";
	open (OUT, ">$out") or die "can't open $out";
	print OUT "$consensus_score\n";
	close OUT;

	if ($min_score < $threshold) {
		printf STDERR "minimum consensus score (%f) was below threshold (%f)\n", $min_score, $threshold;
		return;
	}

	return $consensus;
}

sub detailed_check {
	my $seqs = shift;

	my $seq_str = "";
	my $seq_count=0;

#	print STDERR "looking through $seqs->[0]\t$seqs->[1]" . scalar @$seqs;
	for my $s (@$seqs) {
		my ($count, $seq) = split /:/, $s;
	#	$seq =~ s/\^//;
		$seq =~ s/\^/AAAAA/;
		$seq_str .= ">$count.$seq_count\n$seq\n";
		$seq_count++;
#		my ($front, $back) = split /\^/, $seqBest;
	}
	my $out = "$$.$in.uc$GLOBAL_COUNT.fa";
	open (SEQ, ">$out") or die "can't open $out: $!\n";
	print SEQ $seq_str;
	close SEQ;
	my $uc_out = "$$.$in.uc$GLOBAL_COUNT.uc";
#	system("uclust --quiet --usersort --id 0.97 --input $out --uc $uc_out");
	system("uclust --quiet --usersort --id 0.98 --input $out --uc $uc_out");
	
	#die if $GLOBAL_COUNT > 2;
	my ($cluster_ratio, $best_cluster_idxs) = get_cluster_ratio($uc_out);
#	print STDERR "cluster ratio for $GLOBAL_COUNT is $cluster_ratio\t@$best_cluster_idxs\n";

	$seq_str = "";
	for my $i (@$best_cluster_idxs) {
		my $s = $seqs->[$i];
		my ($count, $seq) = split /:/, $s;
	#	$seq =~ s/\^//;
		$seq =~ s/\^/AAAAA/;
		$seq_str .= ">$count.$i\n$seq\n";
#		my ($front, $back) = split /\^/, $seqBest;
	}
	my $out2 = "$$.$in.uc$GLOBAL_COUNT.best.fa";
	open (SEQ, ">$out2") or die "can't open $out2: $!\n";
	print SEQ $seq_str;
	close SEQ;

#	$GLOBAL_COUNT++;
	#die if $GLOBAL_COUNT > 10;
	
	
	return $cluster_ratio;
}

sub get_cluster_ratio {
	my $in = shift;

	my %count_hash;
	my @cluster_to_seqs;

	my $count=0;
	open (CLUST_IN, $in) or die "can't open $in: $!\n";
	while (<CLUST_IN>){
		if (/^S|^H/) {
			chomp;
			my @pieces = split /\t/;
			my ($num_reads, $id) = split /\./, $pieces[8];
#			print STDERR "HERE\t$pieces[1]\t$pieces[8]\t$num_reads\t$id\n";
			$count_hash{$pieces[1]}+=$num_reads;
			push @{$cluster_to_seqs[$pieces[1]]}, $count;  # put the ids of the sequences in the cluster

			$count++;
		}
#		if (/^C\t0\t(\d+)\t/) {
#			my $bestCount = $1;
#
#			my $next = <CLUST_IN>;
#
#			my $secondBestCount = 1;
#			if ($next && $next =~ /^C\t1\t(\d+\t)/) {
#				$secondBestCount = $1;
#			}
#			printf STDERR "have $bestCount\t$secondBestCount\t%f\n", $bestCount/$secondBestCount;
#			
#		}
	}

	my @keys = sort {$count_hash{$b} <=> $count_hash{$a}} keys %count_hash;

	if (scalar @keys <= 1) {
		push @keys, 1;
		$count_hash{1} = 0.5;
	}

#	printf STDERR "adj$GLOBAL_COUNT\t$count_hash{$keys[0]}\t$count_hash{$keys[1]}\t$keys[0]\t$keys[1]\t%f\n", $count_hash{$keys[0]}/$count_hash{$keys[1]};

#	print STDERR "$count_hash{$keys[0]}\t$count_hash{$keys[1]}\n";
	return $count_hash{$keys[0]}/$count_hash{$keys[1]}, $cluster_to_seqs[$keys[0]];  # put the ids of the sequences in the cluster
}

# attempts removal of random barcodes that are just sequencing errors of true barcodes
# to save time, we skip barcodes with less than $min_reads
sub check_randBC_for_unique {
	my $in = shift;
	my $uniq_thresh = shift;
	my $min_reads = shift;

	print STDERR "attempting to clean up random barcodes\n";

	open (IN, $in) or die "can't open $in: $!\n";
	my $out = "$$.$in.randBC_uc.fa";
	open (OUT, ">$out") or die "can't open $out: $!\n";

	my $id = 0;
	while (<IN>) {
		chomp;
		my ($BC, $count, $stuff) = split /\t/;

		if ($count > $min_reads) {
			print OUT ">$BC.$count\n$BC\n"
		}
	}
	close IN;
	close OUT;
#	die;
	my $uc_out = "$$.$in.randBC.uc$GLOBAL_COUNT.uc";
	system("uclust --quiet --usersort --id $uniq_thresh --input $out --uc $uc_out");

	return get_unique_randBC($uc_out);
}

sub get_unique_randBC {
	my $in = shift;

	my %unique;
	open (CLUST_IN, $in) or die "can't open $in: $!\n";
#	print STDERR "reading $in\n";
	while (<CLUST_IN>){
		if (/^C/) { # only keep clusters; don't keep BCs that are too similar
			chomp;
			my @pieces = split /\t/;
			my ($BC, $num_reads) = split /\./, $pieces[8];
			$unique{$BC} = $num_reads;
#			print STDERR "$BC\t$num_reads\n";
		}
	}
	close CLUST_IN;
#	die;

	return \%unique;
}
