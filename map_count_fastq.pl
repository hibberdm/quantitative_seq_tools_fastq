#!/usr/bin/env perl

# generates a counts file from a mapped file;  works for both normal and gzipped *.mapped files

# ON HANDLING TIES
# When multiple features have equivalent scores, an attempt is made (after all unique matches are complete)
# to assign those nonunique matches using the proportions of the features from the unique matches, this
# can be turned off using the -X option
#
# Example:
# If a hit matches geneA, geneB, and geneC equally well and the number of unique hits for those
# three genes respectively is 90, 9, and 1 -- the algorithm will assign 0.9, 0.09, and 0.01 hits to
# those three genes.
#
# Note that the above does NOT allow new hits to genes that have never been seen before (i.e. if the match
# (is to four proteins, one of which has NEVER received a non-unique match, it will never receive a partial hit)
#
# Exceptions:
# It is pretty common that ALL of the hits for a non-unique match all map to different locations on the same gene,
# in this case the match is essentially a unique match for the particular gene and the hit will be added even for
# previously unseen features
#
# Possible improvements:
# Assuming that nonunique matches are small relative to the unique matches, the above schema should work pretty well.
# A slightly more accurate algorithm, would cycle back and for between counting nonuniques to update the overall hits for each gene
# and then RECOUNTING the nonunique matches with the updated counts (this should converge after awhile and is like an Expectation Maximization
# algorithm (EM)).  This would complicate things a bit and make them non-deterministic, which is a little annoying.
#
# A simpler improvment that might be useful to implement is to first run through the the hits adding ONLY the exception non-unique
# that all map non-uniquely to the same feature and THEN to calculate the partial hits using the updated hit frequencies.


# counts percentage of CDS, intergenic, and rRNA (5S, 16S, 23S), tRNA from
# solexa reads mapped back to a genome that's already been broken into those
# components

# TO DO
#

#MCH Notes
#Fixed line 184 to allow script to complete when nothing maps to intended genome.
#added functionality to map within

use warnings;
use strict;
use Getopt::Long;
use IO::Uncompress::AnyUncompress qw(anyuncompress $AnyUncompressError);
use IO::Compress::Gzip qw(gzip $GzipError) ;
use Bio::DB::Sam;
use FindBin qw($Bin);
use lib "$Bin/lib";
#use lib "/home/comp/jglab/mhibberd/RNA-Seq/quantitative_seq_tools/lib";	#MCH

my $join_char = "&";

# two nasty global variables added afterwards to prevent adding parameters to many many functions
my $TOTAL_MAPPED = 0;
my $TOTAL_SEQUENCES = 0;


my $min_score = 0;
my $map_file = 0;
my $CDSonly;
my $outfile;
my $exclude_ties;
my $length_norm_file;
my $rRNAcount;
my @genome_counts;
my $get_genome_counts;
my $lwgv_file;
my $format;
my $tie_file;
my $PE_counts;
my $database;

GetOptions (
            "min_score=s"   => \$min_score,
            "infile=s"   => \$map_file,
            "outfile=s"   => \$outfile,
            "CDSonly"   => \$CDSonly,
            "rRNAcount"   => \$rRNAcount,
	    "Xclude_ties" => \$exclude_ties,
	    "length_norm=s" => \$length_norm_file,
	    "genome_counts=s" => \$get_genome_counts,
	    "format=s" => \$format,
	    "PE" => \$PE_counts,
        "database" => \$database
            );  # flag

usage() unless $map_file;

my $lwgv_suffix = ".lwgv_counts";
$lwgv_suffix = ".genome_counts" if $format && $format == 2;

$lwgv_file =  $outfile . $lwgv_suffix if $outfile;

unless ($outfile) {
	$outfile = $map_file;
	$outfile =~ s/\.gz$//;
	$lwgv_file = $outfile . $lwgv_suffix;

	$outfile .= ".counts";
}

my $logfile = $outfile.".log";
open(LOG, ">$logfile") or die "can't open $logfile: $!\n";

$tie_file = '/tmp/' . $outfile . ".ties.gz";
#$tie_file = $outfile . ".ties.gz";

print STDERR "ties into $tie_file\n";


my $TOTAL_HITS = 0;
my $length_norm = get_length_norm($length_norm_file) if $length_norm_file;
my ($scores, $hits, $num_ties) = read_map_file($map_file, $min_score, $get_genome_counts, $tie_file, $PE_counts, $database);

my $pretie_total = 0;
for my $g (keys %$hits) {
	$pretie_total += $hits->{$g};
	}
print STDERR "have $pretie_total non-tie hits\n";

my $ties_skipped = include_ties($scores, $hits, $tie_file) unless $exclude_ties;
my $rna_stats = get_RNA_stats($hits) if $rRNAcount;

my $pre_nonCDS = 0;
for my $g (keys %$hits) {
	$pre_nonCDS += $hits->{$g};
	}
print STDERR "have $pre_nonCDS pre-nonCDS removal hits\n";

exclude_nonCDS($scores, $hits) if $CDSonly;

my %gene_pos_lookup = ();
if ($get_genome_counts) {
	my $magic = make_gene_pos_lookup($get_genome_counts);
	%gene_pos_lookup = %$magic;
	print_genome_counts($hits, $get_genome_counts, $lwgv_file, $format);
	}

print_results($outfile, $scores, $hits, $num_ties, $exclude_ties, $length_norm, $rna_stats, $ties_skipped);

unlink $tie_file;

#blah

sub usage {
	print "\tperl map_count.pl -i <file.mapped>\n".
		"\t\t-m [min_score]\n".
		"\t\t-o [outfile]\n".
		"\t\t-CDSonly [keep only genes (RNA only)]\n".
		"\t\t-X [exclude tie matches(default is to include ties in proportion to their non-tie hits)]\n".
		"\t\t-r [rRNAcount]\n".
		"\t\t-g [lwgv_track (name of sequence to use on track)\n".
		"\t\t-f [format for -g option; 1 = lwgv track, 2 = tab-delimited]\n\n";
	exit 1;
}

#MCH This next step errors if no reads map to intended genome.  Added "if" to remedy.
#will need to fix the lwgv track maker (else, below) for future reference.
	my $power = 0;
	if ($mapped_reads > 0) {
		$power = int(log($mapped_reads));
		}

	open OUT, ">$lwgv_file" or die "can't open $lwgv_file: $!\n";
	print STDERR "out to $lwgv_file\n";

	if ($format == 2) {
		for (my $i=0; $i<$#genome_counts; $i++) {

			my $b = "";
			my $f = "";
			my $r = "";

			if ($genome_counts[$i]) {
				$b = $genome_counts[$i] if $genome_counts[$i];
				$f = $genome_counts_f[$i] if $genome_counts_f[$i];
				$r = $genome_counts_r[$i] if $genome_counts_r[$i];
#				print OUT "$i|\t$b\t$f\t$r\t$genome_counts[$i]\n";
#				print OUT "$i|$b|$f|$r|$genome_counts[$i]\n";
				print OUT "$i\t$b\t$f\t$r\n";
			}
		}
	}
	elsif ($power > 0) {
		my $track = "graph RNAseq_e$power addPoints(";
		for (my $i=0; $i<$#genome_counts; $i++) {
			if ($genome_counts[$i] && log($genome_counts[$i])) { # skip the non-existant and ones only there a single time
				$track .= sprintf("%d:%.2f,", $i, log($genome_counts[$i]));
			}
		}
		chop $track;
		$track .= ")";
		print OUT "$track\n";
		}
	else {
		print STDERR "No mapped reads found, Power = $power.";
		}

	close OUT;
}

sub get_RNA_stats {
	my $hits = shift;

	# get totals
	my $adapterSeq = 0;
	$adapterSeq += $hits->{'solexa_Adapter'} if $hits->{'solexa_Adapter'};
	$adapterSeq += $hits->{'scriptseq_truseq_Adapter'} if $hits->{'scriptseq_truseq_Adapter'};
	my $total = 0;

	for my $k (keys %$hits) {
		$total += $hits->{$k};
	}
	my $totalHitCount = $total-$adapterSeq;

	my %hit_tracker = %$hits;

	my %RNA_stats;
	for my $k (keys %$hits) {
		print LOG "$k\t$hit_tracker{$k}\n";
		if ($k=~/CDS/) {
			$RNA_stats{'CDS'}{'count'} += $hits->{$k};
		}
		elsif ($k =~ /intergenic/) {
			$RNA_stats{'intergenic'}{'count'} += $hits->{$k};
		}
		elsif ($k=~/rRNA/) {
#			print "rRNA\n";
			if ($k =~ /16S/) {
				$RNA_stats{'16S'}{'count'} += $hits->{$k};
			}
			elsif($k =~ /23S/) {
				$RNA_stats{'23S'}{'count'} += $hits->{$k};
			}
			elsif($k =~ /5S/) {
				$RNA_stats{'5S'}{'count'} += $hits->{$k};
			}
		}
		elsif ($k=~/tRNA/) {
			$RNA_stats{'tRNA'}{'count'} += $hits->{$k};
		}
		elsif ($k=~/tmRNA/) {
			$RNA_stats{'tmRNA'}{'count'} += $hits->{$k};
		}
		elsif ($k !~ /seq_adapter/) {
			$RNA_stats{'junk'}{'count'} += $hits->{$k};
		}

	}

	for my $k (keys %RNA_stats) {
		$RNA_stats{$k}{'fraction'} = $RNA_stats{$k}{'count'}/$totalHitCount;
	}

	return \%RNA_stats;
}

sub get_length_norm {
        my $in = shift;

        open(IN, $in) or die "can't open $in: $!\n";
        my $sum=0;
        my $count = 0;
        my %length_scale;
        while (<IN>) {
                chomp;
                my ($name, $length) = split /\t/;
                $length_scale{$name} = $length; # want short ones to become long
#		print "have $name\t$length\n";
	}
	close IN;


	return \%length_scale;
}


sub include_ties {
	my ($scores, $hits, $tie_file) = @_;

	my $skipped_ties = 0;
	my %hits = %$hits; # make a copy otherwise ties will influence the weights on ties; which might not be bad, but is less well defined

	my $tie_fh;
#	open ($tie_fh, $tie_file) or die "can't open $tie_file: $!\n";

	$tie_fh = new IO::Uncompress::AnyUncompress $tie_file or die "can't open $tie_file: $!\n";
#	open ($tie_fh, $tie_file) or die "can't open $tie_file: $!\n";

	my $count = 0;
	while (<$tie_fh>) {
#		my $t = $_;
		my @t = split /\^/;
		my ($weights, $skipped) = get_weights(\@t, \%hits);
		$skipped_ties += $skipped;

		my $num_ties = scalar @t;
		for my $i (0 .. $#t) {
			my $tie_hit = $t[$i];
			my @tie_hit = split /$join_char/, $tie_hit;

			my $weight = $weights->[$i];
			next if $weight == 0;
			my $partial_hit = $tie_hit[3] * $weight;

#			die "Error hit not big enough ph $partial_hit weight $weight | @$tie_hit | $hits->{$tie_hit->[1]} | total $num_ties\n" unless $hits->{$tie_hit->[1]}; #$partial_hit == 0;
#			print "adding $partial_hit\n";

			$hits->{$tie_hit[1]} += $partial_hit;
			$scores->{$tie_hit[1]} += $tie_hit[2] * $partial_hit;
		}

		print STDERR "on tie $count\n" if $count++ % 1000000 == 0;
	}

	close $tie_fh;
#	print STDERR "finished with ties (skipped: $skipped_ties) total $TOTAL_TIES\n";
	#print "here\n";


	return $skipped_ties;
}

sub get_weights {
	my ($tied, $hits) = @_;

	my $skipped = 0;
	my $sum = 0;
	my @weights;
	my %targets;
	# first calculate total counts
	for my $t (@$tied) {
		my @t = split /$join_char/, $t;
		$targets{$t[1]}=1;
		$sum+=$hits->{$t[1]} if $hits->{$t[1]};
	}
	my @num_targets = keys %targets;


	# if something matches equally well to multiple locations IN THE SAME GENE that has never been seen
	my $all_hits_to_same_gene = 0;
	if ($sum == 0 && scalar @num_targets == 1) {
		$sum = scalar @$tied;  # if none of tthem has been seen before, they have all been seen equally few times
		$all_hits_to_same_gene = 1;
	}
	elsif ($sum == 0) { # temporary
		$sum = scalar @$tied;  # if none of tthem has been seen before, they have all been seen equally few times
		$all_hits_to_same_gene = 1;
	}

	my $count = 0;
#	print "--------------------------\n";
	if ($sum) {
		for my $t (@$tied) {
			my @t = split /$join_char/, $t;
			my $hit = 0;
			if ($hits->{$t[1]}) {
				$hit = $hits->{$t[1]};
#				print "not_even for $t[1] $hit count $count\n";
			}
			elsif ($all_hits_to_same_gene) {
				$hit = 1;
#				print "Even     for $t[1] $hit count $count\n";
			}
			else {
#				print "none     for $t[1] $hit count $count\n";
			}


			my $weight = $hit/$sum;
			push @weights, $weight;
			$count++;
	#		die "have @$t for $hits->{$t->[1]} with weight $weight with sum $sum\n" unless $hits->{$t->[1]};
		}
	}
	else { # don't count ties that map to multiple genes with zero hits (it skews the present/absent too much)
		for my $t (@$tied) {
			my @t = split /$join_char/, $t;
			push @weights, 0;
#			print "skip     for $t[1]\n";
		}
		$skipped = 1;
#		print "skip\n";
	}
#	print "@weights\n";

	return \@weights, $skipped;
}

sub exclude_nonCDS {
	my ($scores, $hits) = @_;

	print STDERR "deleting nonCDS\n";
	for my $k (keys %$scores) {
		unless ($k =~ /CDS|solexaAdapter|scriptseq_truseq_Adapter/) {
			delete $scores->{$k};
			delete $hits->{$k};
		}
	}
}

sub read_map_file {
	my ($in, $minScore, $get_genome_counts, $tie_file, $PE_counts, $db) = @_;

	my %allHits;
	my %allScores;
	my $tie_fh;
	$tie_fh = new IO::Compress::Gzip $tie_file or die "gzip failed: $GzipError\n";
#	open ($tie_fh, ">$tie_file") or die "Can't open $tie_file: $!\n";
#	print STDERR "opened $tie_file for ties\n";
	my $num_ties=0;
#	my @ties;  # queries with multiple equivalent top hits

	my $previous = ""; # the previous target (used to determine if in the same hit series)
	my @hit_series;  # a series of hits for a given query sequence

	my ($q, $t); # column indexes into an array

	# determine whether to read file with gzip or without
#	my $fh = new IO::Uncompress:AnyUn
#	my $fh = new IO::Uncompress::AnyUncompress $in;
	my %info;
#	my $line = <$fh>;
#	$info{"file_type"} = infer_file_type($line);
	$info{"file_type"} = "bam";
	$info{"PE"} = $PE_counts;
#	$fh->close;

	# reopen the file to start at the beginning
#	$fh = new IO::Uncompress::AnyUncompress $in;

#	die "file type is $info{file_type}\n";

#	if ($in =~ /\.gz$/) {
#		open ($fh, "<:gzip", $in) or die "can't open gzip file $in: $!\n";
#	}
#	else {
#		open ($fh, $in) or die "can't open $in: $!\n";
#	}

    get_number_of_lines_bowtie($map_file); #sets TOTAL_SEQUENCES, brought up from infer_file_type

    #open bam file
    my $sam = Bio::DB::Sam->new(-fasta=> "$db", -bam  =>"$in");

	# read the mapped file
	my $hit = 0;

    my @alignments = $sam->features();

#    for my $align (sort @alignments) { #MCH# what does alignments sort on here? is this appropriate?
    for my $align (@alignments) { #MCH# what does alignments sort on here? is this appropriate?
        my $query = $align->qname;
        my $target = $align->seq_id;
        my $query_seq = $align->query->dna;
        my $length = length($query_seq);
        my @keys = $align->aux_keys;
        my $counts_for_match = 1;
        my $start = $align->start;
        my $stop = $align->end;
        my $dir = $align->strand;
        my $is_unmapped = $align->unmapped;
        my $mismatches = 0;

        if ($dir > 0) {
            $dir = "+";
        }
        elsif ($dir < 0) {
            $dir = "-";
        }
        else {
            die "Confused direction!!\n";
        }

        if ($is_unmapped) {
#            print "Unmapped check: $query\n";
        }
        else {
            #extract mismatches from edit distance (in keys)
            foreach (@keys) {
                if ($_ eq "NM") {
                    $mismatches = $align->aux_get($_);
                    }
                }
            my $score = $length - $mismatches;

            #want 1-based coordinates, not 0-based
            $start++;
            $stop++;

            $hit++;
            next if $score < $minScore;

            if ($get_genome_counts) {
                $target .= "POS_" . $start . "_" . $stop . "_" . $dir;
    #			print "here target $target\n";
            }

            if ($query eq $previous) { # if we are still on the same query sequence, append to that query's hits
    #            print "$query equals $previous, same hit!\n";
                push @hit_series, [$query, $target, $score, $counts_for_match, $start, $stop, $dir];
    #							   0	   1        2       3                  4       5      6
                }
            else { # otherwise, process the query's hits, saving ties for later
    #            print "$query is new, proceeding to new hit\n";
    #			process_new_hit(\@hit_series, \%allHits, \@ties, \%allScores) if @hit_series;
                process_new_hit(\@hit_series, \%allHits, $tie_fh, \%allScores, \$num_ties) if @hit_series;
                @hit_series = ();
                push @hit_series, [$query, $target, $score, $counts_for_match, $start, $stop, $dir];
                $TOTAL_MAPPED++;
            }
        } #is unmapped omission
    $previous = $query;
    }

# 	while (my @hit = get_hit($fh, \%info)) {
# 		my ($query, $target, $score, $counts_for_match, $start, $stop, $dir) = @hit;
# #		print "have @hit\n";
# 		$hit++;
# 		next if $score < $minScore;
#
# 		if ($get_genome_counts) {
# 			$target .= "POS_" . $start . "_" . $stop . "_" . $dir;
# #			print "here target $target\n";
# 		}
#
# 		if ($query eq $previous) { # if we are still on the same query sequence, append to that query's hits
# 			push @hit_series, [$query, $target, $score, $counts_for_match, $start, $stop, $dir];
# #							   0	   1        2       3                  4       5      6
# 			}
# 		else { # otherwise, process the query's hits, saving ties for later
# #			process_new_hit(\@hit_series, \%allHits, \@ties, \%allScores) if @hit_series;
# 			process_new_hit(\@hit_series, \%allHits, $tie_fh, \%allScores, \$num_ties) if @hit_series;
# 			@hit_series = ();
# 			push @hit_series, [$query, $target, $score, $counts_for_match, $start, $stop, $dir];
# 			$TOTAL_MAPPED++;
# 		}
#
# 		$previous = $query;
# 	}
# 	close $fh;

#	process_new_hit(\@hit_series, \%allHits, \@ties, \%allScores);
	process_new_hit(\@hit_series, \%allHits, $tie_fh, \%allScores, \$num_ties);
	close $tie_fh;
	$TOTAL_MAPPED++;

	print STDERR "ties was $num_ties\n";
	return \%allScores, \%allHits, $num_ties;

}

sub infer_file_type {
	my $first_line=shift;
	if ($first_line =~ /Max\. sequence length:/ || $first_line =~ /^ALIGNMENT/) {
		return "ssaha2";
	}
	else {
		my @pieces = split /\t/, $first_line;
#		die "pieces are " . scalar @pieces;
		if (scalar @pieces == 8) {
			# try to get number of lines
			get_number_of_lines_bowtie($map_file);
			return "bowtie";
		}
		elsif (scalar @pieces == 12) {
			return "BLAST";
		}

		return "seqMap";
	}
}

sub get_number_of_lines_bowtie {
	my $infile = shift;
	my $seqfile = $infile;

	$seqfile =~ s/mapped.*$/fastq/;

	my $num_seqs = `wc -l $seqfile`;
#	my $junk;
	my ($things, $junk) = split /\s+/, $num_seqs;
	$TOTAL_SEQUENCES = $things/4; #convert to fastq

    print "$seqfile: $TOTAL_SEQUENCES total seqs\n";

#	die "have infile $infile reading $seqfile $TOTAL_SEQUENCES\n";

}

# allows multiple solexa mapping programs (currently 2 versions of seqMap and ssaha2)
# to be treated in the same way by the rest of the code, by infering file type
# and parsing accordingly to return the same basic info to the calling function
sub get_hit {
	my ($fh, $info) = @_;

#	unless ($info->{"file_type"}) {
#		my $line = <$fh>;
#		$info->{"file_type"} = infer_file_type($line);
#
#		# reopen the file to start at the beginning
##		seek($fh, -length($line), 1); # place the same line back onto the filehandle
#	}

	my $type = $info->{"file_type"};

	if ($type eq "bowtie") {
		return read_bowtie_line($fh, $info);
	}
	else {
		die "Unrecognized map file\n";
	}
}

sub read_bowtie_line {
	my ($fh, $info) = @_;
	my $counts_for_match = 1;
#	my $score = 1000;  # for bowtie the threshod
#	my $score = 1000;  # for bowtie the threshod

	while (<$fh>) {
#		print $_;
		chomp;
#MCH#	my ($query, $orientation, $target, $dstart, $seq, $qual, $M_res, $mismatches) = split /\s+/, $_;
		my ($query, $orientation, $target, $dstart, $seq, $qual, $M_res, $mismatches) = split /\t+/, $_;
		my $score = length($seq);
		my $len = $score;
		if ($mismatches) {
			$score -= 1; # at least one mismatch
			my $comma_count = $mismatches =~ tr/,/,/;
			$score -= $comma_count;  # mismatches are comma separated
#			print "have $score with commas $comma_count\n";
		}
#		print "have $score\n";
		$dstart++;  # bowtie is 0-based I want 1-based
		my $dstop = $dstart + ($len-1);
#		print "dstart:dstop $dstart:$dstop $orientation\n";

		if ($info->{"PE"}) { # PE from bowtie are on two separate lines
			$_ = <$fh>;
#MCH#		my ($PEquery, $PEorientation, $PEtarget, $PEstart, $PEseq, $PEqual, $PEM_res, $PEmismatches) = split /\s+/, $_;
			my ($PEquery, $PEorientation, $PEtarget, $PEstart, $PEseq, $PEqual, $PEM_res, $PEmismatches) = split /\t+/, $_;
			my $PEscore = length($PEseq);
			my $PElen = $PEscore;
			if ($PEmismatches) {
				$PEscore -= 1; # at least one mismatch
				my $PEcomma_count = $PEmismatches =~ tr/,/,/;
				$PEscore -= $PEcomma_count;  # mismatches are comma separated
	#			print "have $score with commas $comma_count\n";
			}
			$score += $PEscore;
		}


		return $query, $target, $score, $counts_for_match, $dstart, $dstop, $orientation;
	}

	return;

}

sub read_seqMap_line {
	my ($fh, $info) = @_;

	my ($q,$t, $start, $stop);

	my $score;
	my $counts_for_match;
	my $query;
	my $target;
	my $start_pos;
	my $stop_pos;
	while (<$fh>) {
		chomp;
		my (@res) = split /\t/;
		my $score = $res[0];

		if ($info->{"query_cols"}) {
			($q,$t, $start, $stop) = @{$info->{"query_cols"}};
		}
		else {
			($q,$t) = determine_query_target_columns(scalar @res);
			$info->{"query_cols"} = [$q,$t, $start, $stop];
		}

		# support for "uniqify" where scarf files are first compressed into a nonredundant read set
		my $counts_for_match = 1; # default one count per match
		$query = $res[$q];
		$target = $res[$t];
		$start_pos = $res[$start];
		$stop_pos = $res[$stop];

		$counts_for_match = $1 if ($query =~ /^UNIQ.*:(\d+)$/);

		return $query, $target, $score, $counts_for_match, $start_pos, $stop_pos;
	}

	return;
}



# allows legacy support for older seqMap output formats
sub determine_query_target_columns {
	my $num_cols = shift;

	my ($q,$t, $start, $stop);


	if ($num_cols == 8) {
		$q = 6, $t = 7, $start = 3, $stop = 4;
	}
	elsif ($num_cols == 6) {
		$q = 4, $t = 5, $start = 1, $stop = 2;
	}
	elsif ($num_cols == 5) {
		$q = 3, $t = 4;
	}
	else {
		die "Warning: incorrect number of columns =  $num_cols (unknown file type)\n";
		next;
	}

	return $q, $t, $start, $stop;
}




sub print_results {
	my ($outfile, $allScores, $allHits, $num_ties, $exclude_ties, $length_norm, $RNA_stats, $num_ties_skipped) = @_;

	open(OUT, ">$outfile") or die "can't open $outfile: $!\n";


	my @g = sort { $allHits->{$a} <=> $allHits->{$b} } keys %$allHits;

	my $adapterSeq = 0;
	$adapterSeq += $allHits->{'solexa_Adapter'} if $allHits->{'solexa_Adapter'};
	$adapterSeq += $allHits->{'scriptseq_truseq_Adapter'} if $allHits->{'scriptseq_truseq_Adapter'};
	my $total = 0;
	my $length_norm_sum = 0;

	for my $g (@g) {
		$total += $allHits->{$g};
		$length_norm_sum += $allHits->{$g}/$length_norm->{$g} if $length_norm->{$g};
	}

	my $totalHitCount = $total-$adapterSeq;
	my $totalSumScore = 0;
	my $have_header=0;
	for my $g (@g) {
		next if $g =~ /solexaAdapter|scriptseq_truseq_Adapter/; #don't print the adapter out as a gene

		my $hits = $allHits->{$g};
		my $scoreSum = $allScores->{$g};
		$totalSumScore += $scoreSum;

#		$length_norm_sum += $allHits->{$g}/$length_norm->{$g} if $length_norm->{$g};
		if ($length_norm->{$g}) {
			if (!$have_header) {
				printf OUT "feature_name\thits\tfraction_of_reads\taverage_match_score\tlength_norm\tlength_norm2\n";

				$have_header = 1;
			}
			printf OUT "%s\t%.1f\t%.5g\t%f\t%f\t%f\n", $g, $hits, $hits/$totalHitCount, $scoreSum/$hits, $allHits->{$g}/$length_norm->{$g}, ($allHits->{$g}/$length_norm->{$g})/$length_norm_sum;
#			print "here in length norm\n";
		}
		else {
			if (!$have_header) {
				printf OUT "feature_name\thits\tfraction_of_reads\taverage_match_score\n";

				$have_header = 1;
			}
			printf OUT "%s\t%.1f\t%.5g\t%f\n", $g, $hits, $hits/$totalHitCount, $scoreSum/$hits;
		}
	}

	my $optional_headers = "";
	my $optional_stats = "";
	if ($TOTAL_SEQUENCES) {
		#my $TOTAL_MAPPED = 0;
		$optional_headers .= "\ttotal_mapped\ttotal_reads\ttotal_fraction_mapped";
		$optional_stats .= sprintf "\t$TOTAL_MAPPED\t$TOTAL_SEQUENCES\t%f", $TOTAL_MAPPED/$TOTAL_SEQUENCES;
	}
	printf OUT "##unique_hits = number sequence database features with hits (genomes for COPRO-Seq; genes for RNA-Seq)\n";
	printf OUT "##mapped_reads = number of reads mapped after any exclusions (e.g. if you use option -r nonCDS is not counted)\n";
	printf OUT "##total_reads = number of reads mapped before any exclusions\n";
	printf OUT "#unique_hits\taverage_score (excluding adapter hits)\tmapped_reads\tadapter_hits\tadapter_percent$optional_headers\n";
	#printf STDERR "#%d\t%d\n", $totalHitCount, $total;
	if ($totalHitCount == 0) {
		printf OUT "#warning total hit count was zero ($totalHitCount) totalHits including adapter ($total)\n";
	}
	else {
		printf OUT "#%d\t%f\t%d\t%d\t%f$optional_stats\n", scalar @g, $totalSumScore/$totalHitCount, $total, $adapterSeq, $adapterSeq/$total;
	}
#	printf OUT "# %d unique hits;  average score %f (not counting adapter hits)\n", scalar @g, $totalSumScore/$totalHitCount;
#	printf OUT "# adapter is %d of %d mapped reads (%f)\n", $adapterSeq, $total, $adapterSeq/$total;
	if ($exclude_ties) {
		printf OUT "#ties_excluded\n";
#		printf OUT "#%d\n", scalar @$ties;
		printf OUT "#%d\n", $num_ties;
	}
	else {
		printf OUT "#ties_included\tnot_hit_ties_skipped\ttotal_reads\tnon_tie_hits\n";
		printf OUT "#%d\t%d\t%d\t%d\n", scalar $num_ties-$num_ties_skipped, $num_ties_skipped, $TOTAL_HITS, $TOTAL_HITS-$num_ties-$num_ties_skipped;
#		printf OUT "#ties_included\tnot_hit_ties_skipped\n";
#		printf OUT "#%d\t%d\n", scalar $num_ties-$num_ties_skipped, $num_ties_skipped;
	}

	if ($RNA_stats) {
#		printf OUT "# RNA statistics: ";
		my @keys = sort {$a cmp $b} keys %$RNA_stats;
		my $text_header = "#";
		my $text_data = "#";
		for my $k (@keys) {
			$text_header .= sprintf "fraction_$k\tnumber_$k\t";
			$text_data .= sprintf "%0.4f\t%d\t", $RNA_stats->{$k}{fraction}, $RNA_stats->{$k}{count};
		}
		chop $text_header;
		chop $text_data;
		print OUT "$text_header\n$text_data\n";
	}
}

# if there are ties keep only the top hits
# don't keep hits less than the top score
sub process_new_hit {
	my ($hits, $allHits, $tie_fh, $allScores, $num_ties) = @_;

#    my $numhits = scalar @{$hits};

#    print "Have $numhits in process_new_hit sub\n";

	my @tie;

	my @sorted_hits = sort {$b->[2] <=> $a->[2]} @$hits;

    my $top_hit = shift @sorted_hits;
	my $top_score = $top_hit->[2];

	push @tie, $top_hit;
	for my $h (@sorted_hits) {
		if ($h->[2] == $top_score) {
			push @tie, $h;
		}
	}

#	print "here\n";
	if (scalar @tie == 1) {  # no tie
#		print "have @$top_hit\n";
#		print "$top_hit->[1]\n";
		$allHits->{$top_hit->[1]} += $top_hit->[3];
		$allScores->{$top_hit->[1]} += $top_hit->[2]*$top_hit->[3]; # multiple the score by the number of times it was found (1 unless using uniqify)
	   }
	else {
		my $hit_species = "";
		for my $t (@tie) {
#			print "have @$t\n";
			$hit_species .= join "$join_char", @$t;
			$hit_species .= '^'
		}
		chop $hit_species;
#		print "tie for $hit_species\n";
#		$ties{$
#		push @$ties, \@tie;
#		push @$ties, $hit_species;
		print $tie_fh $hit_species . "\n";
		$$num_ties++;
#		print "ties size is " . scalar @$ties . "\n";
	}

	$TOTAL_HITS++;
}

sub adjust_start_stop {
	my ($key, $genome)= @_;
#	my $gene = "";
	my ($start, $stop, $start_adj, $stop_adj) = 0;
	if ($key =~ /\w+_intergenic_(\w+)POS_(\d+)_(\d+)_/) {
		print STDERR "In adjust sub, intergenic $1 found.\t$2\t$3\t";
#		$gene = $1;
#		$start = $2;
#		$stop = $3;
		$start_adj = $gene_pos_lookup{$1}{stop} + $2 + 1;
		$stop_adj = $gene_pos_lookup{$1}{stop} + $3 + 1;
		}
	elsif ($key =~ /(\w+)\|.+POS_(\d+)_(\d+)_/) {
		print STDERR "In adjust sub, feature $1 found.\t$2\t$3\t";
#		$gene = $1;
#		$start = $2;
#		$stop = $3;
		$start_adj = $gene_pos_lookup{$1}{start} + $2;
		$stop_adj = $gene_pos_lookup{$1}{start} + $3;
		}
	else {
		die "Error parsing key for start/stop_adj calculations\n";
		}
	print STDERR "start_adj = $start_adj, stop_adj = $stop_adj\n";
	return ($start_adj, $stop_adj);
	}
