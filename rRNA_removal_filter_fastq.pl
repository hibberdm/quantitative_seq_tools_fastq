#!/usr/bin/env perl

# use to remove rRNA from ssaha results "on-the-fly" rather than with map_counts.pl
# this prevents crashing map_counts.pl with the giant file that contains too many ties

#MCH finish sub to read bowtie2 SAM files line-by-line
#MCH 150312 Changed line parsing sep to \t rather than \s, because preserving the native
#fastq header info introduces a new space

use strict;
use warnings;
use IO::Uncompress::AnyUncompress;
use Bio::DB::Sam;
use Data::Dumper;

my $in = shift;
my $file_type = shift;
my $db = shift;
my $pipe = shift;

die "Usage: perl rRNA_removal_filter.pl <infile> <file_type (ssaha2 OR bowtie OR bowtie2)> [pipe]\n" unless $in && $file_type && ($file_type eq 'ssaha2' || $file_type eq 'bowtie'|| $file_type eq 'bowtie2');

my $fh;
my $header;

if ($pipe) {
	$fh = *STDIN; #can I pipe input into the Sam object below?
}
else {
#	$fh = new IO::Uncompress::AnyUncompress $in;
	$fh = Bio::DB::Sam->new(-fasta=> "$db", -bam  =>"$in");
	$header = $fh->header();
}

my $mode = "w+";
my $bam_out = Bio::DB::Bam->open($in."_CDS", $mode);
$bam_out->header_write($header);

my $log = $in . "_CDS.log";
open (LOG, ">$log") or die "can't open $log: $!\n";

my $hit = 0;
my %info;
$info{"file_type"}=$file_type;
my $minScore = 29;
my $query;
my @hit_series;
my $TOTAL_SEQUENCES=0;  # sequences attempted to map
my $MAPPED_COUNT = 0; # sequences mapped
my $CDS_COUNT = 0; # sequences mapped to coding regions
my $ADAPTER_COUNT = 0; # sequences mapped to coding regions
my $previous = "";

get_number_of_lines_bowtie($in);

my @alignments = $fh->features();

#$header = $fh->header;

for my $align (@alignments) {
	my $query = $align->qname;
	my $target = $align->seq_id;
	my $is_unmapped = $align->unmapped;

	$hit++;

	if ($is_unmapped) {
#		print "Unmapped check: $query\n";
		}
	else {
#		print "Query: $query Target: $target\n";
		if ($query eq $previous) { # if we are still on the same query sequence, append to that query's hits
			push @hit_series, [$query, $target, $align];
			}
		else { # otherwise, process the query's hits, saving ties for later
			process_new_hit(\@hit_series) if @hit_series;
			@hit_series = ();
			push @hit_series, [$query, $target, $align];
			}

		}
	$previous = $query;
	}
#close $fh;

print LOG "total_sequences\tmapped_reads\tCDS\tadapter\tCDS fraction\n";
printf LOG "$TOTAL_SEQUENCES\t$MAPPED_COUNT\t$CDS_COUNT\t$ADAPTER_COUNT\t%f\n", $CDS_COUNT/($MAPPED_COUNT-$ADAPTER_COUNT);

sub process_new_hit {
	my $hits = shift;

	# if any hit is to

#	for my $h (
	# if CDS and intergenic, keep
	# if intergenic only toss
	# if ANYTHING matches tRNA or rRNA, toss
	# $query, $target, $score, $counts_for_match, $start, $stop
	my ($rna, $cds, $intergenic, $adapter) = (0,0,0,0);
	for my $h (@$hits) {
		my $t = $h->[1]; # match target
#		die "have $target\n";
		if ($t =~ /CDS/) {
			$cds = 1;
			}
		elsif ($t =~ /intergenic/) {
			$intergenic=1;
			}
#		elsif ($t =~ /16S|23S|5S|tRNA/) {
		elsif ($t =~ /16S|23S|5S|tRNA|tmRNA/) {
			$rna = 1;
			}
        elsif ($t =~ /solexaAdapter|scriptseq_truseq_Adapter/) {
            $adapter=1;
            }
	}
	$MAPPED_COUNT++; # sequences mapped

#	printf "$cds\t$intergenic\t$rna\t%d\n", scalar @$hits;
	if (!$rna && !$adapter && $cds) {
#		printf "$cds\t$intergenic\t$rna\t%d\n", scalar @$hits;
		$CDS_COUNT++; # sequences mapped to coding regions
		print_CDS($hits); #MCH need to figure out how to report aligment here
	}

	$ADAPTER_COUNT++ if $adapter;
}

sub print_CDS {
	my $hits = shift;
#	die "have " . scalar @$hits . "\n";
	for my $h (@$hits) {
#		my $last = $h->[$#$h];
		my %last = %{$h->[2]};
#		print "Code to write: $last{align}\n";
#		print Dumper($last);
		$bam_out->write1($last{align});
	}
}

sub get_number_of_lines_bowtie {
	my $infile = shift;
	my $seqfile = $infile;

	$seqfile =~ s/mapped.*$/fastq/;

	my $num_seqs = `wc -l $seqfile`;
#	my $junk;
	my ($things, $junk) = split /\s+/, $num_seqs;
	$TOTAL_SEQUENCES = $things/4; #convert to fastq

    print "$seqfile: $TOTAL_SEQUENCES total seqs\n";

#	die "have infile $infile reading $seqfile $TOTAL_SEQUENCES\n";

}
