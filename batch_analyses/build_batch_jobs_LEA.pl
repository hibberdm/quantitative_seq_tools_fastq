#!/usr/bin/env perl

#/home/comp/jglab/faithj/cgs/numerical/LEA-Seq


# TO DO 
# 1) add final technical datasets from JBC paper (four left) using -u option
# 2) get checksum and file name from the processed file (which will be the counts file)
# 3) put a symbolic link to them in the GEO_DATA folder

# things to add
#1) check to see if the adapter sequence is in the databases
#2) automatically build database from microbialomics if they don't exist
#3) allow always mapping to the union of all species or to the individual species databases

# bowtie notes: http://bowtie-bio.sourceforge.net/manual.shtml#example-6--a---best---strata

use strict;
use warnings;
use Getopt::Long qw(:config no_ignore_case);
use FindBin qw($Bin);
use DBI;
# Functional style
use Digest::MD5 qw(md5 md5_hex md5_base64);


#my $db_name = "microbialomics_practice";
#my $db_name = "microbialomics_dev";
my $db_name = "microbialomics_v1_Build2";
my $host = "hamlet";
my $pass = "reader";
my $user = "reader";
my $dbh = DBI->connect("DBI:mysql:$db_name:$host",$user,$pass) or die "can't open database: $DBI::errstr\n";

my $ADAPTER_SEQ = ">solexaAdapter\nCAAGCAGAAGACGGCATACGAGCTCTTCCGATCTAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT\n";

my %KEEPER_CODES;
my $gid;


my $species;
my $keep;
my $help;
my $separate_by_species;
my $output_format;
my $google_key = "pWqWjQjxksHswGlqG5_cpYg"; #default spreadsheet
my $update;   # update the script files, only add jobs where the output file doesn't exist already

my $SOLEXA_TO_FASTA = "$Bin/../solexaToFasta.pl";
my $SOLEXA_TO_FASTA_PE = "$Bin/../solexaToFastaPE.pl";
my $SPLIT_BARCODES = "$Bin/../split_barcodes.pl";
my $SPLIT_BARCODES_PE = "$Bin/../split_barcodes_PE.pl";
my $MAP_COUNT = "$Bin/../map_count.pl";
my $LWGV_TRACK = "$Bin/../lwgv_combine.pl";
my $MAPPING_PROGRAM = "ssaha2";
my $LEA_PE_BC = "$Bin/../LEA/print_PE_bc.pl";
my $LEA_CONSENSUS  = "$Bin/../LEA/consensify_majority.pl";
my $BOWTIE = "$Bin/../LEA/PE_bowtie.pl";


my $instructions = "INSTRUCTIONS";

GetOptions(
#	"species=s" => \$species,
	"keep=s" => \$keep,
	"format=s" => \$output_format,
	"google_key=s" => \$google_key,
	"mapping=s" => \$MAPPING_PROGRAM,
	"id_for_google_sheet=i" => \$gid,
#	"type=s" => \$batch_type,
	"help" => \$help,
);

show_help() if $help;

show_help("Error: must pass required parameter -k") unless $keep;

if ($keep) {
	my @keeps = split /,/, $keep;
	for my $k (@keeps) {
		$KEEPER_CODES{$k}=1;	
	}
}


# all of the barcodes we currently use
my %BARCODES = (
	'LBC1' => 'GGATGGT', 'LBC2' => 'AAGCTCG', 'LBC3' => 'GTAGCAG', 'LBC4' => 'TTGCGGT', 'LBC5' => 'ACGGCGA', 'LBC6' => 'AACCTAA', 'LBC7' => 'GAGAATA', 'LBC8' => 'CGTCAAA',
	'LBC9' => 'TGAGCGG', 'LBC10' =>'TCATGGC', 'LBC11' => 'TAGTTTT', 'LBC12' => 'GAATCGG', 'LBC13' => 'TACGATC', 'LBC14' => 'GATCTGC', 'LBC15' => 'CTCGTCA', 'LBC16' => 'GGGTATT',
	'LBC17' => 'TCTTTTG', 'LBC18' => 'TTCCGAA', 'LBC19' => 'CTATGAC', 'LBC20' => 'GTTGGGT', 'LBC21' => 'ACATGTT', 'LBC22' => 'CCATAGG', 'LBC23' => 'TAAACCT', 'LBC24' => 'TGCAACC',
	'8C1' => 'TGAGGTT','8C2' => 'GCTTAGA','8C3' => 'ATGACAG','8C4' => 'CACCTCC','8C5' => 'ATCGAGC','8C6' => 'TACTCTA',
	'8C7' => 'AGACTGA','8C8' => 'CTTGGAA','8C9' => 'CCGATTA','8C10' => 'GGCAGCG','8C11' => 'CCATCAT','8C12' => 'TAACAAG',
	'8C13' => 'GAGGCGT','8C14' => 'TTTAACT','8C15' => 'GGTCCTC','8C16' => 'CGGTGGC','8C17' => 'ACTGTCG','8C18' => 'GTATTTG',
	'8C19' => 'GAGTACG','8C20' => 'ACAGATA','8C21' => 'CTCAATG','8C22' => 'AAATGCA','8C23' => 'ACGCGGG','8C24' => 'GGAGTCC',
	'8C25' => 'CGTCGCT','8C26' => 'TCAACTG','8C27' => 'TGTTTGT','8C28' => 'TACATGG','8C29' => 'GTTCTCA','8C30' => 'CTGGTGG',
	'8C31' => 'TGCCCAT','8C32' => 'AAACCTT','8C33' => 'ACCATAC','8C34' => 'AATACGC','8C35' => 'CGCTACA','8C36' => 'TGGCATA',
	'8C37' => 'TTTTGTC','8C38' => 'ACCCACT','8C39' => 'CCGGACC','8C40' => 'GTACGGC','8C41' => 'TTGCCCC','8C42' => 'ACTCCAA',
	'8C43' => 'TGTGCCA','8C44' => 'AACGGAG','8C45' => 'GATAGTT','8C46' => 'GGTGAAT','8C47' => 'ATGTTCT','8C48' => 'GTAAAAA',
	'8C49' => 'GTCTGAT','8C50' => 'CAATATC','8C51' => 'CTCCCGA','8C52' => 'GCCGTTT','8C53' => 'TAGGTAA','8C54' => 'TCGAGAT',
	'8C55' => 'CATTTAG','8C56' => 'TCCGGGA','8C57' => 'CGAAAGT','8C58' => 'GCCTCCC','8C59' => 'AGTTATG','8C60' => 'CTGCAAT',
	'8C61' => 'CAAGCCG','8C62' => 'GGGTCAA','8C63' => 'GCAACGC','8C64' => 'TGATTAC','8C65' => 'TGCTGGG','8C66' => 'GACACAG',
	'8C67' => 'AGGACCT','8C68' => 'TTTTCGG','8C69' => 'CAGAGTC','8C70' => 'GTATCCT','8C71' => 'CCGGCAG','8C72' => 'AATTCAT',
	'8C73' => 'GCAGGGG','8C74' => 'AGATACT','8C75' => 'TCCCAGC','8C76' => 'CCTAATC','8C77' => 'TCAAACA','8C78' => 'ACCTGAA',
	'8C79' => 'CCAGGCA','8C80' => 'TTTCAGA','8C81' => 'ACTGGAT','8C82' => 'GGCAAGA','8C83' => 'GATGGCC','8C84' => 'AGGCGAA',
	'8C85' => 'ACGTCTG','8C86' => 'GTCGATA','8C87' => 'TACAGAC','8C88' => 'CCCCCCG','8C89' => 'AGCGCCG','8C90' => 'TGGTCTC',
	'8C91' => 'TTCGCTG','8C92' => 'TATCTTA','8C93' => 'CTCACCC','8C94' => 'TCGTGCG','8C95' => 'TGACTCT','8C96' => 'TTTGGCG'
);


$gid = 0 unless $gid;
#my $project_url = "http://spreadsheets.google.com/pub?key=$google_key&single=true&gid=$gid&output=txt";
my $project_url = "https://docs.google.com/spreadsheet/pub?key=$google_key&single=true&gid=$gid&output=txt";
my $project_data = "sequencing.projects";

open (INSTRUCTIONS, ">$instructions") or die "can't open $instructions: $!\n";
download_table($project_url, $project_data);
my ($project_data_hash) = table_to_hash($project_data);
prepare_data_batch_files($project_data_hash);
close INSTRUCTIONS;

sub show_help {
	my $error = shift;
	print "\n\t$error\n" if $error;
	print "\n\tperl build_batch_jobs.pl -k <keep_only>\n";
#	print "\t\t-f <format for normalized output (excel OR matlab; default = excel)>\n";
	print "\t\t-g <google spreadsheet key (for using keys other than the default)>\n";
	print "\t\t-i <google spreadsheet id (for multiple sheets in one spreadsheet document; the default is the first sheet: i.e. -i 0)>\n";
#	print "\t\t-C [data is COPRO-Seq; this option old and is equivalent to -t COPRO]\n";
	print "\n";
	exit(1);
}

sub prepare_data_batch_files {
	my ($project, $output_format, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $GEO, $rRNA_filter, $genome_assembly) = @_;

	my %barcodes;
	my %barcodes_PE;
	my %unique_files;
	my %name_translate;
	my %files_to_map;




	for my $p (@$project) {
		# check and see if we want to keep this experiment
		if (%KEEPER_CODES) {
			if (!$p->{skip} || !$KEEPER_CODES{$p->{skip}}) {
				next;
			}
		}

		if ($p->{solexa_run} && $p->{lane}) {
#			print "HERE $p->{experiment_name}\t$p->{species}\t$p->{solexa_run}\t$p->{lane}\n" if $p->{solexa_run};
			my $dir = run_to_directory($p->{solexa_run});
#			my $seq_file = $dir . "s_" . $p->{lane} . '_sequence.txt';
			my $seq_file = $dir . "s_" . $p->{lane} . '_withindex_sequence.txt';
#			print "directory is $dir\n seqFile is $seq_file\n";
			my $scarf_file = 'run' . $p->{solexa_run} . '_lane'  . $p->{lane} . '.scarf';
			my ($PEfile1, $PEfile2) = transfer_sequence_files($p, $seq_file, $scarf_file);
			#die "have $PEfile1\t$PEfile2\n";

			if ($p->{barcode}) {
				push @{$barcodes{$scarf_file}}, $p->{barcode};
				if ($p->{'platform'} && $p->{'platform'} =~ /PE/) {
					$barcodes_PE{$scarf_file} = 1;
				}
				my $bc_filename = make_barcode_filename($scarf_file, $p->{barcode});
				my $base_name = $bc_filename;
				$base_name =~ s/\.scarf$//;
				$files_to_map{$base_name} = $p;
				$files_to_map{$base_name}{PEfile1} = $PEfile1;
				$files_to_map{$base_name}{PEfile2} = $PEfile2;
				#if ($p->{'platform'} =~ /PE/) {
		#		$name_translate{make_barcode_filename($scarf_file, $p->{barcode})} = $p->{experiment_name};
			}
			else {
				my $base_name = $scarf_file;
				$base_name =~ s/\.scarf$//;
#				$name_translate{$base_name} = $p->{experiment_name};
				$files_to_map{$base_name} = $p;
				$files_to_map{$base_name}{PEfile1} = $PEfile1;
				$files_to_map{$base_name}{PEfile2} = $PEfile2;
			}
		}
#		print "hereA\n";
	}

	# need to combine scarf files from multiple solexa lanes into a single experiment (happens when the same experiment is run onto multiple lanes)
#	print STDERR "at combining step\n";
	my %unique_experiments;
	my $combiner_file = "combine_multirun_experiments.sh";

	open (COMBINER, ">$combiner_file") or die "can't open $combiner_file: $!\n";
	for my $f (keys %files_to_map) {
		#print "have $f\t$files_to_map{$f}{experiment_name}\n";
		my $exp_name = $files_to_map{$f}{experiment_name};
		push @{$unique_experiments{$exp_name}}, $f;
#		print "have $f\t$files_to_map{$f}{experiment_name}\n";
	}
	
	for my $exp (keys %unique_experiments) {
		my $num_exp = scalar @{$unique_experiments{$exp}};

#		print "checking $exp\n";

		if ($num_exp > 1) {
			my @f = @{$unique_experiments{$exp}};

			my $new_output = $exp . "_combined";	

			my $command1 = "cat ";
			my $command2 = "cat ";
			my $command = "";
			my $isPE = 0;

			for my $i (0 .. $#f) {
				my $f = $f[$i];
				
				my $scarf_file_noBC = "$f.scarf";
				$scarf_file_noBC =~ s/_BC\d+\.scarf/.scarf/;
				if ($barcodes_PE{$scarf_file_noBC}) {
#					print COMBINER "PE\n";
					$command1 .= " $f" . "_1" . ".scarf";
					$command2 .= " $f" . "_2" . ".scarf";
					$isPE = 1;
				}
				else {
					my @all = keys %barcodes_PE;
#					print COMBINER "noPE @all\n";
					$command1 .= " $f.scarf";

				}

				if ($i == 0) {
					$files_to_map{$new_output} = $files_to_map{$f};  # keep the old mapping stuff for the first entry only
				}
				delete $files_to_map{$f}; # remove the old mapping stuff
			}


			if ($isPE) {
				$command = "$command1 > $new_output" . "_1.scarf\n$command2 > $new_output" . "_2.scarf\n";
			}
			else {
				$command = $command1 . " > $new_output.scarf";
			}

#			print "$command > $new_output.scarf\n";
			print COMBINER "$command\n";
		}
#		print "have $num_exp\t$exp\t@{$unique_experiments{$exp}}\n";
	}
	close COMBINER;
	print INSTRUCTIONS "# combined experiments that were run across multiple lanes (not always necessary, but doesn't hurt to run\n";
	print INSTRUCTIONS "sh $combiner_file\n";
#	print "finished combining\n";


#	make_LEA_batch_files(\%barcodes,\%name_translate, \%barcodes_PE, $update);
	make_LEA_batch_files(\%files_to_map);
#	make_barcode_batch_file(\%barcodes,\%name_translate, \%barcodes_PE, $update);

#	my $count_files_by_species;
#	if (!$genome_assembly) {
#		$count_files_by_species = make_mapping_batch_file(\%files_to_map, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $rRNA_filter);
#	}
#	else {
#		make_assembly_batch_file(\%files_to_map);
#	}

#	my $count_files_by_species = make_mapping_batch_file(\%files_to_map, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $rRNA_filter);

	
#        my ($project, $output_format, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $GEO, $rRNA_filter, $genome_assembly) = @_;

}

sub make_LEA_batch_files {
	my ($exps_to_map) = @_;

	my @all = keys %$exps_to_map;

	my $text = "";
	my $consensus_text = "";

	my $out1 = "LEA_counts.sh";
	open (OUT1, ">$out1") or die "can't open $out1: $!\n";
	my $out2 = "consensus_seq.sh";
	open (OUT2, ">$out2") or die "can't open $out2: $!\n";
	my $out3 = "consensus_divide.sh";
	open (OUT3, ">$out3") or die "can't open $out3: $!\n";
	my $out4 = "mapping.sh";
	open (OUT4, ">$out4") or die "can't open $out4: $!\n";

	
	for my $a (@all) {
#		$files_to_map{PEfile1} = $PEfile1;
#		$files_to_map{PEfile2} = $PEfile2;
#		$text .= $a . ":" . $exps_to_map->{$a}{experiment_name} . ':' . $exps_to_map->{$a}{barcode} . ":" . $exps_to_map->{$a}{PEfile1} .":" . $exps_to_map->{$a}{PEfile2}.  ",\n";
		my $BC_seq = $BARCODES{$exps_to_map->{$a}{barcode}};
		unless ($BC_seq) {
			die "Error: could not find barcode $exps_to_map->{$a}{barcode}\n";
		}

		my $basefile = $exps_to_map->{$a}{experiment_name};
		my $split_by_randBC = $basefile . "_" . $exps_to_map->{$a}{barcode} . ".LEA";
# perl ../print_PE_bc.pl s_6_1_withindex_sequence.txt s_5_3_withindex_sequence.txt TTCCGAA > V1V2_spike2.txt &
		my $region = lc($exps_to_map->{$a}{region});
		$text .= "perl $LEA_PE_BC $exps_to_map->{$a}{PEfile1} $exps_to_map->{$a}{PEfile2} $BC_seq $region > $split_by_randBC\n";
		my $consensus_outfile = $basefile . ".consensus.fna";
		$consensus_text .= "perl $LEA_CONSENSUS $split_by_randBC > $consensus_outfile\n";

		my $consensus_outfileCM = $basefile . ".consensus.CM.fna";
		my $consensus_outfileM = $basefile . ".consensus.M.fna";
		my $consensus_outfileC = $basefile . ".consensus.C.fna";
		print OUT3 "fasta_grep -q $consensus_outfile -p 'M\\|' > $consensus_outfileM\n";
		print OUT3 "fasta_grep -q $consensus_outfile -p '^C\\||^CM' > $consensus_outfileC\n";
		print OUT3 "fasta_grep -q $consensus_outfile -p '^CM\\|' > $consensus_outfileCM\n";
		print OUT4 "perl $BOWTIE $consensus_outfileM\n";
		print OUT4 "perl $BOWTIE $consensus_outfileC\n";
		print OUT4 "perl $BOWTIE $consensus_outfileCM\n";

#perl ../PE_bowtie.pl V4V5_1_spike1_CM.consensus.fna
#perl ../PE_bowtie.pl V4V5_1_spike1_M.consensus.fna
#perl ../PE_bowtie.pl V4V5_1_spike1_C.consensus.fna


#		my $LEA_PE_BC = "$Bin/../LEA/print_PE_bc.pl";

	}
	print OUT1 "$text";
	print OUT2 "$consensus_text";
	close OUT1;
	close OUT2;



}

sub transfer_sequence_files {
	my ($p, $seq_file, $scarf_file, $unique_files)=@_;
	my ($seq_fileA, $seq_fileB);
	my ($scarf_fileA, $scarf_fileB);
	$seq_fileA = $seq_fileB = $seq_file;
	$scarf_fileA = $scarf_fileB = $scarf_file;
#	$seq_fileA =~ s/_sequence/_1_sequence/;
#	$seq_fileB =~ s/_sequence/_2_sequence/;
	$seq_fileA =~ s/_withindex/_1_withindex/;
	$seq_fileB =~ s/_withindex/_3_withindex/;

	$scarf_fileA =~ s/\.scarf/_1.scarf/;
	$scarf_fileB =~ s/\.scarf/_2.scarf/;

	if ($p->{'platform'} && $p->{'platform'} =~ /Knight/) {
	}
	elsif ($p->{'platform'} && $p->{'platform'} =~ /PE/) {
		move_file_sym_link($unique_files, $seq_fileA, $scarf_fileA);
		move_file_sym_link($unique_files, $seq_fileB, $scarf_fileB);

#		die "Paired-end!\n";
	}
	else {
		move_file_sym_link($unique_files, $seq_file, $scarf_file);
	}

	return $scarf_fileA, $scarf_fileB;
}
sub move_file_sym_link {
	my ($unique_files, $seq_file, $scarf_file) = @_;

	# don't want to grab the same file 2x
	if (!$unique_files->{$scarf_file}) {
		# COPY THE FILE OVER; need to check if file exists
#		print "$seq_file to $scarf_file\n";
		unless (-e $scarf_file) {
			print STDERR "moving $seq_file to $scarf_file\n";
#			system("cp $seq_file $scarf_file");
			system("ln -s $seq_file $scarf_file");
		}
		$unique_files->{$scarf_file}=1;
	}
}

sub get_organism {
	my ($accession_to_species_name, $species_list) = @_;

	$species_list =~ s/\s+//g;
	my @s = split /,/, $species_list;

	my @n;
	
	for my $s (@s) {
		if ($accession_to_species_name->{$s}) {
			push @n, $accession_to_species_name->{$s};
		}
		else {
			push @n, $s;
			print STDERR "Warning couldn't find $s in species list\n";
		}
	}

	return join ",", @n;
}

sub generate_sequence_database {
	my ($sequence_db, $mapping_prog, @species) = @_; 
##	die "have species @species at generate_sequence_database\n";
	
	
	for my $i (0 .. $#species) {
		my $s = $species[$i];
		if ($i == 0) {
			open (SEQ_DB, ">$sequence_db") or die "can't open $sequence_db: $!\n";
			print SEQ_DB $ADAPTER_SEQ;
			close SEQ_DB;
		}
		system("cat $s >> $sequence_db");
	}

	if ($mapping_prog eq 'bowtie') {
		my $check_existing = "$sequence_db.rev.1.ebwt";
		if (-e $check_existing) {
			print STDERR "using existing bowtie index for $sequence_db\n";
		}
		else {
			print STDERR "building index $sequence_db for bowtie\n";
#			system("bowtie-build -o 5 $sequence_db $sequence_db");
			system("bowtie-build -o 4 $sequence_db $sequence_db");
			# -o 5 is the default; -o 3 takes 4 times the memory but runs 4x faster
#			system("bowtie-build -o 3 $sequence_db $sequence_db");
		}
	}
	#die;
}

sub download_files_from_microbialomics {
	my $species = shift;
	my $make_lwgv = shift;
	my $COPRO = shift;

#	return;
	print STDERR "downloading sequence files for @$species\n";
#	die "downloading sequence files for @$species\n";

	if ($species->[0] && $species->[0] eq "all") {
		write_sequence_file_all();
		write_position_file_all();
	#		write_position_file($s, $genome_id);
	}
	else {
		my $fh;
		my $fh2;
		if ($COPRO) { 
			open($fh, ">genome_lengths.txt") or die "can't open genome_lengths.txt: $!\n"; 
			open($fh2, ">accession_to_species.txt") or die "can't open accession_to_species.txt: $!\n"; 
		}
		for my $s (@$species) {
			my ($genome_id, $genome_seq, $genome_length, $genome_name) = get_genome_data($s);

			if ($COPRO) {
				print $fh "$s\t$genome_length\n";
				print $fh2 "$s\t$genome_name\n";
			}

#			print STDERR "writing sequence file\n";	
			my $features = write_sequence_file($s, $genome_id, $genome_seq, $make_lwgv, $COPRO);
#			print STDERR "writing position file\n";	
			write_position_file($s, $genome_id, $features);
#			print "$genome_id\n";
		}
		if ($COPRO) { close $fh; close $fh2;}
	}
#	die "finished downloading\n";
}

sub get_accession_to_species_name {
	my $query = "SELECT genome_constant_id, genome_name from genome";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my %id_to_name;
	while (my @res = $sth->fetchrow_array()) {
		$id_to_name{$res[0]} = $res[1];
#		print "adding $res[0] to $res[1]\n";
	}	

	return \%id_to_name;
}

sub write_sequence_file_all {
	my $query = "SELECT genome_id, genome_sequence from genome";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my @prev;

	my $out = "all";
	open (OUT, ">$out") or die "can't open $out: $!\n";
	
	
	my $query2 = "SELECT feature_symbol, feature_type, feature_description, start_pos, stop_pos, direction FROM feature f WHERE primary_feature='Y' AND f.genome_id=? ORDER BY start_pos";
	my $sth2 = $dbh->prepare($query2);
	while (my @res_gen = $sth->fetchrow_array()) {
		$sth2->execute($res_gen[0]);
		my $genome_seq = $res_gen[1];
		print "on genome $res_gen[0] len " . length($genome_seq) . "\n";
		my $last;
		while (my @res = $sth2->fetchrow_array()) {
			my $seq = get_sequence(\$genome_seq, $res[3], $res[4]);
			$res[1] =~ s/\s+/_/;
			print OUT to_fasta("$res[0]|$res[1]|$res[2]", $seq);
			if (@prev && $res[3] > $prev[4]) {
				my $name = "$res[0]_intergenic_$prev[0]";
				my $seqIntergenic = get_sequence(\$genome_seq, $prev[4], $res[3]);
       				print OUT to_fasta("$name", $seqIntergenic);
			}
			@prev = @res;
			$last = $res[4];
		}
		my $dif = length($genome_seq) - $last;
#		print "last is $last dif is $dif\n";
	}
	close OUT;
}

sub write_sequence_file {
	my ($constant_id, $genome_id, $genome_seq, $make_lwgv, $COPRO) = @_;

	my $query = "SELECT feature_symbol, feature_type, feature_description, start_pos, stop_pos, direction FROM feature f WHERE primary_feature='Y' AND f.genome_id=? ORDER BY start_pos";
	my $sth = $dbh->prepare($query);

	my @prev;

	$sth->execute($genome_id);

#	die "writing sequence file with $COPRO\n";
	my $out = $constant_id;
	open (OUT, ">$out") or die "can't open $out: $!\n";

	if ($make_lwgv || $COPRO) {
       		print OUT to_fasta("$constant_id", $genome_seq);
		return;
	}
	elsif ($genome_id) { # sequence is in database
#		print "using database for $genome_id\n";
		while (my @res = $sth->fetchrow_array()) {
			my $seq = get_sequence(\$genome_seq, $res[3], $res[4]);
			$res[1] =~ s/\s+/_/;
			print OUT to_fasta("$res[0]|$res[1]|$res[2]", $seq);
			if (@prev && $res[3] > $prev[4]) {
				my $name = "$res[0]_intergenic_$prev[0]";
				my $seqIntergenic = get_sequence(\$genome_seq, $prev[4], $res[3]);
       				print OUT to_fasta("$name", $seqIntergenic);
			}
			@prev = @res;
		}
	}
	else { # sequence is provided by user with ptt and rnt files
		my $features = read_genome_features_from_ptt_rnt($constant_id);
		for my $res (@$features) {
			my @res = @$res;
			my $seq = get_sequence(\$genome_seq, $res[3], $res[4]);
			$res[1] =~ s/\s+/_/;
			print OUT to_fasta("$res[0]|$res[1]|$res[2]", $seq);
			if (@prev && $res[3] > $prev[4]) {
				my $name = "$res[0]_intergenic_$prev[0]";
				my $seqIntergenic = get_sequence(\$genome_seq, $prev[4], $res[3]);
       				print OUT to_fasta("$name", $seqIntergenic);
			}
			@prev = @res;
		}
		close OUT;
		return $features;
	}

	close OUT;

}

sub get_sequence {
	my ($genome_seq, $start, $stop) = @_;
	my $len = ($stop-$start) + 1;

	my $s = $start-1;
	my $res = substr($$genome_seq, $start-1, $len);
	return $res;
}

sub to_fasta {
	my ($seqName, $seq, $len) = @_;

	$len = 80 unless $len;

	my $formatted_seq = ">$seqName\n";
	while (my $chunk = substr($seq, 0, $len, "")) {
		$formatted_seq .= "$chunk\n";
	}

	return $formatted_seq;
}

# make one big concatenated sequence
sub slurp_sequence {
	my $in = shift;

	open (IN, $in) or die "can't open $in: $!\n";

	my $seq = "";
	while (<IN>) {
		next if /^>/;	
		s/\s+//;
		$_ = uc($_);
		$seq .= $_;
	}
	close IN;

	return $seq;
}

sub make_barcode_filename {
	my ($scarf_file, $barcode) = @_;

	my $base_file = $scarf_file;
	$base_file =~ s/\.scarf$//;

	my $file = $base_file . "_" . $barcode . ".scarf";
	
	return $file;
}

sub run_to_directory {
	my $text=shift;
	my $dir_name_len = 4;
	
	my $dir_base = "/srv/seq/solexa/results/";
#	print "updating $text\n";

	my @pieces = split '\.', $text;
	my $dir = shift @pieces;
#	print "dir now is $dir from @pieces\n";

	while (length($dir) < $dir_name_len) {
		$dir = "0" . $dir;
	}

	unshift @pieces, $dir;

	my $final_dir = join ".", @pieces;
#	die "have $final_dir\n";

	return $dir_base . $final_dir . "/";
}


sub table_to_hash {
	my $in = shift;

	open (IN, $in) or die "can't open $in: $!\n";

	my $line = <IN>;
	chomp $line;
	my @headers = split /\t/, $line;
	for (@headers) { $_=lc($_); }
#	die "headers @headers\n";
	my @table;
	while (<IN>) {
		chomp;
		my @cols = split /\t/;

#		if (scalar @cols != scalar @headers) {
#			printf STDERR  "different number of columns and headers %d vs %d\n", scalar @cols, scalar @headers;
#			print "@headers\n@cols\n\n";
#		}
		my %hash;
		for my $i (0 .. $#cols) {
			$hash{$headers[$i]} = $cols[$i];	
		}

		push @table, \%hash;
	}
	close IN;
	
	return \@table;
}

sub download_table {
	my ($url, $outfile) = @_;

	print STDERR "\n\n" . fancy_title("downloading $url to $outfile");

#	sleep(1 + 2*rand()); # don't piss off google

	# note that $url below need to be surrounded by the single-quotes otherwise wget doesn't see the other parameters
	#                          what?  where?    tries?
	my $download_info = `wget '$url' -O $outfile -t 3`;

}

# little headers for telling people stuff
sub fancy_title {
	my $title = shift;
	my $head_bar;
	for (0 .. length($title)+11) { $head_bar .= '-'; }

	return "$head_bar\n|     $title     |\n$head_bar\n";
}

