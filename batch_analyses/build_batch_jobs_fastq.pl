#!/usr/bin/env perl

#J's TO DO
# 1) add final technical datasets from JBC paper (four left) using -u option
# 2) get checksum and file name from the processed file (which will be the counts file)
# 3) put a symbolic link to them in the GEO_DATA folder

#J's things to add
#1) check to see if the adapter sequence is in the databases
#2) automatically build database from microbialomics if they don't exist
#3) allow always mapping to the union of all species or to the individual species databases

# bowtie notes: http://bowtie-bio.sourceforge.net/manual.shtml#example-6--a---best---strata

#MCH note:  folder access is changing with GTAC not storing data.  I moved dir_base around
#a bit and is now specified by -d.

#MCH's TO DO
# 1) bowtie2 compatibility/output format. Mapping ok, but parsing output not complete.
# [COMPLETE] 2) remove dependence on microbialomics, functionally & formally (in code)
# [COMPLETE] 3) allow user specification of database storage location
# [COMPLETE] 4) allow access to ncbi genomes for genome pulls - may require larger change in overall filetypes/format allowances
# 5) figure out how/if the -u option works and whether to keep it or not...
# [COMPLETE] 7) consider using symlinks instead of moving genomes around
# [COMPLETE] 8) must fix "clip" command now that solexa_to_fasta is written out of the script
# [COMPLETE] 9) make_assembly_batch_file needs to be fixed to allow for fastq intermediate files
# 10) allow for PE mapping, both in this file and in split_barcodes_PE.pl
# [COMPLETE] 11) Write out ssaha2 mapping
# [COMPLETE]12) Write in more informative progress messages.
# [COMPLETE]13) Modify suffix for genome files and make sure to copy over contigs files to genomes dir
# 14) Consider commenting out RNASeq_norm.pl calls for *CDS.counts and *CDS.COUNTS files
# 15) Add support for DRAFT genome searching.

##MCH Required input files for each genome##
##One of these options is required; additionally, priority is assigned in this order, as
###limited by how the usr specifies --location (local|ncbi|both)
#Either 1) .genome and .gene_pos files in cwd or USR_GENOMES_DIR
#		2) .fna, .ptt, AND .rnt in USR_GENOMES_DIR
#		3) .gb or .gbk in USR_GENOMES_DIR
#		4) .fna, .ptt, AND .rnt in NCBI_GENOMES_DIR
#		5) .gbk in NCBI_GENOMES_DIR

use strict;
use warnings;
use Getopt::Long qw(:config no_ignore_case);
use FindBin qw($Bin);
use lib "$Bin/../lib";
use BARCODES;
use GENOMECODES qw(declare_speciesname_for_inhouseacc);
# Functional style
use Digest::MD5 qw(md5 md5_hex md5_base64);
use File::Slurp;

#check for most updated version of pipeline (Nick S. tool)
my $git_status = "$Bin/.git-status-tester.sh";
if (-e $git_status) {
	system("$Bin/.git-status-tester.sh");
	}

#Default genome locations
my $USR_GENOMES_DIR = "/scratch/ref/rnaseq/1.0/100G_genome_storage/";
my $NCBI_GENOMES_DIR = "/scratch/ref/rnaseq/1.0/Bacteria/";
my $NCBI_DRAFT_GENOMES_DIR = "/scratch/ref/rnaseq/1.0/Bacteria_DRAFT/";
my $ADAPTER_LOC = "$Bin/../ref/adapters.fa";

my %accession_to_species_name = declare_speciesname_for_inhouseacc();

my %GEO_DATA;

my $species;
my $keep;
my $help;
my $separate_by_species;
my $output_format;
my $google_key; #require user to either set google_key or use a local file.
my $update;   # update the script files, only add jobs where the output file doesn't exist already

my $SOLEXA_TO_FASTA = "$Bin/../solexaToFasta.pl";
my $SOLEXA_TO_FASTA_PE = "$Bin/../solexaToFastaPE.pl";
my $SPLIT_BARCODES = "$Bin/../split_barcodes_fastq.pl";
my $SPLIT_BARCODES_PE = "$Bin/../split_barcodes_PE.pl";
my $MAP_COUNT = "$Bin/../map_count_fastq.pl";
my $LWGV_TRACK = "$Bin/../lwgv_combine.pl";
my $RRNA_REMOVAL_FILTER = "$Bin/../rRNA_removal_filter_fastq.pl";
my $RNA_SEQ_NORM = "$Bin/../RNAseq_norm.pl";
my $MAPPING_PROGRAM = "bowtie";
#my $VELVET_ASSEMBLER = 'perl ~/velvet_1.0.12/contrib/VelvetOptimiser-2.1.7/VelvetOptimiser.pl';
#my $VELVET_ASSEMBLER = 'perl ~/velvet_1.1.04/contrib/VelvetOptimiser-2.1.7/VelvetOptimiser.pl';
my $VELVET_ASSEMBLER = 'VelvetOptimiser.pl';
my $GENBANK_TO_PTT_RNT_FNA = "$Bin/../genbank_split.pl";
my $ASSEMBLY_FINAL = "$Bin/../final_assembly.pl";

#my $instructions = "$Bin/rnaseq_pipeline_instructions.txt";
my $no_processed_genomes;
my $make_lwgv_tracks;
my $COPRO_seq;
my $ncbi;
my $gid;
my $GEO;
my $rRNA_filter = 'none';
my $exclude_ties;
my $genome_assembly;
my $batch_type = "";
my $dir_base;
my $clip = 0;
my $mapping_file;
my $location = "local";
my $date = get_datecode();
my $genome_log = $date."_genome_sources.log";
my $ADAPTER_SEQ = read_file($ADAPTER_LOC);

GetOptions(
	"a=s" => \$mapping_file,
	"separate_by_species" => \$separate_by_species,
	"directory=s" => \$dir_base,
	"keep=s" => \$keep,
	"format=s" => \$output_format,
#	"google_key=s" => \$google_key,
#	"mapping=s" => \$MAPPING_PROGRAM,
	"location=s" => \$location,
#	"no_processed_genomes" => \$no_processed_genomes,
	"id_for_google_sheet=i" => \$gid,
	"rRNA_filter=s" => \$rRNA_filter,
	"Xclude" => \$exclude_ties,
	"v" => \$make_lwgv_tracks,
#	"COPRO" => \$COPRO_seq,
	"type=s" => \$batch_type,
	"GEO" => \$GEO,
	"u" => \$update,
	"U=s" => \$USR_GENOMES_DIR,
	"clip=i" => \$clip,
	"help" => \$help,
);

show_help() if $help;

if ($location eq "default") {
	print "\tDefault Genome Locations:\n";
	print "\t\tUser Genomes Directory: $USR_GENOMES_DIR\n";
	print "\t\tNCBI (mirror) Genomes Directory: $NCBI_GENOMES_DIR\n";
	print "\t\tNCBI (mirror) Draft Genomes Directory: $NCBI_DRAFT_GENOMES_DIR\n";
exit(0);
}

print STDERR "\nYou have specified full path lengths to sequence data files...\n" unless $dir_base;

show_help("Error: must pass required parameter -a") unless $mapping_file;

show_help("Error: must pass required parameter -k") unless $keep;

show_help("Error: mapping program (-m) must be bowtie or bowtie2") unless $MAPPING_PROGRAM eq 'bowtie' or $MAPPING_PROGRAM eq 'bowtie2';

unless ($USR_GENOMES_DIR =~ /\/$/) {
	$USR_GENOMES_DIR = $USR_GENOMES_DIR . "/";
	}

die "$USR_GENOMES_DIR doesn't exists or is not a valid directory!\n" unless (-e $USR_GENOMES_DIR and -d $USR_GENOMES_DIR);

$rRNA_filter = lc($rRNA_filter) if $rRNA_filter;

if ($batch_type =~ /COPRO/i) {
	$COPRO_seq=1;
}
elsif ($batch_type =~ /assembly/i) {
#	print STDERR "assembly!!!\n";
	$genome_assembly=1;
}

my %KEEPER_CODES;

if ($keep) {
	my @keeps = split /,/, $keep;
	for my $k (@keeps) {
		$KEEPER_CODES{$k}=1;
	}
}

#MCH mod to match nate's coproseq setup.  check locations to make sure things are accessed correctly.
my %BARCODES = barcodes::declare_barcodes();

$gid = 0 unless $gid;
#my $project_url = "http://docs.google.com/spreadsheet/ccc?key=$google_key&output=txt";
#my $project_url = "https://docs.google.com/spreadsheet/pub?key=$google_key&single=true&gid=$gid&output=txt";
#my $project_url = "https://docs.google.com/spreadsheets/d/$google_key/pubhtml?gid=$gid";
my $project_data = "sequencing.projects";

#system("cp $instructions .");

print STDERR "Preparing analysis... \n\tLoading ";
#MCH# open (INSTRUCTIONS, ">$instructions") or die "can't open $instructions: $!\n";
open (GENOME_LOG, ">>$genome_log") or die "can't open $genome_log: $!\n";
if ($mapping_file) {
	print "mapping file: $mapping_file\n";
	$project_data = $mapping_file;
	}
else {
	my $project_url = "https://docs.google.com/spreadsheet/ccc?key=$google_key&output=txt";
	print "data from $project_url\n";
	download_table($project_url, $project_data);
	}
my ($project_data_hash) = table_to_hash($project_data);
#prepare_data_batch_files($project_data_hash, $output_format, $no_microbialomics, $make_lwgv_tracks, $MAPPING_PROGRAM, $COPRO_seq, $update, $GEO, $rRNA_filter, $genome_assembly);
prepare_data_batch_files($project_data_hash, $output_format, $no_processed_genomes, $make_lwgv_tracks, $MAPPING_PROGRAM, $COPRO_seq, $update, $GEO, $rRNA_filter, $genome_assembly, $clip);
#MCH# close INSTRUCTIONS;

make_cleanup_file();

#MCH moving genome data around to keep things tidy
unless (-d "genomes") { system("mkdir ./genomes"); }
my @tmp = glob("*.gb *.fna *.faa *.gbk *.ptt *.rnt *.contig");
foreach my $file (@tmp) {
	system("mv $file ./genomes");
	}

sub show_help {
	my $error = shift;
	print "\n\t$error\n" if $error;
	print "\n\tperl build_batch_jobs_fastq.pl\n";
	print "\t\t-k [text tag for samples to keep (from mapping file)]\n";
	print "\t\t-a [path to local mapping file (alternative to google download approach)]\n";
	print "\t\t-l <local|ncbi|both|default> (genome locations to use (user directory, CGS ncbi mirror, or both, or print defaults)\n";
	print "\t\t-c [when demultiplexing, trim sequences and quality scores to user-defined n characters]\n";
	print "\t\t-t [analysis type; options: COPRO, RNA, assembly; default is RNA]\n";
#MCH#	print "\t\t-g <google spreadsheet key (for using keys other than the default)>\n";
#MCH#	print "\t\t-i <google spreadsheet id (for multiple sheets in one spreadsheet document; the default is the first sheet: i.e. -i 0)>\n";
#MCH#	print "\t\t-m <bowtie|bowtie2> (mapping program, bowtie is default - bowtie2 is not yet supported)\n";
#MCH#	print "\t\t-C [data is COPRO-Seq; this option old and is equivalent to -t COPRO]\n";
	print "\t\t-f [format for normalized output (excel OR matlab; default = excel)]\n";
	print "\t\t-X [exclude tie matches]\n";
	print "\t\t-G [generate geo_info.txt and srf files to help with GEO submission]\n";
	print "\t\t-v [make lwgv files]\n";
#	print "\t\t-d [path to sequence data directory, one level above run number EX: /srv/seq/solexa/results/]\n";
	print "\t\t-u [update mode: only add jobs to the batch files for tasks where the appropriate outfile doesn't exist]\n";
	print "\n\t\t*** RNA-Seq specific options ***\n";
	print "\t\t-r [post|pre|none (default: none?); (pre: deletes rRNA reads;  post: removes rRNA after mapping (results in much larger files but no data loss)]\n";
	print "\t\t-s [separate counts files by species]\n";
	print "\t\t-U [path to user genome directory]\n";
	print "\n";
	exit(1);
}

sub get_datecode {
	my ($day, $month, $year)=(localtime)[3,4,5];
	$year = $year - 100;
	if ($month < 10) {
		$month = $month + 1;
		$month = "0".$month;
		}
	else {
		$month = $month + 1;
		}
	if ($day < 10) {
		$day = "0".$day;
		}
	my $datecode = $year.$month.$day;
	return $datecode;
	}

sub prepare_data_batch_files {
#	my ($project, $output_format, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $GEO, $rRNA_filter, $genome_assembly) = @_;
	my ($project, $output_format, $no_processed_genomes, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $GEO, $rRNA_filter, $genome_assembly, $clip) = @_;

	my %barcodes;
	my %barcodes_PE;
	my %unique_files;
	my %name_translate;
	my %files_to_map;
	my $must_trim_index;

	for my $p (@$project) {
		# check and see if we want to keep this experiment
		if (%KEEPER_CODES) {
			if (!$p->{keep} || !$KEEPER_CODES{$p->{keep}}) {
				next;
				}
			}

		if (($p->{run} && $p->{lane}) || $p->{path_to_seq_file}) {
#			print "HERE $p->{experiment_name}\t$p->{species}\t$p->{run}\t$p->{lane}\n" if $p->{run};

			my $seq_file;
			my $fastq_file;
			if ($p->{path_to_seq_file}) {
#				my $dir = $p->{path_to_file} . $p->{pool_name} . ".txt";
				$seq_file = $p->{path_to_seq_file};
#MCH#			$fastq_file = 'run_' . $p->{pool_name} . '.fastq';
				$fastq_file = $p->{run} . '_lane'  . $p->{lane} . '.fastq';
				}
			else {
				my $dir = run_to_directory($p->{run});
				$seq_file = $dir . "s_" . $p->{lane} . '_sequence.txt';
				$fastq_file = $p->{run} . '_lane'  . $p->{lane} . '.fastq'; #added "run_" instead of "run" #MCH#
				}
#			print "directory is $dir\n seqFile is $seq_file\n";
#MCH#		if ($p->{platform} =~ /miseq/i || $p->{platform} =~ /fastq/i) {
#MCH#			$fastq_file =~ s/\.scarf$/\.fastq$/;
#MCH#			}

#MCH#			print "\n\nseq_file = $seq_file\n";
#MCH#			print "fastq_file = $fastq_file\n\n";

			$must_trim_index = transfer_sequence_files($p, $seq_file, $fastq_file);

			if ($p->{barcode}) {
#MCH#			print "Have barcode.  BC = $p->{barcode}\n"; #MCH	
				push @{$barcodes{$fastq_file}}, $p->{barcode};
				if ($p->{'platform'} && $p->{'platform'} =~ /PE/) {
#					print "PE barcode $fastq_file\n";
					$barcodes_PE{$fastq_file} = 1;
					}
				my $bc_filename = make_barcode_filename($fastq_file, $p->{barcode});
				my $base_name = $bc_filename;
				$base_name =~ s/\.fastq$//;
				$files_to_map{$base_name} = $p;
#MCH#			print "Base Name = $base_name, Exp = $p->{experiment_name}\n";
				#if ($p->{'platform'} =~ /PE/) {
		#		$name_translate{make_barcode_filename($fastq_file, $p->{barcode})} = $p->{experiment_name};
				}
			else {
#MCH#			print "Don't have barcode.\n"; #MCH
				my $base_name = $fastq_file;
				$base_name =~ s/\.fastq$//;
#				$name_translate{$base_name} = $p->{experiment_name};
				$files_to_map{$base_name} = $p;
				}
			}
#		print "hereA\n";
		}
#	print "hereB\n";

	# need to combine fastq files from multiple solexa lanes into a single experiment (happens when the same experiment is run onto multiple lanes)
#	print STDERR "at combining step\n";
	print "Checking for unique experiments to be combined for analysis\n";
	my %unique_experiments;
	my $combiner_file = "combine_multirun_experiments.sh";

	open (COMBINER, ">$combiner_file") or die "can't open $combiner_file: $!\n";

	for my $f (keys %files_to_map) {
#		print "have $f\t$files_to_map{$f}{experiment_name}\n";
		my $exp_name = $files_to_map{$f}{experiment_name};
		push @{$unique_experiments{$exp_name}}, $f;
#		print "have $f\t$files_to_map{$f}{experiment_name}\n";
		}

	my $num_exp = 0;
	for my $exp (keys %unique_experiments) {
		$num_exp = scalar @{$unique_experiments{$exp}};
		print "Number of experiments in unique identifier loop = $num_exp\n";
#		print "checking $exp\n";

		if ($num_exp > 1) {
			my @f = @{$unique_experiments{$exp}};

#			print "in combine\n";

			my $new_output = $exp . "_combined";

			my $command1 = "cat ";
			my $command2 = "cat ";
			my $command = "";
			my $isPE = 0;

			for my $i (0 .. $#f) {
				my $f = $f[$i];

				my $fastq_file_noBC = "$f.fastq";
				$fastq_file_noBC =~ s/_BC\d+\.fastq/.fastq/;
				$fastq_file_noBC =~ s/_8C\d+\.fastq/.fastq/;
				if ($barcodes_PE{$fastq_file_noBC}) {
#					print  "PE $f\n";
					$command1 .= " $f" . "_1" . ".fastq";
					$command2 .= " $f" . "_2" . ".fastq";
					$isPE = 1;
				}
				else {
					my @all = keys %barcodes_PE;
#					print COMBINER "noPE @all\n";
#					print "noPE $f\n";
					$command1 .= " $f.fastq";

				}

				if ($i == 0) {
					$files_to_map{$new_output} = $files_to_map{$f};  # keep the old mapping stuff for the first entry only
				}
				delete $files_to_map{$f}; # remove the old mapping stuff
				}


			if ($isPE) {
				$command = "$command1 > $new_output" . "_1.fastq\n$command2 > $new_output" . "_2.fastq\n";
				}
			else {
				$command = $command1 . " > $new_output.fastq";
				}

#			print "$command > $new_output.fastq\n";
			print COMBINER "$command\n";
			}
#		print "have $num_exp\t$exp\t@{$unique_experiments{$exp}}\n";
	}
	print "\tNumber of experiments in unique identifier loop (to be combined for analysis) = $num_exp\n";
	close COMBINER;
#MCH#	print INSTRUCTIONS "# combined experiments that were run across multiple lanes (not always necessary, but doesn't hurt to run\n";
#MCH#	print INSTRUCTIONS "sh $combiner_file\n";
#	print "finished combining\n";

	make_barcode_batch_file(\%barcodes,\%name_translate, \%barcodes_PE, $update, $clip);

	my $count_files_by_species;

#	print "Number of experiment before calling make_mapping_batch = $num_exp\n";

	if (!$genome_assembly) {
#		$count_files_by_species = make_mapping_batch_file(\%files_to_map, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $rRNA_filter);
		$count_files_by_species = make_mapping_batch_file(\%files_to_map, $no_processed_genomes, $make_lwgv, $mapping_prog, $COPRO_seq, $update, $rRNA_filter, $num_exp);
		}
	else {
		make_assembly_batch_file($must_trim_index, \%files_to_map);
		}

	if ($GEO) {
		#make_srf_convert_batch_file(\%files_to_map);
		make_GEO_helper_file(\%files_to_map); # file that contains most of the info filed out for GEO
		}

	unless ($genome_assembly || $COPRO_seq) {
		make_RNA_seq_norm_batch($count_files_by_species, $output_format, $USR_GENOMES_DIR);
		}

	make_lwgv_batch(keys %$count_files_by_species) if $make_lwgv;

	}

sub transfer_sequence_files {
	my ($p, $seq_file, $fastq_file, $unique_files)=@_;

	my $has_index = 0;
	if ($p->{'platform'} && $p->{'platform'} =~ /PE/ && $p->{'platform'} =~ /fastq/) {  # most recent fastq format
#		print "\n\nPE found\n\n";
		my ($seq_fileA, $seq_fileB);
		my ($fastq_fileA, $fastq_fileB);
		$seq_fileA = $seq_fileB = $seq_file;
		$fastq_fileA = $fastq_fileB = $fastq_file;
		$seq_fileA =~ s/_sequence/_R1_001.fastq.gz/;
		$seq_fileB =~ s/_sequence/_R2_001.fastq.gz/;
		$seq_fileA =~ s/\.txt$//;
		$seq_fileB =~ s/\.txt$//;
		$seq_fileA =~ s/\/s_(\d+)/\/lane$1_NoIndex_L00$1/;
		$seq_fileB =~ s/\/s_(\d+)/\/lane$1_NoIndex_L00$1/;

		$fastq_fileA =~ s/\.fastq/_1.fastq/;
		$fastq_fileB =~ s/\.fastq/_2.fastq/;

#		die "have files $seq_fileA\t$seq_fileB\n";

		move_file_sym_link($unique_files, $seq_fileA, $fastq_fileA);
		move_file_sym_link($unique_files, $seq_fileB, $fastq_fileB);
		}

	elsif ($p->{'platform'} && $p->{'platform'} =~ /fastq/) {  # most recent fastq format
#		print "\n\nfastq\n\n";
		my ($seq_fileA);
		my ($fastq_fileA);
		$seq_fileA =  $seq_file;
		$fastq_fileA =  $fastq_file;
#		$seq_fileA =~ s/_sequence/_R1_001.fastq.gz/;
#		$seq_fileA =~ s/\.txt$//;
#		$seq_fileA =~ s/\/s_(\d+)/\/lane$1_NoIndex_L00$1/;

		move_file_sym_link($unique_files, $seq_fileA, $fastq_fileA);
		}

	elsif ($p->{'platform'} && $p->{'platform'} =~ /PE/) {
#		print "\n\nlast elsif PE?\n\n";
		my ($seq_fileA, $seq_fileB);
		my ($fastq_fileA, $fastq_fileB);
		$seq_fileA = $seq_fileB = $seq_file;
		$fastq_fileA = $fastq_fileB = $fastq_file;
		$seq_fileA =~ s/_sequence/_1_sequence/;
		$seq_fileB =~ s/_sequence/_2_sequence/;

		$fastq_fileA =~ s/\.fastq/_1.fastq/;
		$fastq_fileB =~ s/\.fastq/_2.fastq/;

		unless (-e $seq_fileB) {  # sometimes the files are named like this if they contain indexed reads
			$seq_fileB = $seq_file;
			$seq_fileB =~ s/_sequence/_3_sequence/;
		}

		unless (-e $seq_fileB && -e $seq_fileA) { # sometimes the files are named "withindex" if they have an index inside; then we have to remove the index
			$has_index = 1;
			$seq_fileB =~ s/3_sequence/3_withindex_sequence/;
			$seq_fileA =~ s/1_sequence/1_withindex_sequence/;
#			print STERRR "HERE CHANGING\n";
		}

		move_file_sym_link($unique_files, $seq_fileA, $fastq_fileA);
		move_file_sym_link($unique_files, $seq_fileB, $fastq_fileB);
#		die "Paired-end!\n";
		}

	elsif ($p->{'platform'} && $p->{'platform'} =~ /miseq/i) {
#		if ($p->{platform} =~ /miseq/i)  {
#			print "\n\nScript recognizes miseq format\n\n";

			$fastq_file =~ s/\.fastq/\.fastq/;

#MCH#		my $miseq_id = $p->{lane};

#			$seq_file =~ s/solexa\/results/miseq$miseq_id/;
#			$seq_file =~ s/s_\d_sequence.txt$//;
#			$seq_file .= "/Data/Intensities/BaseCalls/";

#			print "have $p->{platform} for $p->{experiment_name}\t$seq_file\t$fastq_file\n";
#			have to physically copy the file over because there are several files that need concatenation #MCH# I don't think so...

#			print "copying fastq file in $seq_file/ into $fastq_file\n";
		#	system("zcat $seq_file/*.fastq.gz > $fastq_file");

#MCH#		unless (-e $fastq_file) {
#MCH#			print "\nzcatting seqfile to fastqfile\n";
#MCH#			system("zcat $seq_file > $fastq_file");
#MCH#			}

#MCH		print "\n\nmiseq: seq_file = $seq_file\n";
#MCH		print "miseq: fastq_file = $fastq_file\n\n";

		move_file_sym_link($unique_files, $seq_file, $fastq_file);

			}
	else {
		move_file_sym_link($unique_files, $seq_file, $fastq_file);
		}

	return $has_index;
}

sub move_file_sym_link {
	my ($unique_files, $seq_file, $fastq_file) = @_;

	# don't want to grab the same file 2x
	if (!$unique_files->{$fastq_file}) {
		# COPY THE FILE OVER; need to check if file exists
#		print "$seq_file to $fastq_file\n";
		unless (-e $fastq_file) {
			print STDERR "Linking $seq_file to $fastq_file\n";
#			system("cp $seq_file $fastq_file");
			system("ln -s $seq_file $fastq_file");
		}
		$unique_files->{$fastq_file}=1;
	}
}

sub make_GEO_helper_file {
	my $files = shift;

#	my $accession_to_species_name = get_accession_to_species_name();
	system("mkdir GEO_DATA");
	system("rm GEO_DATA/*.fastq");
	system("rm GEO_DATA/*.counts");


#	my $normalized_data = parse_normalized_data();

	my $out = "GEO_DATA/geo_helper.txt";
	open (OUT, ">$out") or die "can't open $out: $!\n";


	print OUT "Sample name\ttitle\tsource name\torganism\tmolecule\tdescription\tprocessed data file\tprocessed data file type\tprocessed data file MD5 checksum\traw file\traw file type\traw file MD5 checksum\n";
	for my $f (keys %$files) {
		my $p = $files->{$f};
		my $title = $p->{experiment_name};
		my $description = "";
		$description = $p->{description_geo} if $p->{description_geo};
#		die "description is $description\n";
		my $source_name = infer_source($title, $description);
		my $organism = get_organism(\%accession_to_species_name, $p->{species});
		my $MD5_raw = get_MD5_hash("$f.fastq");
		my $MD5_processed = get_MD5_hash("$title.counts");
		system("ln $title.counts GEO_DATA/$title.counts");
		print OUT "$f\t$title\t$source_name\t$organism\trRNA depleted total RNA\t$description\t$title.counts\ttxt\t$MD5_processed\t$f.fastq\tIllumina_native\t$MD5_raw\n";


		if ($f =~ /_BC/) { # make a symbolic link to the source file for gzipping
			system("ln $f.fastq GEO_DATA/$f.fastq");
		}
		else { # sequence files that haven't been split (i.e. those without a barcode; are ALREADY symbolic links, so just copy the link [making a symbolic link to a symbolic link fails on the cluster])
			system("cp -P $f.fastq GEO_DATA/$f.fastq");
		}
	}



	my $treatment_protocol = 'Cecal and fecal samples obtained from mice at the time that they were sacrificed, as well as bacteria in vitro cultures, were immediately frozen at -80°C and maintained at this temperature prior to processing. All samples were treated with RNAProtect (Qiagen).';
	my $library_construction_protocol = "Libraries were prepared according to Illumina's instructions accompanying the genomic DNA Sample Kit. Briefly, cDNA was sonicated in a biorupter sonicator, cleaned up/concentrated through a Qiagen PCR column, and end-repaired. The blunt DNA was treated with Klenow fragment (exo minus) to add an A-overhang, and ligated to the relevant Illumina adapter sequence (either with our without a barcode).  Adaptered-DNA was then size-selected on a agarose gel and PCR amplified. The purified PCR was captured on an Illumina flow cell for cluster generation. Libraries were sequenced on the Genome Analyzer following the manufacturer's protocols.";
	my $data_processing = "After dividing sequence runs by barcode, we mapped the reads to the relevant genomes using the $MAPPING_PROGRAM algorithm. Minimum score thresholds for ssaha were selected based on the distribution of scores for all mapped reads of a 32nt barcoded sample and a 36-nt non-barcoded sample (29 was selected as the minimum score for 32nt barcoded samples; 33 was the minimum score used for 36-nt non-barcoded samples). Although an 18-nt read is sufficient to map more than 90% of the sequencing reads, even at 32-36nt there is a large fraction of the reads that map to multiple locations within a genome or across genomes. Reads that map non-uniquely were added to each gene in proportion to each gene’s fraction of unique-match counts (e.g., a non-unique read that maps equally well to gene A with 18 unique reads and gene B with 2 unique reads will be scored as 0.9 count to gene A and 0.1 count to gene B). We added a pseudocount (i.e. added 1) to each gene count prior to normalization to account for differences in sampling depth, although clearly a more appropriate model-based approach to smooth lower expression values is needed in the future, particularly with mixed species samples where one or more species is clearly undersampled. Raw counts were normalized to reads/kb/million mapped reads";
	print OUT "\n";
	print OUT "PROTOCOLS\n";
	print OUT "growth protocol\tfill out\n";
	print OUT "treatment protocol\t$treatment_protocol\n";
	print OUT "library construction protocol\t$library_construction_protocol\n";
	print OUT "library strategy\tRNA-Seq\n";
	print OUT "library source\tgenomic\n";
	print OUT "library selection\tcDNA\n";
	print OUT "instrument model\tIllumina Genome Analyzer II\n";
	print OUT "data processing\t$data_processing\n";

	close OUT;
}

sub parse_normalized_data {
	my @files = `find *normalized | grep '\\.normalized'`;

	my %normalized_data;
	for my $f (@files) {
		chomp $f;
		open (NORMALIZED, $f) or die "can't open $f: $!\n";


		my $header = <NORMALIZED>;
		die "$header read from $f\n";

	}
}

sub get_MD5_hash {
	my $file = shift;
	my $md5 = "";
	if (-e $file) {
		my $md5_info = `md5sum $file`;
		my ($md5_hash, $junk) = split /\s/, $md5_info;
		$md5 = $md5_hash;
	}
	else {
		if ($file =~ /fastq/) {
			print STDERR "Warning: couldn't find $file to make an MD5 hash (maybe you need to split your barcodes)\n";
		}
		elsif ($file =~ /counts/) {
			print STDERR "Warning: couldn't find $file to make an MD5 hash (maybe you need to run map_counts.pl)\n";
		}
		else {
			print STDERR "Warning: couldn't find $file to make an MD5 hash (maybe you need to split your barcodes)\n";
		}
	}

	return $md5;
}

sub get_organism {
	my ($accession_to_species_name, $species_list) = @_;

	$species_list =~ s/\s+//g;
	my @s = split /,/, $species_list;

	my @n;

	for my $s (@s) {
		if ($accession_to_species_name->{$s}) {
			push @n, $accession_to_species_name->{$s};
		}
		else {
			push @n, $s;
			print STDERR "Warning couldn't find $s in species list\n";
		}
	}

	return join ",", @n;
}

sub infer_source {
	my $title = shift;
	my $description = shift;

	my $type = 'in vitro culture';

	if ($title =~ /FP/ || $description =~ /fecal/ || $description =~ /feces/) {
		$type = 'feces';

		if ($description =~ /mouse/ || $description =~ /mice/) {
			$type = 'mouse feces';
		}
	}
	elsif ($title =~ /CC/ || $description =~ /cecal/ || $description =~ /cecum/) {
		$type = 'cecal contents';

		if ($description =~ /mouse/ || $description =~ /mice/) {
			$type = 'mouse cecal contents';
		}
	}


	return $type;
}

sub make_srf_convert_batch_file {
	my $files=shift;

        my $batch = "illumina_to_srf.sh";
	system("mkdir GEO_DATA");
	open (OUT, ">$batch") or die "can't open $batch: $!\n";
	for my $k (keys %$files) {
		my $outfile = $k . ".srf";
		$outfile =~ s/fastq$/srf/;
		print OUT "illumina2srf $k.fastq -o GEO_DATA/$outfile\n";
	}
	close OUT;
}

sub make_lwgv_batch {
	my @species = @_;
	my $batch = "make_lwgv_tracks_batch.sh";
	open (OUT, ">$batch") or die "can't open $batch: $!\n";
        for my $s (@species) {
		print OUT "perl $LWGV_TRACK $s\n";
        #        print "have species $s\n";
        }
	close OUT;

#	print INSTRUCTIONS "# make a combined lwgv track for each species (note to make a separate track for EACH experiment add a 1 after the species name/accession)\n";
#	print INSTRUCTIONS "sh $batch\n";
}


sub make_RNA_seq_norm_batch {
	my $count_files_by_species = shift;
	my $output_format = shift;
	my $genome_source_dir = shift;

	my $batch = "RNA_seq_norm_batch.sh";
	open (OUT, ">$batch") or die "can't open $batch: $!\n";

	print OUT "#!/bin/bash\n";
	
#	print INSTRUCTIONS "# normalize each species counts separately\n";
#	print INSTRUCTIONS "sh $batch\n";
	for my $k (sort keys %$count_files_by_species) {
		print OUT "\n# ******   set up normalization scripts for $k ******\n";
		my $outdir = $k . "_normalized";
		my @files = @{$count_files_by_species->{$k}};
		print OUT "# make species directory\n";
		print OUT "mkdir $outdir\n";
		print OUT "#move $k files into species-specific directory\n";
		print OUT "cp @files $outdir\n";
		print OUT "#move $k gene position file into species-specific directory\n";
#MCH 	This next command may need modification based the order of operations
		print OUT "cp $k.gene_pos $outdir\n";
		print OUT "#position into outdir\n";
		print OUT "cd $outdir\n";
		print OUT "#run RNA_seq_norm.pl\n";

		my $outfile = "$k.normalized";
		if ($output_format && $output_format eq 'matlab') {
			print OUT "perl $RNA_SEQ_NORM -g $k.gene_pos -f matlab > $outfile\n";
		}
		else {
			print OUT "perl $RNA_SEQ_NORM -g $k.gene_pos > $outfile\n";
		}
		print OUT "# move back into current directory\n";
		print OUT "cd ..\n";
#		print "for $k have @files\n";
	}

	close OUT;
}

sub make_assembly_batch_file {
	my $trim_index = shift;
	my $genomes = shift;
#	my ($genomes) = @_;

	my $batch = "assembly_batch.sh";
	open (OUT, ">$batch") or die "can't open $batch: $!\n";
	my $batch2 = "assembly_final.sh";
	open (OUT2, ">$batch2") or die "can't open $batch: $!\n";

	for my $g (keys %$genomes) {
		my $outfile = "$g.fa";
		my $outdir = "assembly_$g";
		if ($genomes->{$g}{experiment_name}) {
			$outdir = "assembly_" . $genomes->{$g}{experiment_name};
		}
		my ($fastq_fileA, $fastq_fileB);
		my $fastq_file = "$g.scarf";
		$fastq_fileA = $fastq_fileB = $fastq_file;
		$fastq_fileA =~ s/\.fastq/_1.fastq/;
		$fastq_fileB =~ s/\.fastq/_2.fastq/;

#MCH#	Must fix this - needs to be set up to deal with fastq files or prepare for MIRA
		my $command = "perl $SOLEXA_TO_FASTA_PE $fastq_fileA $fastq_fileB > $outfile";
		if ($trim_index) {
			$command = "perl $SOLEXA_TO_FASTA_PE $fastq_fileA $fastq_fileB 101 > $outfile"; # trim to 101 bp
			print "Warning: trimming to 101 basepairs because need to remove index (this assumes this read is a PE101bp)\n";
		}
#		$SOLEXA_TO_FASTA_PE = "$Bin/../solexaToFastaPE.pl";
#		$command .= "; $VELVET_ASSEMBLER --t 1 -p $outdir -f '-shortPaired $outfile'";
		$command .= "; $VELVET_ASSEMBLER -s 31 -e 31 --t 1 -p $outdir -f '-shortPaired $outfile'";
		$command .= "; rm $outfile";
#perl ~/velvet_1.0.12/contrib/VelvetOptimiser-2.1.7/VelvetOptimiser.pl -p 100G_BC16 -f '-short run351_lane4_BC16.fa -shortPaired run370.1_lane3_BC16.fa -shortPaired2 run370.1_lane2_BC16.fa'
		print OUT "$command\n";
		print OUT2 "$ASSEMBLY_FINAL $genomes->{$g}{experiment_name} $genomes->{$g}{genome_name}\n";

#blah

	}
	close OUT;
	close OUT2;

}

sub make_mapping_batch_file {
	my ($experiments, $no_processed_genomes, $make_lwgv, $mapping_prog, $COPRO, $update, $rRNA_filter, $num_exp) = @_;
#	my ($experiments, $no_microbialomics, $make_lwgv, $mapping_prog, $COPRO, $update) = @_;
#	die "mapping prog is $mapping_prog\n";

	print "Generating mapping batch file for $mapping_prog...\n";

	my %all_species;
	my %all_species_counts;
	my %seq_db_files;

	my %species_list;
	for my $f (keys %$experiments) {
		my $species = $experiments->{$f}{species};
		my @s = split /,/, $species;
		for my $s (@s) {
			$species_list{$s}=1;
		}
	}
	my @species_list = keys %species_list;
	process_genomes(\@species_list, $make_lwgv, $COPRO) unless $no_processed_genomes;
	close GENOME_LOG;
	my $batch = "mapping_batch.sh";
	open (OUT, ">$batch") or die "can't open $batch: $!\n";
	my $batch_counts = "map_counts_batch.sh";
	open (OUT_COUNTS, ">$batch_counts") or die "can't open $batch: $!\n";
	my $num_mapping_jobs = 0;

	if ($rRNA_filter eq 'post') {
		open(RRNA_REMOVE, '>rRNA_remove.sh') or die "can't open rRNA_remove.sh: $!\n";
	}

	for my $f (sort keys %$experiments) {
		my $species = $experiments->{$f}{species};
		my @s = split /,/, $species;
		@s = sort {$a cmp $b} @s;
		my $db = join "_", @s;
		$db .= ".fa";

		# learned from Nate and Ansel that there is a maximum file name length of 255 bytes in unix
#		if (length($db) > 254) {
#			$db = md5_base64($db) . ".fa";
#			$db = md5_hex($db) . ".fa";
#			die "sequence db too long ($digest) $sequence_db\n";
#		}

		#MCH# To be consistent, I required the md5 conversion every time.  The concat accessions are cumbersome.
		$db = md5_hex($db) . ".fa";

#		if (!$no_microbialomics && !$seq_db_files{$db}) {
		if (!$seq_db_files{$db}) {  #no_processed_genomes
#			print "Generating bowtie database for @s\n";
#			generate_sequence_database($db, $mapping_prog, $USR_GENOMES_DIR, @s);
			generate_sequence_database($db, $mapping_prog, @s);
			$seq_db_files{$db} = 1;
		}
		# create a fasta file; map fasta file to the db; remove the fasta file

		my $fa = "$f.fa";
		my $fs = "$f.fastq";
		my $mapping_out = "$f.mapped";
		my $fs_exists = -e $fs;
		my $fs_size = -s $fs;
		my $fasta_out = $fa;

		if ($num_exp == 1) {
			$fa = "$experiments->{$f}{experiment_name}.fa";
			$fs = "$experiments->{$f}{experiment_name}.fastq";
			$mapping_out = "$experiments->{$f}{experiment_name}.mapped";
			my $naming_correction = $experiments->{$f}{experiment_name}.".fastq";
			unless (-e $naming_correction) {
				system("ln -s $f.fastq $naming_correction");
				}
			}

		my $filter = "";

		if ($rRNA_filter eq 'pre') {
			$filter = "| perl $RRNA_REMOVAL_FILTER $mapping_out $mapping_prog $db 1 ";
			#$filter = " perl $RRNA_REMOVAL_FILTER $mapping_out $mapping_prog 1 | ";
		}

		my $max_file_for_tmp = 20000000000; # roughly size of tmp on cluster (Neel modified this 2016)
		if (!$fs_exists) {
#			print STDERR "Couldn't find $fs; pipeline will run barcode_batch.sh to demultiplex (and combine_multirun_experiments.sh if sequencing over multiple lanes) and rerun build_batch_jobs.pl to complete batch file setup\n";
#			print STDERR "Warning: couldn't find $fs; split barcodes and rerun build_batch_jobs.pl if some of your split files may be larger than 7GB\n";
			$fasta_out = "/tmp/$fa";
		}
		elsif ($fs_size < $max_file_for_tmp) {
			$fasta_out = "/tmp/$fa";
		}
		else {
			print STDERR "$fs is too large ($fs_size) to put in /tmp ($max_file_for_tmp)\n";
		}
#		die "size is $fs_size\n";

		my $command = "";

		#mapping with bowtie, output as SAM, converted to BAM, then sorted by qname (read name) rather than genome coordinate
		if ($mapping_prog eq 'bowtie') {
			if ($num_exp > 1) {
				#$command = "bowtie -a --best --strata -q -v 1 $db $fs | $filter gzip --best > $mapping_out";
				$command = "bowtie -S -a --best --strata -q -p \${SLURM_CPUS_PER_TASK} -v 1 $db $fs $filter > $mapping_out";
				}
			else {
				#$command = "bowtie -a --best --strata -q -v 1 $db $f.fastq | $filter gzip --best > $mapping_out";
				$command = "bowtie -S -a --best --strata -q -p \${SLURM_CPUS_PER_TASK} -v 1 $db $f.fastq $filter > $mapping_out";
				}
			}
		elsif ($mapping_prog eq 'bowtie2') {
			if ($num_exp > 1) {
				#$command = "bowtie2 --no-unal --end-to-end -a -p 4 -x $db -U $fs | $filter gzip --best > $mapping_out";
				$command = "bowtie2 --no-unal --end-to-end -a -p \${SLURM_CPUS_PER_TASK} -x $db -U $fs $filter | samtools view -hbS | samtools sort -n - -o $mapping_out";
				}
			else {
				#$command = "bowtie2 --no-unal --end-to-end -a -p 4 -x $db -U $f.fastq | $filter gzip --best > $mapping_out";
				$command = "bowtie2 --no-unal --end-to-end -a -p \${SLURM_CPUS_PER_TASK} -x $db -U $f.fastq $filter | samtools view -hbS | samtools sort -n - -o $mapping_out";
				}
			}
		else {
			die "mapping program not recognized...\n";
			}

		# bowtie
		# -a --best --strata
		# bowtie2
		# --no-unal --end-to-end -a -f -p 4
		$num_mapping_jobs++;

		print OUT "$command\n" unless ($update && -e $mapping_out);

		if ($rRNA_filter eq 'post') {
			my $rRNA_filter_out = $mapping_out;
			$rRNA_filter_out =~ s/mapped/mapped_CDS/g;
#			print "Generating rRNA_removal batch file...\n";
			#print RRNA_REMOVE "perl $RRNA_REMOVAL_FILTER $mapping_out $mapping_prog | gzip --best > $rRNA_filter_out\n" unless ($update && -e $rRNA_filter_out);
#			print RRNA_REMOVE "perl $RRNA_REMOVAL_FILTER $mapping_out $mapping_prog $db > $rRNA_filter_out\n" unless ($update && -e $rRNA_filter_out);
			print RRNA_REMOVE "perl $RRNA_REMOVAL_FILTER $mapping_out $mapping_prog $db\n" unless ($update && -e $rRNA_filter_out);
			$mapping_out = $rRNA_filter_out; # this makes the map_counts read from the filtered data
		}

		my $outfile = "$f.counts";
#		print "Outfile base = $outfile\n";
		if ($experiments->{$f}{experiment_name}) {
			$outfile = $experiments->{$f}{experiment_name} . ".counts";
			}
		my $min_score = 33;
		if ($update && -e $outfile) {
#			print "here skipping $outfile\n";
			next;
		}

		if ($experiments->{$f}{barcode}) { # barcode runs are 4bp shorter
			my $barcode = $experiments->{$f}{barcode};

			if ($barcode =~ /^PE8BC/ || $barcode =~ /^8C/ || $barcode =~ /^8BC/)  { # 8 bp barcode
				$min_score = 25;
			}
			else { # default is 4bp barcode
				$min_score = 29;
			}
		}
		if ($experiments->{$f}{read_length} && $experiments->{$f}{read_length} < 36) {
			$min_score -= 36 - $experiments->{$f}{read_length};
		}
#		my $ssaha_out = "$f.ssaha_mapped.gz";

#		print "Generating counting batch file...\n";

		my $counts_command = "perl $MAP_COUNT -i $mapping_out -m $min_score -C -r -o $outfile -d $db";
		if ($COPRO) {
			$counts_command = "perl $MAP_COUNT -i $mapping_out -m $min_score -o $outfile -d $db";
		}
		if ($make_lwgv) {
			$counts_command = "";
			for my $s (@s) {
#				$outfile = "$f.$s.counts";
				$outfile = "$experiments->{$f}{experiment_name}.$s.counts"; #MCH My preference is to have counts files listed by sample name
				if ($exclude_ties) {
					$counts_command .= "perl $MAP_COUNT -i $mapping_out -m $min_score -o $outfile -g $s -f 2 -X\n";
				}
				else {
					$counts_command .= "perl $MAP_COUNT -i $mapping_out -m $min_score -o $outfile -g $s -f 2\n";
				}
				push @{$all_species{$s}}, $outfile;
			};
		}
#		elsif($COPRO) {
#			$counts_command = "";
#			for my $s (@s) {
#				$outfile = "$f.$s.counts";
#				$counts_command .= "perl $MAP_COUNT -i $ssaha_out -m $min_score -o $outfile\n";
#				push @{$all_species{$s}}, $outfile;
#			};
#		}
		else {
			for my $s (@s) { push @{$all_species{$s}}, $outfile; }
		}

		if ($exclude_ties) {
			if (!$make_lwgv) {
				$counts_command .= " -X";
			}
		}


		print OUT_COUNTS "$counts_command\n";
#		for my $s (@s) { push @{$all_species_counts{$s}}, $ssaha_out };
	}
	close OUT;

#	print INSTRUCTIONS "\n#map sequencing reads to a fasta database using $MAPPING_PROGRAM\n";
#	print INSTRUCTIONS "# note this requires a lot of memory\n";
#	print INSTRUCTIONS "# if you map more than ~40 files at once ";
#	print INSTRUCTIONS " there is a high chance of crashing the cgscluster\n";
	my $max_jobs_at_once = 40;
	my $num_jobs_per_nq = int($num_mapping_jobs/$max_jobs_at_once) + 1;
#	print INSTRUCTIONS "# recommended command:\n";
	if ($num_jobs_per_nq > 1) {
#		print INSTRUCTIONS "nq $batch $num_jobs_per_nq | qsub -l h_vmem=4G -P long\n";
	}
	else {
	#	print INSTRUCTIONS "nq $batch | qsub  -l h_vmem=4G\n";
	}
	if ($rRNA_filter eq 'post') {
	#	print INSTRUCTIONS "\n#remove rRNA reads\n";
	#	print INSTRUCTIONS "nq rRNA_remove.sh | qsub\n";

	}

#	print INSTRUCTIONS "\n#obtain the counts of each gene from the mapped reads\n";
#	print INSTRUCTIONS "nq $batch_counts | qsub\n";

	# make the LWGV map_counts batch script here
#	if ($make_lwgv) {
#		my $lwgv_batch = "lwgv_batch.sh";
#		open (LWGV, $lwgv_batch) or die "can't open $lwgv_batch: $!\n";
#		for my $k (keys %all_species_counts) {
#			my $temp_out = $k . "_allMapped.gz";
#			print LWGV "cat @{$all_species_counts{$k}} > $temp_out\n";
#			print LWGV "$MAP_COUNT -i $temp_out -g
#		}
#		close LWGV;
#	}

	return \%all_species;
}

sub generate_sequence_database {
#	my ($sequence_db, $mapping_prog, $genome_source_dir, @species) = @_;
	my ($sequence_db, $mapping_prog, @species) = @_;
	print STDERR "My database is: $sequence_db\n";
#	die "have species @species at generate_sequence_database\n";

	for my $i (0 .. $#species) {
		my $s = $species[$i].".genome";	#MCH mod for genome suffix handling
		if ($i == 0) {
			open (SEQ_DB, ">$sequence_db") or die "can't open $sequence_db: $!\n";
			print SEQ_DB $ADAPTER_SEQ;
			close SEQ_DB;
		}
		system("cat $s >> $sequence_db");
	}

	if ($mapping_prog eq 'bowtie') {
		my $check_existing = "$sequence_db.rev.1.ebwt";
		if (-e $check_existing) {
			print STDERR "\tusing existing bowtie index for $sequence_db\n";
		}
		else {
			print STDERR "\tbuilding index $sequence_db for bowtie\n";
#			system("bowtie-build -o 5 $sequence_db $sequence_db");
			system("bowtie-build -o 4 $sequence_db $sequence_db");
			# -o 5 is the default; -o 3 takes 4 times the memory but runs 4x faster
#			system("bowtie-build -o 3 $sequence_db $sequence_db");
		}
	}
	elsif ($mapping_prog eq 'bowtie2') {
		my $check_existing = "$sequence_db.rev.1.bt2";
		if (-e $check_existing) {
			print STDERR "\tusing existing bowtie2 index for $sequence_db\n";
		}
		else {
			print STDERR "\tbuilding index $sequence_db for bowtie2\n";
			system("bowtie2-build -o 4 $sequence_db $sequence_db");
			# -o 5 is the default; -o 3 takes 4 times the memory but runs 4x faster
		}
	}
	#die;
}

sub to_fasta {
	my ($seqName, $seq, $len) = @_;

	$len = 80 unless $len;

	my $formatted_seq = ">$seqName\n";
	while (my $chunk = substr($seq, 0, $len, "")) {
		$formatted_seq .= "$chunk\n";
	}

	return $formatted_seq;
}

# make one big concatenated sequence
sub slurp_sequence {
	my $in = shift;

	open (IN, $in) or die "can't open $in: $!\n";

	my $seq = "";
	while (<IN>) {
		next if /^>/;
		s/\s+//;
		$_ = uc($_);
		$seq .= $_;
	}
	close IN;

	return $seq;
}

sub make_barcode_filename {
	my ($fastq_file, $barcode) = @_;

	my $base_file = $fastq_file;
	$base_file =~ s/\.fastq$//;

	my $file = $base_file . "_" . $barcode . "_R1.fastq";

	return $file;
}

sub make_barcode_batch_file {
	my ($barcodes, $name_translate, $barcodes_PE, $update, $clip) = @_;

	print "Generating demultiplexing batch file...\n";

	my $batch = "barcode_batch.sh";
#	print INSTRUCTIONS "#split the sequence files by barcode\n";
#	print INSTRUCTIONS "nq $batch | qsub\n";

	open (BATCH, ">$batch") or die "can't open $batch: $!\n";

	my $barcodes_not_yet_split = 0;
	for my $fastq_file (keys %$barcodes) {

		my $bc_file = $fastq_file;
#MCH#	$bc_file =~ s/\.scarf$/\.bc/;
		$bc_file =~ s/\.fastq$/\.bc/;
		my $base_file = $fastq_file;
		$base_file =~ s/\.fastq$//;

		my @BCs = @{$barcodes->{$fastq_file}};
		check_BC_uniqueness(@BCs);
		if ($update) { $barcodes_not_yet_split = check_BC_already_split($base_file, @BCs); }
		if ($update && !$barcodes_not_yet_split) { next; }

		if ($barcodes_PE->{$fastq_file}) {
			print BATCH "perl $SPLIT_BARCODES_PE $bc_file $fastq_file 1\n";
			}
		else {
			if ($clip) {
				print BATCH "perl $SPLIT_BARCODES $bc_file $fastq_file 1 $clip\n";
				}
			else {
				print BATCH "perl $SPLIT_BARCODES $bc_file $fastq_file 1\n";
				}
			}
		open (OUT, ">$bc_file") or die "can't open $bc_file: $!\n";

		for my $b (@BCs) {
			my $file = $base_file . "_" . $b . "_R1.fastq";
#			print "$file\n";

			if (!$BARCODES{$b}) {
				print STDERR "Error: unknown barcode: $b\n";
				exit(1);
			}
			else {
				print OUT "$b\t$BARCODES{$b}\t$file\n";
			}
		}
		#print "have $fastq_file @BCs\n";
		close OUT;
	}
	close BATCH;

}

sub check_BC_already_split {
	my ($base_file, @BCs) = @_;

	my $file_doesnt_exist = 0;

	for my $bc (@BCs) {
		my $file = $base_file . "_" . $bc . ".fastq";
		$file_doesnt_exist = 1 unless (-e $file);
	}

#	print "file doesn't exist is: $file_doesnt_exist\n";

	return $file_doesnt_exist;
}

sub check_BC_uniqueness {
	my (@BCs) = @_;

	my %unique;

	for my $bc (@BCs) {
		if ($unique{$bc}) {
			print "Error: duplicate barcode ($bc): @BCs\n";
			exit(1);
		}

		$unique{$bc} = 1;
	}
}

sub run_to_directory {
#J's original code
#	my $text=shift;
#	my $dir_name_len = 4;
#	$dir_base = "/srv/seq/solexa/results/";
#	print "updating $text\n";
#	my @pieces = split '\.', $text;
#	my $dir = shift @pieces;
#	print "dir now is $dir from @pieces\n";
#	while (length($dir) < $dir_name_len) {
#		$dir = "0" . $dir;
#		}
#	unshift @pieces, $dir;
#	my $final_dir = join ".", @pieces;
#	print "My final_dir is $final_dir, my dir_base is $dir_base\n";
#	return $dir_base . $final_dir . "/";

#MCH Update - simplified data directory read-in, provided as user input in flag -d
	my $text=shift;
	return $dir_base . $text . "/";
}

sub table_to_hash {
	my $in = shift;

	open (IN, $in) or die "can't open $in: $!\n";

	my $line = <IN>;
	chomp $line;
	my @headers = split /\t/, $line;
	for (@headers) { $_=lc($_); }
#	die "headers @headers\n";
	my @table;
	while (<IN>) {
		chomp;
		my @cols = split /\t/;

#		if (scalar @cols != scalar @headers) {
#			printf STDERR  "different number of columns and headers %d vs %d\n", scalar @cols, scalar @headers;
#			print "@headers\n@cols\n\n";
#		}
		my %hash;
		for my $i (0 .. $#cols) {
			$hash{$headers[$i]} = $cols[$i];
		}

		push @table, \%hash;
	}
	close IN;

	return \@table;
}

sub download_table {
	my ($url, $outfile) = @_;

	print STDERR "\n\n" . fancy_title("downloading $url to $outfile");

	sleep(1 + 2*rand()); # don't piss off google

	# note that $url below need to be surrounded by the single-quotes otherwise wget doesn't see the other parameters
	#                          what?  where?    tries?
	my $download_info = `wget '$url' -O $outfile -t 3`;

}

# little headers for telling people stuff
sub fancy_title {
	my $title = shift;
	my $head_bar;
	for (0 .. length($title)+11) { $head_bar .= '-'; }

	return "$head_bar\n|     $title     |\n$head_bar\n";
}

#this sub has been heavily modified by MCH to work through the genome storage file structure @ CGS.
sub process_genomes {
	my $species = shift;
	my $make_lwgv = shift;
	my $COPRO = shift;

#	return;
	print STDERR "Processing sequence files for @$species\n";
#	die "downloading sequence files for @$species\n";

	my $fh;
	my $fh2;
	if ($COPRO) {
		open($fh, ">genome_lengths.txt") or die "can't open genome_lengths.txt: $!\n";
		open($fh2, ">accession_to_species.txt") or die "can't open accession_to_species.txt: $!\n";
		}
	for my $s (@$species) {
		my $gen = $s.".genome";
		my $gp = $s.".gene_pos";
		print "\tMy gene_pos query is $gp\n";
		if (-s $gen && $gp) {
			print "\t\t$s genome and gene_pos files are already in current directory\n";
			print GENOME_LOG "\t$s\tgenome and gene_pos files are already in current directory\n";
			}
		elsif ($location eq "local") {
			if (-e $USR_GENOMES_DIR.$gen && $USR_GENOMES_DIR.$gp) {
				system("cp $USR_GENOMES_DIR$gen ./$gen");
				system("cp $USR_GENOMES_DIR$gp ./$gp");
				print "\t\t$s genome and gene_pos files copied from $USR_GENOMES_DIR to current directory\n";
				print GENOME_LOG "\t$s\tgenome and gene_pos files copied from $USR_GENOMES_DIR to current directory\n";
				}
			else {
				my ($status, $genome_seq, $genome_length, $genome_name) = get_local_genome_data($s, $USR_GENOMES_DIR);
				if ($status) {
					print "\tWriting .genome and .gene_pos files for $s\n";
					my $features = write_sequence_file($s, $genome_seq, $make_lwgv, $COPRO);
					write_position_file($s, $features);
					if (-e $gen && $gp) {
						print "\t\t$s genome and gene_pos files created from ncbi-formatted files in $USR_GENOMES_DIR\n";
						print GENOME_LOG "\t$s\tgenome and gene_pos files created from ncbi-formatted files in $USR_GENOMES_DIR\n";
						}
					else {
						die "Unable to process ncbi-formatted local files for $s after retrieval from $USR_GENOMES_DIR\n";
						}
					}
				else {
					die "Unable to retrieve ncbi-formatted local files for $s from $USR_GENOMES_DIR\n";
					}
				}
			}
		elsif ($location eq "ncbi") {
			my ($status, $genome_seq, $genome_length, $genome_name) = get_ncbi_genome_data($s, $NCBI_GENOMES_DIR);
			if ($status) {
				print "\tWriting .genome and .gene_pos files for $s\n";
				my $features = write_sequence_file($s, $genome_seq, $make_lwgv, $COPRO);
				write_position_file($s, $features);
				if (-e $gen && $gp) {
					print "\t\t$s genome and gene_pos files created from ncbi-formatted files in $NCBI_GENOMES_DIR\n";
					print GENOME_LOG "\t$s\tgenome and gene_pos files created from ncbi-formatted files in $NCBI_GENOMES_DIR\n";
					}
				else {
					die "Unable to process ncbi-formatted files for $s after retrieval from $NCBI_GENOMES_DIR\n";
					}
				}
			else {
				die "Unable to retrieve ncbi-formatted files for $s from $NCBI_GENOMES_DIR\n";
				}
			}
		elsif ($location eq "both") {
			if (-e $USR_GENOMES_DIR.$gen && $USR_GENOMES_DIR.$gp) {
				system("cp $USR_GENOMES_DIR$gen ./$gen");
				system("cp $USR_GENOMES_DIR$gp ./$gp");
				print "\t\t$s genome and gene_pos files copied from $USR_GENOMES_DIR to current directory\n";
				print GENOME_LOG "\t$s\tgenome and gene_pos files copied from $USR_GENOMES_DIR to current directory\n";
				}
			else {
				my ($status, $genome_seq, $genome_length, $genome_name) = get_local_genome_data($s, $USR_GENOMES_DIR);
				if ($status) {
					print "\tWriting .genome and .gene_pos files for $s\n";
					my $features = write_sequence_file($s, $genome_seq, $make_lwgv, $COPRO);
					write_position_file($s, $features);
					if (-e $gen && $gp) {
						print "\t\t$s genome and gene_pos files created from ncbi-formatted files in $USR_GENOMES_DIR\n";
						print GENOME_LOG "\t$s\tgenome and gene_pos files created from ncbi-formatted files in $USR_GENOMES_DIR\n";
						}
					else {
						die "Unable to process ncbi-formatted local files for $s after retrieval from $USR_GENOMES_DIR\n";
						}
					}
				else {
					my ($status, $genome_seq, $genome_length, $genome_name) = get_ncbi_genome_data($s, $NCBI_GENOMES_DIR);
					if ($status) {
						print "\tWriting .genome and .gene_pos files for $s\n";
						my $features = write_sequence_file($s, $genome_seq, $make_lwgv, $COPRO);
						write_position_file($s, $features);
						if (-e $gen && $gp) {
							print "\t\t$s genome and gene_pos files created from ncbi-formatted files in $NCBI_GENOMES_DIR\n";
							print GENOME_LOG "\t$s\tgenome and gene_pos files created from ncbi-formatted files in $NCBI_GENOMES_DIR\n";
							}
						else {
							die "Unable to process ncbi-formatted files for $s after retrieval from $NCBI_GENOMES_DIR\n";
							}
						}
					else {
						die "Unable to retrieve ncbi-formatted files for $s from $USR_GENOMES_DIR\n";
						}
					}
				}
			}
		else {
			die "Genome file location information unrecognized!\n";
			}
		}
	if ($COPRO) { close $fh; close $fh2;}
#	die "finished downloading\n";
	}

sub get_local_genome_data {
	my $constant_id = shift;
	my $dir = shift;
	my $fna = "$dir"."$constant_id.fna";
	my $ptt = "$dir"."$constant_id.ptt";
	my $rnt = "$dir"."$constant_id.rnt";
	my $gb = "$dir"."$constant_id.gb"; # only genbank file convert to ptt rnt fna
	my $gbk = "$dir"."$constant_id.gbk"; # only genbank file convert to ptt rnt fna

#	print "$gbk\n";

	if (-e $fna && -e $ptt && -e $rnt) { # look for a user provided sequence
		print "\tUsing user provided sequence files for $constant_id\n";
		system("cp $fna ./$constant_id.fna");
		system("cp $ptt ./$constant_id.ptt");
		system("cp $rnt ./$constant_id.rnt");

		my $seq = slurp_sequence("$constant_id.fna");
#		die "seq is $seq\n";
		return 1, $seq, length($seq), $constant_id;
		}
	elsif (-e $gb) {
		unless (-e "./$constant_id.gbk") {
			system("cp $gb ./$constant_id.gbk");
			print STDERR "\tUser provided sequence files for $constant_id\tcopying $gb to $gbk and create fna, ptt, rnt from genbank\n";
			}
		system("perl $GENBANK_TO_PTT_RNT_FNA $constant_id.gbk");
		my $seq = slurp_sequence("$constant_id.fna");
#		die "have $seq\n";
		return 1, $seq, length($seq), $constant_id;
		}
	elsif (-e $gbk) {
		unless (-e "./$constant_id.gbk") {
			print STDERR "\tUser provided sequence files for $constant_id\tcreate fna, ptt, rnt from genbank\n";
			system("cp $gbk ./$constant_id.gbk");
			}
		system("perl $GENBANK_TO_PTT_RNT_FNA $constant_id.gbk");
		my $seq = slurp_sequence("$constant_id.fna");
#		die "have $seq\n";
		return 1, $seq, length($seq), $constant_id;
		}
	else {
		print "\tError: No genome with genome_constant_id (NCBI accession) $constant_id in $dir\n";
		exit(1);
		}
	return;
	}

#MCH#
sub get_ncbi_genome_data {
	my $constant_id = shift;
	my $dir = shift;
	print "\tChecking local NCBI genomes mirror: $dir for $constant_id\n";
	my $fna = $constant_id.".fna";
	my $ptt = $constant_id.".ptt";
	my $rnt = $constant_id.".rnt";
	my $gbk = $constant_id.".gbk";
	my @tmp = ();
	$tmp[0] = `find $dir -name $fna`;
	$tmp[1] = `find $dir -name $ptt`;
	$tmp[2] = `find $dir -name $rnt`;
	if ($tmp[0] ne "" && $tmp[1] ne "" && $tmp[2] ne "") {
		foreach (@tmp) {
			chomp;
			system("ln -s $_ ./")
			}
		}
	if (-e $fna && -e $ptt && -e $rnt) {
		print "\t$constant_id fna, ptt, and rnt files copied, proceeding\n";
		my $seq = slurp_sequence("$fna");
		return 1, $seq, length($seq), $constant_id;
		}
	else {
		$tmp[3] = `find $dir -name $gbk`;
		chomp $tmp[3];
		if ($tmp[3] ne "")	{
			system("ln -s $tmp[3] ./");
			}
		if (-e $gbk) {
			print STDERR "\t$constant_id gbk file copied, proceeding\n";
			system("perl $GENBANK_TO_PTT_RNT_FNA $gbk");
			my $seq = slurp_sequence("$fna");
			return 1, $seq, length($seq), $constant_id;
			}
		else {
			die "\tError: could not link to ncbi files for $constant_id in $dir\n";
			}
		}
	return;
	}

#MCH#
sub get_ncbi_draft_genome_data {
	my $constant_id = shift;
	my $dir = shift;
	print "\tChecking local NCBI DRAFT genomes mirror: $dir for $constant_id\n";
	my $gbk = $constant_id."scaffold.gbk.tgz";
	my $fna = $constant_id.".fna";
	my $gbk_out = $constant_id.".gbk";
	my $gbk_string = `find $dir -name $gbk`;
	chomp $gbk_string;
	if ($gbk_string ne "")	{
		system("ln -s $gbk_string ./");
		}
	if (-e $gbk) {
		print "\t$constant_id draft gbk file copied, proceeding\n";
		my $string = `tar -ztf $gbk`;  #get the names
		my @files = split /\n/, $string;
		system("tar -xzf $gbk");	#extract the .tgz file
		foreach (@files) {	#concatenate the output files
			system("cat $gbk_out $_");
			}
		system("perl $GENBANK_TO_PTT_RNT_FNA $gbk_out"); #run J's genbank_split.pl on cat'd files
		my $seq = slurp_sequence("$fna");
		return 1, $seq, length($seq), $constant_id;
		}
	else {
		die "\tError: could not link to NCBI DRAFT .gbk file for $constant_id in $dir\n";
		}
	return;
	}

sub write_sequence_file {
#	my ($constant_id, $genome_id, $genome_seq, $make_lwgv, $COPRO) = @_;
	my ($constant_id, $genome_seq, $make_lwgv, $COPRO) = @_;

#	my $query = "SELECT feature_symbol, feature_type, feature_description, start_pos, stop_pos, direction FROM feature f WHERE primary_feature='Y' AND f.genome_id=? ORDER BY start_pos";
#	my $sth = $dbh->prepare($query);

	my @prev;

#	$sth->execute($genome_id);

#	die "writing sequence file with $COPRO\n";
	my $out = $constant_id . ".genome";
	open (OUT, ">$out") or die "can't open $out: $!\n";

	if ($make_lwgv || $COPRO) {
       		print OUT to_fasta("$constant_id", $genome_seq);
		return;
	}
#MCH - This is a remnant from the sql days...
#	elsif ($genome_id) { # sequence is in database
#		print "using database for $genome_id\n";
#		while (my @res = $sth->fetchrow_array()) {
#			my $seq = get_sequence(\$genome_seq, $res[3], $res[4]);
#			$res[1] =~ s/\s+/_/;
#			print OUT to_fasta("$res[0]|$res[1]|$res[2]", $seq);
#			if (@prev && $res[3] > $prev[4]) {
#				my $name = "$res[0]_intergenic_$prev[0]";
#				my $seqIntergenic = get_sequence(\$genome_seq, $prev[4], $res[3]);
 #      				print OUT to_fasta("$name", $seqIntergenic);
#			}
#			@prev = @res;
#		}
#	}
#MCH# modified the below code to fix order sequences of output .genome files and make naming
#more intuitive in the output file.  Validated by running pipeline pre/post and comparing outputs.
	else { # sequence is provided by user with ptt and rnt files
		my $features = read_genome_features_from_ptt_rnt($constant_id);
		for my $res (@$features) {
			my @res = @$res;
			my $seq = get_sequence(\$genome_seq, $res[3], $res[4]);
			$res[1] =~ s/\s+/_/;
#			print OUT to_fasta("$res[0]|$res[1]|$res[2]", $seq);
			if (@prev && $res[3] > $prev[4]) {
				my $name = "$prev[0]_intergenic_$res[0]";
				my $seqIntergenic = get_sequence(\$genome_seq, $prev[4], $res[3]);
       				print OUT to_fasta("$name", $seqIntergenic);
				}
			print OUT to_fasta("$res[0]|$res[1]|$res[2]", $seq);
			@prev = @res;
		}
		close OUT;
		return $features;
	}

	close OUT;

}

sub get_sequence {
	my ($genome_seq, $start, $stop) = @_;
	my $len = ($stop-$start) + 1;

	my $s = $start-1;
	my $res = substr($$genome_seq, $start-1, $len);
	return $res;
}

sub read_genome_features_from_ptt_rnt {
	my $id = shift;

#	die "reading ptt rnt\n";

	my $ptt = "$id.ptt";
	my $rnt = "$id.rnt";

	open (PTT, "$ptt")  or die "can't open $ptt: $!\n";

#	my $query = "SELECT feature_symbol, feature_type, feature_description, start_pos, stop_pos, direction FROM feature f WHERE primary_feature='Y' AND f.genome_id=? ORDER BY start_pos";

	my @all_features;
	while (<PTT>) { last if /^Location|^Product/; } # skip headers

	my $genBankNew = 0;
	if (/^Product/) { $genBankNew = 1; }

	while (<PTT>) {
		chomp;
		my @pieces = split /\t/;
		if ($genBankNew) {
			my ($start, $stop) = ($pieces[1], $pieces[2]);
#			die "[$pieces[0],\"CDS\", $pieces[8],$start,$stop]\n";
			push @all_features, [$pieces[8],"CDS", $pieces[0],$start,$stop];

		}
		else {
			my ($start, $stop) = split /\.\./, $pieces[0];
			push @all_features, [$pieces[5],"CDS", $pieces[8],$start,$stop];
		}
	}

	close PTT;

#16S_rRNA
#23S_rRNA
#tRNA
#tmRNA

	open (RNT, "$rnt")  or die "can't open $rnt: $!\n";
	while (<RNT>) { last if /^Location|^Product/; } # skip headers

	my $genBankNew2 = 0;
	if (/^Product/) { $genBankNew2 = 1; }

	while(<RNT>) {
		chomp;
		my @pieces = split /\t/;

		if ($genBankNew2) {
			my $start = $pieces[1];
			my $stop = $pieces[2];
			my $description = $pieces[0];

			my $type = "";
			if ($description =~ /16S/) { $type = "16S_rRNA"; }
			elsif ($description =~ /23S/) { $type = "23S_rRNA"; }
			elsif ($description =~ /5S/) { $type = "23S_rRNA"; }
			elsif ($description =~ /tRNA/) { $type = "tRNA"; }
			elsif ($description =~ /tmRNA/) { $type = "tmRNA"; }
			elsif ($description =~ /transfer-messenger RNA/) { $type = "tmRNA"; }
			elsif ($description =~ /small subunit ribosomal RNA/) { $type = "16S_rRNA"; }
			elsif ($description =~ /Small Subunit Ribosomal RNA; ssuRNA; SSU ribosomal RNA/) { $type = "16S_rRNA"; }
			elsif ($description =~ /Large Subunit Ribosomal RNA; lsuRNA; LSU ribosomal RNA/) { $type = "16S_rRNA"; }
			else { die "Error: unknown rRNA or tRNA type found: $description!\n" ;}

			push @all_features, [$pieces[7],$type,$description,$start,$stop];
			}

		else {
			my ($start, $stop) = split /\.\./, $pieces[0];

			my $description = $pieces[8];

			my $type = "";
			if ($description =~ /16S/) { $type = "16S_rRNA"; }
			elsif ($description =~ /23S/) { $type = "23S_rRNA"; }
			elsif ($description =~ /5S/) { $type = "23S_rRNA"; }
			elsif ($description =~ /tRNA/) { $type = "tRNA"; }
			elsif ($description =~ /tmRNA/) { $type = "tmRNA"; }
			elsif ($description =~ /transfer-messenger RNA/) { $type = "tmRNA"; }
			elsif ($description =~ /small subunit ribosomal RNA/) { $type = "16S_rRNA"; }
			elsif ($description =~ /Small Subunit Ribosomal RNA; ssuRNA; SSU ribosomal RNA/) { $type = "16S_rRNA"; }
			elsif ($description =~ /Large Subunit Ribosomal RNA; lsuRNA; LSU ribosomal RNA/) { $type = "16S_rRNA"; }
			else { die "Error: unknown rRNA or tRNA type found: $description!\n" ;}

			push @all_features, [$pieces[5],$type,$description,$start,$stop];
			}

	}
	close RNT;

#NOTE: this code will parse genbank ptt and rnt files in GENBANK FORMAT
#	open (RNT, "$rnt")  or die "can't open $rnt: $!\n";
#	while (<RNT>) { last if /^Location|^Product/; } # skip headers
#	while(<RNT>) {
#		chomp;
#		my @pieces = split /\t/;

#		my ($start, $stop) = split /\.\./, $pieces[0];

#		my $description = $pieces[8];

#		my $type = "";
#		if ($description =~ /16S/) { $type = "16S_rRNA"; }
#		elsif ($description =~ /23S/) { $type = "23S_rRNA"; }
#		elsif ($description =~ /5S/) { $type = "23S_rRNA"; }
#		elsif ($description =~ /tRNA/) { $type = "tRNA"; }
#		else { die "Error: unknown rRNA or tRNA type found!\n" ;}
#		push @all_features, [$pieces[5],$type,$description,$start,$stop];
#	}
#	close RNT;

	@all_features = sort {$a->[3] <=> $b->[3]} @all_features;

#	print  "@{$all_features[0]}\t@{$all_features[1]}\n";
	return \@all_features;
}

sub write_position_file {
#	my ($constant_id, $genome_id, $features) = @_;
	my ($constant_id, $features) = @_;

	my $out = $constant_id . ".gene_pos";
	open (GENE_POS, ">$out") or die "can't open $out: $!\n";

#	if ($genome_id) {
#		my $query = "SELECT feature_symbol, start_pos, stop_pos from feature where genome_id=?";
#		my $sth = $dbh->prepare($query);
#		$sth->execute($genome_id);

#		while (my @res = $sth->fetchrow_array()) {
#			my $text = join "\t", @res;
#			print GENE_POS "$text\n";
#		}
#	}
#	else {
	#my $query = "SELECT feature_symbol, feature_type, feature_description, start_pos, stop_pos, direction FROM feature f WHERE primary_feature='Y' AND f.genome_id=? ORDER BY start_pos";
	for my $res (@$features) {
			my @res = @$res;

			print GENE_POS "$res[0]\t$res[3]\t$res[4]\n";
		}
#	}
	close GENE_POS;
}

sub make_cleanup_file {
	open (CLEANUP, "+>cleanup.sh") or die "Can't open cleanup.sh!\n";
	print CLEANUP "rm slurm*\n";
	print CLEANUP "rm *.counts\n";
	print CLEANUP "rm *.mapped\n";
	print CLEANUP "rm *.mapped_CDS\n";
	print CLEANUP "rm -r genomes/\n";
	print CLEANUP "mkdir logs/\n";
	print CLEANUP "mv *.log logs/\n";
	close CLEANUP;
}
